# cables

![Licensed MPL 2.0](https://badgen.net/gitlab/license/aria.theater/cables) ![Last commit](https://badgen.net/gitlab/last-commit/aria.theater/cables) [![Latest Release](https://gitlab.com/aria.theater/cables/-/badges/release.svg)](https://gitlab.com/aria.theater/cables/-/releases) ![Financial backers](https://opencollective.com/cables/tiers/badge.svg)

This is a Discord bot. It's got cables in it! Big ole' WIP 🤠


## Deployment

Cables can be easily deployed with `docker-compose`. It will automatically
start the app with a volume mount bound to `configs/` by default.

1. Rename the provided configuration folder from `default_configs` to `configs`
2. Modify `configs/bot.json` file according to the
   [Configuration](https://gitlab.com/aria.theater/cables/-/wikis/Operations/Configuration)
   section of the wiki
3. Optionally, create an `.env` file to your liking
4. Start everything the provided script

```bash
./docker build
./docker up
```


## Upgrading

To upgrade from CLI:

```bash
git pull
./docker upgrade
```


To upgrade from your Discord server:

```
.bot kill
```


## Development

This bot requires python 3.9+ and is managed with [`poetry`](https://python-poetry.org/).

```bash
pip install -r requirements.txt
poetry install
poetry shell
```

See the [Deployment](#Deployment) section's checklist through step 3.

Instead of using docker, you can start it up locally, using the provided script:

```bash
./start
```


## Contributing

See the [relevant section](https://gitlab.com/aria.theater/cables/-/wikis/Contributing)
the wiki for instructions on forking and submitting merge requests. We prefer
everything to be formatted with `black`, but it's not absolutely necessary. The
[pre-commit framework](https://pre-commit.com/) will automatically run `black`
and `flake8` on commit to make sure you aren't being naughty. 🙃

To set it up:

```bash
poetry shell
pre-commit install
```
