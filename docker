#!/bin/bash

# Capture optional arguments
while [[ "$1" =~ ^-- ]]; do
    # Select AWS variant of app compose file
    [ "$1" = '--aws' ] && { APP_COMPOSE_FILE=infra/docker-compose.aws.yml; }

    # Set docker-compose profile
    [[ "$1" =~ ^--profile= ]] && { COMPOSE_PROFILE=$1; }

    shift
done

# Set default docker-compose profile if necessary
[ ! "$APP_COMPOSE_FILE" ] && { APP_COMPOSE_FILE=infra/docker-compose.yml; }

# Usage
if  [[ ! "$1" || ! "$1" =~ (clean|build|recompose|up|down|start|restart|stop) ]] ||
    [[ "$1" = restart && ! "$2" ]]
then
    echo 'Usage: $0 [--aws] command [service-name]' >&2
    printf "\tcommands available: clean, build, recompose, up, down, start, restart, stop" >&2
    exit 64
fi

# Constant `docker-compose` argument
# to set build context as root project directory
BASE_ARGS=("--project-directory=$(dirname $0)")
APP_ARGS=(${BASE_ARGS[*]} "$COMPOSE_PROFILE")

# Control flow functions
source include/docker.sh

# Command parsing & control flow
CMD=$1; shift
case $CMD in
    clean)
        docker_clean
        ;;
    build)
        docker_clean
        build_all
        ;;
    up)
        compose_cmd $CMD -d $@
        check_bot
        ;;
    upgrade)
        build_all
        echo '>>> Bringing up infra'
        compose_cmd up -d $@
        check_bot
        docker_clean
        ;;
    recompose)
        echo '>>> Destroying all services'
        compose_cmd down
        docker_clean
        echo '>>> Bringing up infra'
        compose_cmd up -d $@
        check_bot
        ;;
    down|start|restart|stop)
        compose_cmd $CMD $@
        [[ "$CMD" =~ (re)?start ]] && check_bot
        ;;
esac
