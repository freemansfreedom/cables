FROM cables_bot:base

LABEL   source="https://gitlab.com/aria.theater/cables/-/tree/main" \
        issues="https://gitlab.com/aria.theater/cables/-/issues" \
        maintainer="erin"

ENV ENV=development
ENV CABLES_APP_PATH=/usr/src/cables
ENV CABLES_CONFIG_PATH=/var/lib/cables
ARG BUILD_ENV=development

WORKDIR $CABLES_APP_PATH
ADD cabling_bot ./cabling_bot

ADD poetry.lock pyproject.toml README.md ./
ARG SODIUM_INSTALL=system
RUN poetry install \
        $(test "$BUILD_ENV" == production && echo "--no-dev") \
        --no-interaction --no-ansi

ADD .git ./.git
ADD cmd ./
RUN chmod 775 cmd

CMD ["./cmd"]

VOLUME $CABLES_CONFIG_PATH
