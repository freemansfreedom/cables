--------------------------------------------------
-- #303: Mutually exclusive react-role messages --
--------------------------------------------------

-- Create new messages table
CREATE TABLE IF NOT EXISTS role_react_messages (
    id      serial      PRIMARY KEY,
    guild   bigint      NOT NULL,
    channel bigint      NOT NULL,
    message bigint      NOT NULL,
    author  bigint      NOT NULL,
    mutex   bool        NOT NULL DEFAULT 'false'
);
CREATE INDEX IF NOT EXISTS idx_rrm_guild_chan
    ON role_react_messages (guild, channel);
CREATE UNIQUE INDEX IF NOT EXISTS idx_rrm_msg
    ON role_react_messages (message);

-- Create new reactions table
CREATE TABLE IF NOT EXISTS role_react_reactions (
    id      serial      PRIMARY KEY,
    msg_ref bigint      NOT NULL,
    emoji   varchar(64) NOT NULL,
    role    bigint      NOT NULL
);
CREATE UNIQUE INDEX IF NOT EXISTS idx_rrr_msg_emoji
    ON role_react_reactions (msg_ref, emoji);

-- Migrate messages
INSERT INTO role_react_messages
    (guild, channel, message, author, mutex)
SELECT
    MIN(guild), MIN(channel), MIN(message), MIN(author), 'false'
FROM role_react
GROUP BY message;

-- Migrate reactions
INSERT INTO role_react_reactions
    (msg_ref, emoji, role)
SELECT
    msgs.id, emoji, role
FROM role_react AS rr
JOIN role_react_messages AS msgs ON rr.message = msgs.message;

-- Delete old table
DROP TABLE role_react;