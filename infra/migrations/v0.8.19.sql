ALTER TABLE guild_invite_codes
    ADD COLUMN IF NOT EXISTS requested_by bigint NULL;

CREATE UNIQUE INDEX IF NOT EXISTS idx_guildinvcodes_code
    ON guild_invite_codes (code);