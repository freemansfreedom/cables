--
-- #442 Support many-to-one mappings of members to color roles
--

-- Create member-role mapping table
CREATE TABLE IF NOT EXISTS color_role_map (
    id      serial  PRIMARY KEY,
    "role"  bigint  NOT NULL REFERENCES color_roles ON DELETE CASCADE,,
    member  bigint  NOT NULL
);
CREATE INDEX IF NOT EXISTS idx_colorrolemap_roles
    ON color_role_map ("role");

-- Populate mappings
INSERT INTO color_role_map ("role", member)
    SELECT id, member FROM color_roles;

-- Remove member column from definition table
--     and recreate index.
DROP INDEX idx_color_users;
ALTER TABLE color_roles DROP COLUMN member;
CREATE INDEX idx_colorroles_guild ON color_roles (guild);

--
-- #431 Common report channel guild configuration
--

INSERT INTO config_keys (
    module, "key",
    "description",
    "default"
) VALUES (
    'moderation', 'report_channel',
    '(int) A channel ID for which various member-related reports such as watch activity, content violations, etc. will be delivered. Defaults to the system messages channel.',
    NULL
);

INSERT INTO config_guild (
    config_ref,
    guild,
    "value"
)
SELECT
    report_key.id AS config_ref,
    guild.guild AS guild,
    guild."value" AS "value"
FROM config_guild AS guild
JOIN config_keys AS keys ON
    guild.config_ref = keys.id
JOIN config_keys AS report_key ON
    report_key.module = 'moderation'
    AND report_key."key" = 'report_channel'
WHERE
    keys.module = 'user_watch'
    AND keys."key" = 'report_channel';

DELETE FROM config_guild AS guild
USING config_keys AS keys
WHERE
    guild.config_ref = keys.id
    AND keys.module = 'user_watch'
    AND keys."key" = 'report_channel';

--
-- #455 Bug: Invite use ought to be a many-to-many mapping
--

DROP INDEX IF EXISTS idx_guildinvuses_join;
CREATE INDEX IF NOT EXISTS idx_guildinvuses_join ON guild_invite_uses ("join");
