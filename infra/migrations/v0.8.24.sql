------------------------------------
-- #158: Track single-use invites --
------------------------------------

--
-- create new table to hold member join timestamps
--
-- can't do this as part of cog init due to error on
-- index creation for columns that don't exist yet
--
CREATE TABLE IF NOT EXISTS guild_member_joins (
    id              serial      PRIMARY KEY,
    guild           bigint      NOT NULL,
    "user"          bigint      NOT NULL,
    joined_at       timestamp   NOT NULL DEFAULT CURRENT_TIMESTAMP
);
CREATE INDEX IF NOT EXISTS idx_guildjoins_user
    ON guild_member_joins ("user");
CREATE INDEX IF NOT EXISTS idx_guildjoins_joinedat
    ON guild_member_joins ("user", joined_at);

-- populate table
INSERT INTO guild_member_joins (
    guild,
    "user",
    joined_at
)
SELECT
    codes.guild,
    uses."user",
    uses.joined_at
FROM guild_invite_uses AS uses
JOIN guild_invite_codes AS codes
    ON uses.invite = codes.id;

-- ALTER TABLE guild_invite_uses
--     DROP CONSTRAINT "guild_invite_uses_join_fkey",
--     DROP COLUMN "join";

-- add new member join reference column to invite uses table
-- initially setting NOT NULL and DEFAULT 0 to avoid constraint violations
ALTER TABLE guild_invite_uses
    ADD COLUMN "join" bigint NOT NULL DEFAULT 0;

-- populate member join reference column in invite uses table
-- matching against existing unique criteria between invite and joins tabless
UPDATE guild_invite_uses AS uses
SET "join" = joins.id
FROM
    guild_member_joins AS joins,
    guild_invite_codes AS codes
WHERE
    uses.invite = codes.id
    AND joins.guild = codes.guild
    AND joins."user" = uses."user"
    AND joins.joined_at = uses.joined_at;

-- enforce unique constraint on join reference column
-- now that it's populated with data
CREATE UNIQUE INDEX idx_guildinvuses_join
    ON guild_invite_uses ("join");

--
-- alter invite uses table
--
-- remove DEFAULT attribute from join reference
-- change invite code reference to bigint data type (was serial)
-- add foreign key constraint to join reference column
--
ALTER TABLE guild_invite_uses
    ALTER COLUMN "join" DROP DEFAULT,
    ALTER COLUMN invite SET DATA TYPE bigint,
    ADD CONSTRAINT "guild_invite_uses_join_fkey"
        FOREIGN KEY ("join")
        REFERENCES guild_member_joins
        ON DELETE CASCADE;

-- clean up old columns from invite uses table
-- remove joined_at timestamp and user reference
ALTER TABLE guild_invite_uses
    DROP COLUMN joined_at,
    DROP COLUMN "user";

-- ###################################################### --

ALTER TABLE guild_invite_codes
    ADD COLUMN max_uses int NULL,
    ADD COLUMN expires_at timestamp NULL;
