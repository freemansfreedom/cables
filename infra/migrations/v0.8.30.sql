--
-- #392 Add role react honeypot attribute
--

ALTER TABLE role_react_reactions
    ADD COLUMN honeypot bool NOT NULL DEFAULT 'false';
