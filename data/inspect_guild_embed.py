from discord import Guild
from discord.utils import format_dt
from cabling_bot.util.discord import (
    get_administrators,
    get_janitors,
    get_text_channels,
    get_voice_channels
)


def _data(guild: Guild):
    return [
        {
            "name": "ID",
            "value": guild.id,
            "inline": False
        },
        {
            "name": "Dates",
            "value": [
                {
                    "emoji": "🎂",
                    "text": format_dt(guild.created_at, "R")
                },
                {
                    "emoji": "🛬",
                    "text": format_dt(guild.me.joined_at, "R")
                }
            ],
            "inline": True
        },
        {
            "name": "Leadership",
            "value": [
                {
                    "emoji": "👑",
                    "text": guild.owner.mention
                },
                {
                    "emoji": "🪄",
                    "data": get_administrators(guild),
                    "annotation": "Admins"
                },
                {
                    "emoji": "🧹",
                    "data": get_janitors(guild),
                    "annotation": "Jannies"
                }
            ],
            "inline": True
        },
        {
            "name": "Assets",
            "value": [
                {
                    "emoji": "🙂",
                    "data": guild.emojis,
                    "annotation": "Emoji"
                },
                {
                    "emoji": "🥟",
                    "data": guild.stickers,
                    "annotation": "Stickers"
                }
            ],
            "inline": True
        },
        {
            "name": "Channels",
            "value": [
                {
                    "emoji": "💬",
                    "data": get_text_channels(guild),
                    "annotation": "Text"
                },
                {
                    "emoji": "🔉",
                    "data": get_voice_channels(guild),
                    "annotation": "Voice"
                }
            ]
        }
    ]
