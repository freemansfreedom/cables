# NOTE: these attributes are only present for accounts have
#   moderator access to the subreddit where the submission resides
#   https://github.com/reddit-archive/reddit/wiki/JSON
#   banned_by       who removed this comment. null if nobody or you are not a mod
#   num_reports     how many times this comment has been reported, null if not a mod
#   mod_note        https://www.reddit.com/dev/api#GET_api_mod_notes

removal_method = submission.removed_by_category
removed = (removal_method or getattr(submission, "removed", False))
removal_reason = (
    getattr(submission, "mod_note", None)
    or getattr(submission, "removal_reason", None)
)
report_reasons = getattr(submission, "report_reasons", None)
banned = getattr(submission, "banned_at_utc", None)
ban_reason = getattr(submission, "ban_note", None)
if removed:
    if (removal_method == "moderator" and banned):
        await msg.edit(suppress=True)
        text = "{msg.author.mention} ({msg.author}) linked content "
        text += "that resulted in a ban of the OP in the community "
        text += "that it was posted in.\n\n"
        text += "**Link:** <{url}>\n"
        if removal_reason:
            text += "**Removal Reason:** {removal_reason}\n"
        if report_reasons:
            text += "**User Reports:"
        if ban_reason:
            text += "**Ban Reason:** {ban_reason}"
    return (False, None, None)
