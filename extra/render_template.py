from jinja2 import Environment, FileSystemLoader
import yaml
import sys
from os import path
import textwrap

tpl_path = path.abspath(sys.argv[1])
tpl_dir = path.dirname(tpl_path)
tpl_file = path.basename(tpl_path)

loader = Environment(loader=FileSystemLoader(tpl_dir))
template = loader.get_template(tpl_file)

try:
    var_path = path.abspath(sys.argv[2])
    with open(var_path) as f:
        tpl_vars = yaml.safe_load(f)[0]
except (NameError, IndexError):
    if tpl_file == "ban_user_notification.tpl":
        tpl_vars = {
            "guild": "an epic guild",
            "issuer": "owner#1000",
            "reason": textwrap.dedent("""
                this is an example reason for someone being banned
                that contains whitespace such as newlines
                to ensure rendering of the template is kosher
            """),
            "policy_uri": "https://example.com/policy/",
            "can_appeal": True,
            "staff_list": [
                "<@123456789123456789> (admin#2000)",
                "<@123456789123456790> (admin#3000)"
            ]
        }
    else:
        e = "For non-supported templates, provide a second argument to a "
        e += "yaml file containing required variables"
        print(e)
        sys.exit(1)

if tpl_file == "ban_user_notification.tpl":
    if tpl_vars["reason"]:
        tpl_vars["reason"] = tpl_vars["reason"].strip("\n").replace('\n', '\n> ')

    tpl_vars["show_staff"] = False
    if tpl_vars["staff_list"]:
        tpl_vars["staff_list"] = "\n".join(
            map(lambda m: f"- {m}", tpl_vars["staff_list"])
        )
        tpl_vars["show_staff"] = True
else:
    print("Warning: This template file may not be supported!")

tpl_out = tpl_path.replace(".tpl", ".md")
with open(tpl_out, 'w') as f:
    f.write(template.render(tpl_vars))
