#!/bin/bash

_PWD_BASE="$(basename $PWD)"
if [ "$_PWD_BASE" != 'cables' ]; then
  echo 'Must be ran from the root of the repo' >&2
  exit 64
fi