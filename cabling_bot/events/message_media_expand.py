from __future__ import annotations
from typing import TYPE_CHECKING

if TYPE_CHECKING:
    from typing import Union
    from discord import (Message, Guild)
    from NHentai.nhentai import Doujin
    from cabling_bot.bot import CablesBot as Bot

import asyncio
import logging
import os

from urllib.parse import urlparse
from html import unescape
from NHentai import NHentaiAsync
from discord.ext import commands
from cabling_bot.util import (embeds, media as media_parse, files, matching)

from typing import OrderedDict
from discord import (File, Embed)
from asyncpraw.exceptions import AsyncPRAWException
from cabling_bot.services.ehentai import EHentaiService
from cabling_bot.util.bad_content import MediaInspector
from cabling_bot.bot import CablesCog as Cog

logger = logging.getLogger(__name__)

REPORT_TYPES = (1, 2, 3)


class MediaExpand(Cog):
    def __init__(self, bot: Bot, hidden=True):
        self._bot = bot

        self.ehentai = EHentaiService()
        self.inspector = MediaInspector(bot)
        self._hydrus = bot.hydrus

        bot.config_keys.new(
            "feature_flags",
            "previews",
            "(bool) Toggles preview functionality for all supported services.",
            True
        )
        bot.config_keys.new(
            "preview",
            "channels",
            "(list[int]) List of channel or category IDs to preview media posts in.",
            []
        )

        self.handler_mapping = {
            "e-hentai.org": self.handle_ehentai_gallery,
            "exhentai.org": self.handle_ehentai_gallery
        }
        if bot.reddit:
            self.handler_mapping.update(
                {
                    "reddit.com": self.handle_reddit_post,
                    "www.reddit.com": self.handle_reddit_post,
                    "redd.it": self.handle_reddit_post,
                    "old.reddit.com": self.handle_reddit_post,
                }
            )
            self._reddit = bot.reddit
        self.nhentai = NHentaiAsync()
        self.handler_mapping.update({"nhentai.net": self.handle_nhentai_gallery})

    def previews_enabled(self, guild: Guild):
        return self.get_guild_key(guild.id, "feature_flags", "previews")

    def scanning_enabled(self, guild: Guild):
        gid = guild.id
        return (
            self.previews_enabled(guild)
            and self.get_guild_key(gid, "feature_flags", "content_scanning")
        )

    def hydrus_enabled(self):
        return bool(self._bot.hydrus.client)

    def parse_message(self, msg: Message) -> Union[bool, tuple[dict, tuple]]:
        channel_state = matching.channel_or_category(
            msg.channel,
            self.get_guild_key(msg.guild.id, "preview", "channels")
        )
        if not (self.previews_enabled(msg.guild) and channel_state):
            # text = ("parse_message: Feature not enabled "
            #         + f"(global={self.enabled()}, "
            #         + f"channel={msg.channel.id}:{channel_state}")
            # logger.debug(text)
            return False

        url_map, media_map = dict(), OrderedDict()

        urls = matching.extract_all_urls(msg.content)
        for url in urls:
            fqdn = urlparse(url).netloc
            if not getattr(url_map, fqdn, []):
                url_map[fqdn] = [url]
            else:
                url_map[fqdn] += [url]

        media_map["handlers"] = OrderedDict()
        for fqdn in url_map.keys():
            if not self.handler_mapping.get(fqdn, False):
                # logger.debug(f"fqdn '{fqdn}' didn't match a handler: {self.handler_mapping}")
                continue
            media_map["handlers"][fqdn] = url_map[fqdn]

        media_map["images"] = media_parse.list_post_images(msg, urls)

        for items in media_map.values():
            if not items:
                continue

            # TODO: support multiple
            url: str
            if type(items) is OrderedDict:
                items: dict
                url = items.popitem()[1][0]
            elif type(items) is list:
                url = items[0]
            elif type(items) is str:
                url = items
            # logger.debug(f"parsed url from message properties: {url}")
            fqdn = urlparse(url).netloc

            return (url, fqdn)

    async def handle_reddit_post(self, msg: Message, url: str) -> tuple:
        try:
            submission = await self._reddit.submission(url=url)
        except AsyncPRAWException as e:
            logger.debug(f"Reddit API call error: {e}")

        if (
            submission.url == url
            or not submission.over_18
            or submission.is_self
            or submission.is_video
        ):
            return False

        bad_type, bad_reason = await self.inspector.reddit(msg, submission)

        media = []
        if getattr(submission, 'is_gallery', None):
            metadata = submission.media_metadata
            for k in metadata.keys():
                if metadata[k]["s"].get("u", None):
                    media += [metadata[k]["s"]["u"]]
                elif metadata[k]["s"].get("gif", None):
                    media += [metadata[k]["s"]["gif"]]
        else:
            media += [submission.url]

        return (media, None, bad_type, bad_reason)

    async def handle_ehentai_gallery(self, msg: Message, url: str) -> tuple:
        metadata = self.ehentai.get(url)
        if not metadata:
            return

        bad_type, bad_reason, tags = await self.inspector.ehentai(msg, metadata)

        # format embed fields
        details = embeds.field_constructor(
            {
                "ID": metadata["gid"],
                "Category": metadata["category"],
                "Rating": metadata["rating"],
                "Images": metadata["filecount"],
            }
        )

        # construct message embed
        embed = Embed(title=unescape(metadata["title"]), url=url, colour=6686225)
        embed.add_field(name="Details", value=details, inline=True)
        embed.add_field(name="Tags", value=tags, inline=True)
        if not bad_type:
            embed.set_thumbnail(url=metadata["thumb"])
        embed.set_footer(
            text="e-Hentai Gallery",
            icon_url="https://cdn.catgirl.technology/img/ehentai.png",
        )

        return (False, embed, bad_type, bad_reason)

    async def handle_nhentai_gallery(self, msg: Message, url: str) -> tuple:
        urlpath = urlparse(url).path.split("/")
        _, route, gallery_id, *_ = urlpath
        is_gallery = True if route == "g" else False
        if not is_gallery:
            # logger.info(f"path component '{route}' does not contain match 'g'")
            return False

        # TODO: does this do automatic retry handling?
        response: Doujin = await self.nhentai.get_doujin(id=gallery_id)
        if not response:
            # logger.info("the nhentai gallery API call returned an HTTP exception")
            return False

        bad_type, bad_reason, tags, title = (
            await self.inspector.nhentai(msg, response)
        )

        # format embed fields
        details = embeds.field_constructor(
            {
                "ID": response.id,
                "Category": [category.name for category in response.categories],
                "Artist": [artist.name for artist in response.artists],
                "Group": [group.name for group in response.groups],
                "Parody": [parody.name for parody in response.parodies],
                "Character": [character.name for character in response.characters],
                "Pages": response.total_pages,
            }
        )

        # construct message embed
        embed = Embed(title=title, url=response.url, colour=15476820)
        embed.add_field(name="Details", value=details, inline=True)
        if response.tags:
            embed.add_field(name="Tags", value=tags, inline=True)
        if not bad_type:
            embed.set_thumbnail(url=response.images[0].src)
        embed.set_footer(
            text="nHentai Doujinshi",
            icon_url="https://cdn.catgirl.technology/img/nhentai.png",
        )

        if msg.embeds:
            await msg.edit(suppress=True)

        return (None, embed, bad_type, bad_reason)

    @commands.Cog.listener("on_scannable")
    async def hydrus_scan(self, msg: Message) -> None:
        bad_count = 0
        media = media_parse.list_post_images(msg)
        for m in media:
            filename = files.download_file(self._bot.tmp_path, m)
            filepath = f"{self._bot.tmp_path}/{str(filename)}"
            fileext = os.path.splitext(m)
            checksum = files.checksum_file(filepath)
            tags = self._hydrus.get_tags(checksum)
            if not tags:
                files.delete_file(filepath)
                continue
            reason = self.inspector.hydrus(tags)
            # NOTE: sending this as an attachment isn't ideal.
            #   the ux presentation is ugly :( unfortch, the
            #   easy alternative is using the url as a thumb,
            #   which would break if/when the source post or
            #   linked media were deleted...
            if reason:
                attachment = File(filepath, filename=f"{filename}.{fileext}")
                await self.inspector.report(msg, attachment, 1, reason)
                files.delete_file(filepath)
                bad_count += 1

    @commands.Cog.listener("on_previewable")
    async def handle_message(
        self,
        msg: Message,
        url: str,
        fqdn: str
    ) -> None:
        gid = msg.guild.id
        result = None
        if fqdn in self.handler_mapping.keys():
            result = await self.handler_mapping[fqdn](msg, url)
        media, embed, bad_type, bad_reason = None, None, None, None
        if result:
            # logger.debug(f"handler returned '{self.handler_mapping[fqdn]}' response: {result}")
            media, embed, bad_type, bad_reason = result

        reportable = False
        scan_channels = self.get_guild_key(gid, "content_scanning", "channels")
        if (
            self.scanning_enabled(msg.guild)
            and matching.channel_or_category(msg.channel, scan_channels)
        ):
            reportable = True
            if (
                self.hydrus_enabled()
                and (not result or (result and not bad_type))
            ):
                self._bot.dispatch("scannable", msg)

        # post voluntarily deleted
        if bad_type == 4:
            await msg.reply("Author deleted that post")
            return
        # account deleted
        elif bad_type == 5:
            text = "Author's account was deleted"
            if media:
                text += ", but the post media still exists"
            await msg.reply(text)
            await asyncio.sleep(1.0)
        # forbidden or suspicious content
        elif bad_type in REPORT_TYPES and reportable:
            await self.inspector.report(
                msg, embed,
                bad_type, bad_reason
            )
            return

        # reddit media post
        if media and not embed:
            count = len(media)
            text, extra_media = "", []
            for i in range(count):
                if bad_type == 5:
                    text += f"|| {media[i]} ||\n"
                    continue
                else:
                    text += media[i]

                if i == 0:
                    await msg.reply(text, mention_author=False)
                elif i <= 3:
                    await msg.channel.send(text)
                else:
                    extra_media += [f"<{media[i]}>"]

                if not bad_type:
                    text = ""

            if extra_media:
                text = "(remaining media in the gallery)\n"
                text += "\n".join(extra_media)
            if text:
                await msg.channel.send(text)
        # doujin
        elif embed and not media:
            await msg.reply(embed=embed, mention_author=False)
        # presently unused
        """elif media and embed:
            await msg.reply(embed=embed)
            await msg.channel.send(media)"""


def setup(bot: Bot):
    bot.add_cog(MediaExpand(bot))
