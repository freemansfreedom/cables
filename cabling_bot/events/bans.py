from __future__ import annotations
from typing import TYPE_CHECKING

import json
import logging
from discord.ext import commands
from discord.utils import find

from discord import AuditLogAction
from psycopg import DatabaseError
from cabling_bot.bot import CablesCog as Cog
from ..bot.errors import report_database_error
from cabling_bot.util.event_logging import EventLogController
from cabling_bot.util.guild_invites import GuildInvites

if TYPE_CHECKING:
    from typing import Union
    from discord import (Invite, User, Member, Guild)
    from cabling_bot.bot import CablesBot as Bot

logger = logging.getLogger(__name__)


class BanEventManager(Cog):
    """Manage server bans. See `help ban` for more info."""

    def __init__(self, bot: Bot) -> None:
        super().__init__()
        self._bot = bot
        self.log = EventLogController(bot)
        self.db = bot.db
        self.tracker = GuildInvites(bot.db)

    @commands.Cog.listener("on_member_ban")
    async def handle_ban_event(self, guild: Guild, user: Union[User, Member]) -> None:
        """Accounting for guild member ban additions."""

        # get issuer information from the audit log, as the
        # the ban entry reason field is deprecated
        # https://discord.com/developers/docs/resources/guild#create-guild-ban
        async for entry in guild.audit_logs(
            action=AuditLogAction.ban,
            oldest_first=False,
            limit=10
        ):
            if entry.target.id != user.id:
                continue

            if entry.user.id == self._bot.user.id:
                audit = json.loads(entry.reason)
                issuer = guild.get_member(audit["issuer"])
            else:
                audit = dict(issuer=entry.user.id, text=entry.reason)
                issuer = entry.user

            break

        records = self.tracker.list_member_invites(guild, user)
        if records:
            for r in records:
                code = r[0]
                invite: Invite = self._bot.fetch_invite(code)
                revoked_by = dict(revoked_by=issuer.id)
                self._bot.loop.create_task(
                    invite.delete(reason=json.dumps(revoked_by))
                )

        sql = """
            INSERT INTO guild_bans
                (guild, "user", reason, created_by)
            VALUES
                (%s, %s, %s, %s)
        """
        values = (guild.id, user.id, audit["text"], audit["issuer"])
        try:
            with self.db.connection() as conn:
                conn.execute(sql, values)
        except DatabaseError as e:
            await report_database_error(self._bot, None, "ban", e)

        await self.log.member_sanction(
            guild, "ban", user,
            issuer, audit["text"]
        )

    @commands.Cog.listener("on_member_unban")
    async def handle_unban_event(self, guild: Guild, user: User) -> None:
        """Accounting for guild member ban removals."""

        sql = """
            DELETE FROM guild_bans
            WHERE guild = %s AND "user" = %s
        """
        values = (guild.id, user.id)
        with self.db.connection() as conn:
            conn.execute(sql, values)

        async for entry in guild.audit_logs(
            action=AuditLogAction.unban,
            oldest_first=False,
            limit=10
        ):
            if entry.target.id != user.id:
                continue
            else:
                actor, when = entry.user, entry.created_at
                break

        sql = """
            UPDATE guild_bans_history
            SET deleted_by = %s, deleted_at = %s
            WHERE guild = %s AND "user" = %s
        """
        values = (actor.id, when, guild.id, user.id)
        with self.db.connection() as conn:
            conn.execute(sql, values)

        await self.log.member_unban(guild, user)


def setup(bot: Bot):
    bot.add_cog(BanEventManager(bot))
