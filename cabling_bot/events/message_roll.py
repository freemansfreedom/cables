import re
import logging
from threading import Thread
from discord.ext import commands
from discord.utils import snowflake_time
from cabling_bot.util.matching import channel_or_category

from typing import Optional, Union
from cabling_bot.bot.types import MessageableChannel
from discord import (Interaction, Message, TextChannel, VoiceChannel)
from cabling_bot.bot import (
    CablesCog as Cog,
    CablesBot as Bot,
    CablesAppContext as ApplicationContext
)


logger = logging.getLogger(__name__)

# https://s4s.fandom.com/wiki/List_of_Checks
GET_REGEX = r"(0{2,}|1{2,}|2{2,}|3{2,}|4{2,}|5{2,}|6{2,}|7{2,}|8{2,}|9{2,})$"
GET_PREFICES = dict(
    enumerate([
        None, None,
        "nice", "nice",
        "sweet",
        "amazing",
        "excellent",
        "euphric",
        "zOMG", "zOMG", "zOMG"
    ])
)
GET_NAMES = dict(
    enumerate([
        False, False,
        "dubs",
        "trips",
        "quads",
        "quints",
        "sexes",
        "septs",
        "octs",
        "nons",
        "decs",
    ])
)

ROLL_MODULE_KEYS = [
    "timestamp_matches",
    "dubs",
    "trips"
]


class RollManager(Cog):
    """RNG event and command module. See `help <roll>` for more info."""
    def __init__(self, bot: Bot):
        self._bot = bot
        bot.config_keys.new(
            "feature_flags", "rolls",
            "(bool|list[int]) GET detection (#243). Guild-wide toggle, or list of channel/category IDs.",
            False
        )
        bot.config_keys.new(
            "rolls", "timestamp_matches",
            "(bool) Toggles matching against UNIX timestamps in both automatic and command-triggered rolls. If disabled, only message IDs are compared.",
            True
        )
        bot.config_keys.new(
            "rolls", "dubs",
            "(bool) Toggles doubles in automatic message rolls.",
            False
        )
        bot.config_keys.new(
            "rolls", "trips",
            "(bool) Toggles triples in automatic message rolls.",
            False
        )

    def enabled(
        self,
        channel: MessageableChannel,
        feature: str
    ) -> bool:
        chid = channel.guild.id
        if type(channel) not in (TextChannel, Thread, VoiceChannel):
            return None

        if feature == "rolls":
            destinations = self.get_guild_key(chid, "feature_flags", feature)
            if (
                destinations is not True
                and not channel_or_category(channel, destinations)
            ):
                return False
            return destinations
        elif feature in ROLL_MODULE_KEYS:
            return self.get_guild_key(chid, "rolls", feature)
        else:
            return None

    def _is_get(self, candidate: str) -> tuple[bool, int]:
        match = re.search(GET_REGEX, candidate)
        if not match:
            return (False, 0)

        match = match[0]
        length = len(match)
        get = GET_NAMES.get(length, None)
        if get is False:
            return (False, 0)
        elif get is None:
            return (f"**MYTHIC GET!! ({candidate})**", length)
        else:
            text = str()

        prefix = GET_PREFICES.get(length, None)
        if prefix:
            text += prefix + " "

        numeral = int(match[0])
        if numeral == 0:
            text += "clear" + " "
        elif numeral == length:
            text += "pure" + " "

        text += get
        if length > 2:
            text += "!" * (length - 2)
        text += f" ({candidate.replace(match, f'**{match}**')})"

        return (text, length)

    def parse_message(
        self, source: Union[Message, Interaction]
    ) -> Optional[tuple[str, str]]:
        msg_get, msg_length = self._is_get(str(source.id))
        timestamp = snowflake_time(source.id).strftime("%s")
        ts_get, ts_length = self._is_get(timestamp)

        if not (ts_get or msg_get):
            return False
        elif ts_length > msg_length:
            return ("timestamp", ts_get)
        else:
            return ("snowflake", msg_get)

    @commands.slash_command(name="roll")
    async def roll(self, ctx: ApplicationContext) -> None:
        """Roll for your chances at a GET!!1"""
        get = self.parse_message(ctx.interaction)
        if not get:
            reply = f"Sorry about your luck, fam! ({ctx.interaction.id})"
            return await ctx.respond(reply, delete_after=5)
        fmt, match = get
        self._bot.dispatch(
            "message_get_detected",
            ctx, fmt, match
        )

    @commands.Cog.listener("on_message_get_detected")
    async def send_get_reply(
        self, source: Union[ApplicationContext, Message],
        fmt: str, text: str
    ) -> None:
        channel = source.channel
        if (
            fmt == "timestamp"
            and not self.enabled(channel, "timestamp_matches")
        ):
            if type(source) is ApplicationContext:
                reply = f"Sorry about your luck, fam! ({source.interaction.id})"
                return await source.respond(reply, delete_after=5)
            return None

        if type(source) is Message:
            if not self.enabled(channel, "rolls"):
                return

            dubs = self.enabled(channel, "dubs")
            trips = self.enabled(channel, "trips")
            if (
                (not dubs and "dubs" in text)
                or (not trips and "trips" in text)
            ):
                return

            await source.reply(text, mention_author=False)
        elif type(source) is ApplicationContext:
            await source.respond(text)


def setup(bot: Bot):
    bot.add_cog(RollManager(bot))
