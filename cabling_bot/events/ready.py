import logging
from discord.utils import utcnow
from cabling_bot.bot.errors import default_error_handler as handle_err
from cabling_bot.events import invites
from cabling_bot.util import dev_guild
from discord.ext import commands

from discord import (Status, Game)
from cabling_bot.bot import (
    CablesCog as Cog,
    CablesBot as Bot,
)

logger = logging.getLogger(__name__)


class WebsocketManager(Cog):
    """Interal initialization module for bot websocket stream handling."""
    def __init__(self, bot: Bot, hidden=True):
        self._bot = bot

        self._was_ready_before = False
        self.init_time = None
        self.ready_time = None

    @property
    def invite_mgr(self) -> invites.InviteTrackingManager:
        return self._bot.get_cog("InviteTrackingManager")

    @commands.Cog.listener()
    async def on_ready(self) -> None:
        if not self._was_ready_before:
            self.init_time = utcnow()
            await self.on_first_ready()
            self._was_ready_before = True

            self._bot.add_listener(handle_err, "on_command_error")
            self._bot.add_listener(handle_err, "on_application_command_error")

        self.ready_time = utcnow()

    async def on_first_ready(self) -> None:
        await self._log_sign_in()

        # set activity
        _game = self._bot.get_config("fun.game", "in the wired~")
        await self._bot.change_presence(
            status=Status.online,
            activity=Game(_game)
        )

        # add application owner to `bot.owner_ids``
        self._bot.operator = await self._bot.update_owners()

        # initialize dev guild
        self._bot.dev_guild = await dev_guild.init(
            self._bot,
            self._bot.operator
        )

        # analyze invites since last start
        await self.invite_mgr.process_backlog()

    async def _log_sign_in(self):
        me, id = self._bot.user, self._bot.user.id
        appinfo = await self._bot.application_info()
        owner = appinfo.owner
        logger.info(f"Logged in as bot user '{me}' ({id})")
        logger.info(f"Application account belongs to '{owner}' ({owner.id})")

        u, g, p = self._bot.get_reach_metrics().values()
        if not (g or p):
            info = "Not yet participating in a guild or private group"
        else:
            s = self._bot.shard_count
            info = f"Serving {u:n} users"
            info += f" across {p:n} private groups" if p else ""
            info += f" and {g:n} guilds" if g else ""
            info += f" spanning {s:n} shards" if s else ""
        logger.info(info)


def setup(bot: Bot):
    bot.add_cog(WebsocketManager(bot))
