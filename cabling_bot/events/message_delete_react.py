import logging
from typing import Union
from discord import (PartialEmoji, Reaction, Member, User, Message)

from discord.ext import commands
from cabling_bot.bot import (constants, CablesBot as Bot)
from cabling_bot.util.privileged import is_staff_member

logger = logging.getLogger(__name__)


class HandleDeleteReact(commands.Cog):
    def __init__(self, bot: Bot, hidden=True) -> None:
        super().__init__()
        self._bot = bot

    @commands.Cog.listener("on_reaction_add")
    async def handle_delete_react(self, reaction: Reaction, reactor: Union[Member, User]):
        """Delete a bot post on ❌ reaction.

        Only works for the author of the referenced message or a staff member."""

        emoji = reaction.emoji
        msg = reaction.message
        guild = msg.guild
        author = msg.author
        if guild:
            is_staff = is_staff_member(self._bot, author, guild)

        if (
            # emoji match
            ((
                isinstance(emoji, PartialEmoji)
                and emoji.name == constants.Emojis.deny
            ) or (
                isinstance(emoji, str)
                and emoji == constants.Emojis.deny
            ))
            # bots cannot react
            and not reactor.bot
            # this bot's post
            and author.id == guild.me.id
            and ((
                # has a valid message reference
                msg.reference
                and msg.reference.resolved
                and type(msg.reference.resolved) is Message
                # authorize OP
                and reactor.id == msg.reference.resolved.author.id
            ) or (
                # authorize guild staff
                guild and (
                    reactor.id == guild.owner.id
                    or is_staff
                    or msg.channel.permissions_for(reactor).manage_messages
                )
            ))
        ):
            await msg.delete()


def setup(bot: Bot):
    bot.add_cog(HandleDeleteReact(bot))
