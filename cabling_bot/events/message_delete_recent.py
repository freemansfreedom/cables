import logging
from dateutil.relativedelta import relativedelta

import discord
from discord.ext import commands
from cabling_bot.util import media
from cabling_bot.util.matching import channel_or_category

from discord import (RawMessageDeleteEvent, Guild)
from cabling_bot.bot import (CablesCog as Cog, CablesBot as Bot)

logger = logging.getLogger(__name__)


class TombstoneMessage(Cog):
    def __init__(self, bot: Bot, hidden=True) -> None:
        super().__init__()
        self._bot = bot
        bot.config_keys.new(
            "feature_flags",
            "tombstones",
            "(bool) Whether or not to send a channel notification when a message is deleted.",
            False
        )
        bot.config_keys.new(
            "notifications",
            "tombstone_exemptions",
            "(list[int]) A list of users, channels or categories not to notify on message deletions for.",
            []
        )

    def enabled(self, guild: Guild):
        return self.get_guild_key(guild.id, "feature_flags", "tombstones")

    @commands.Cog.listener("on_raw_message_delete")
    async def handle_message_delete(self, event: RawMessageDeleteEvent):
        """Posts a deletion marker in response to someone removing a recent message."""
        now = discord.utils.utcnow()

        if not event.guild_id:
            return

        msg = event.cached_message
        if not msg:
            return

        if not self.enabled(msg.guild):
            return

        if (
            msg.author.bot
            or not msg.channel.can_send()
        ):
            return

        exemptions = self._bot.get_guild_key(
            msg.guild.id,
            "notifications",
            "tombstone_exemptions"
        )
        if exemptions and (
            msg.author.id in exemptions
            or channel_or_category(msg.channel, exemptions)
        ):
            return

        async for since in msg.channel.history(
            after=msg.created_at,
            limit=1
        ):
            return

        delta = abs((now - msg.created_at).seconds)
        if delta > 300:
            return

        msg = event.cached_message

        async for before in msg.channel.history(
            before=msg.created_at,
            after=(msg.created_at - relativedelta(minutes=15)),
            oldest_first=False,
            limit=1
        ):
            if (
                before.author.id == self._bot.user.id
                and before.content.startswith("🪦")
                and str(msg.author) in before.content
            ):
                return

        text = f"🪦 *Message(s) from* `{msg.author}` *were deleted*"

        contains = media.message_type(msg)
        if contains:
            text += " " + " ".join(contains)

        await msg.channel.send(text)


def setup(bot: Bot):
    bot.add_cog(TombstoneMessage(bot))
