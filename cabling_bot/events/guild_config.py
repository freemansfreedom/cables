import logging
from discord.ext import commands

from discord import Guild
from cabling_bot.bot.config import GuildConfig
from cabling_bot.bot import (
    CablesCog as Cog,
    CablesBot as Bot
)

logger = logging.getLogger(__name__)


class GuildConfigManager(Cog):
    """Internal handler for guild configuration."""

    def __init__(self, bot: Bot, hidden=True) -> None:
        super().__init__()
        self._bot = bot

        ## Privileged
        bot.config_keys.new(
            "privileged", "staff_role",
            ("(int) A role that all staff members regardless of hierarchy are assigned. "
             + " Used for notifications for moderator-related matters."),
            None
        )
        bot.config_keys.new(
            "privileged", "admin_role",
            "(int) The role for Administrators; those able to manage the server and ban users.",
            None
        )
        bot.config_keys.new(
            "privileged", "admin_proxy_role",
            "(int) An optional role that administrators using alt/proxy accounts are assigned.",
            None
        )
        bot.config_keys.new(
            "privileged", "global_moderator_role",
            ("(int) The role for Global Moderators; those who have guild-level permissions to "
             + "manage channels and issue user sanctions."),
            None
        )
        bot.config_keys.new(
            "privileged", "channel_moderator_role",
            ("(int) The role for Channel Moderators; those who have management permissions "
             + "set on one or more channels through permission overrides."),
            None
        )
        bot.config_keys.new(
            "privileged", "mod_channel",
            ("(int) A channel ID for where to send notifications pertinant to your server's moderators "
             + "e.g. reports of potentially infringing content."),
            None
        )
        bot.config_keys.new(
            "privileged", "system_channel",
            ("(int) A channel ID for where to send operational notifications e.g. when a user tries to do "
             + "something, but the bot is misconfigured."),
            None
        )

        ## Content Reports
        bot.config_keys.new(
            "feature_flags",
            "content_scanning",
            "(bool) Toggles scanning and reporting of media posts that may violate the Discord terms.",
            True
        )
        bot.config_keys.new(
            "content_scanning",
            "channels",
            "(list[int]) A list of channel or category IDs to scan for media violations in.",
            []
        )
        bot.config_keys.new(
            "content_scanning",
            "staff_mentions",
            "(bool) Toggles the pinging the staff role mention when a content report is sent.",
            False
        )
        bot.config_keys.new(
            "content_scanning",
            "automoderation",
            "(bool) Toggles automatic deletion of potentially infringing media.",
            False
        )

        ## Notifications
        bot.config_keys.new(
            "notifications", "account_age_threshold",
            "(int) Account age cutoff, in weeks, to trigger a staff notification for on member join.",
            4
        )
        bot.config_keys.new(
            "notifications",
            "public_announce_channel",
            "(int) Channel ID for the space used for general, server-wide announcements.",
            None
        )

        ## Moderation
        bot.config_keys.new(
            "moderation",
            "report_channel",
            ("(int) A channel ID for which various member-related reports "
             + "such as watch activity, content violations, etc. will be "
             + "delivered. Defaults to the system messages channel."),
            None
        )
        bot.config_keys.new(
            "moderation", "sanction_approval_quorum",
            "(bool) Requires administrator quorum to enact a user ban.",
            False
        )
        bot.config_keys.new(
            "moderation", "require_sanction_reasons",
            "(bool) Requires providing a reason when enacting sanctions against a member.",
            True
        )
        bot.config_keys.new(
            "moderation", "anonymous_sanctions",
            "(bool) Disables the name of the sanction issuer in any notifications to the user.",
            True
        )
        bot.config_keys.new(
            "moderation", "sanctions_mention_staff",
            "(bool) Adds a mention of the staff role when a sanction is carried out and notified upon.",
            False
        )
        bot.config_keys.new(
            "moderation", "dm_sanctionee",
            "(bool) Notifies a user you or your staff have sanctioned by DM.",
            True
        )
        bot.config_keys.new(
            "moderation", "guidelines_url",
            "(str) An optional link to a document describing your rules and/or guidelines.",
            None
        )
        bot.config_keys.new(
            "moderation", "acceptable_content_url",
            "(str) An optional link to a document describing your acceptable content policy.",
            None
        )

        ## Defcon
        bot.config_keys.new(
            "feature_flags", "defcon",
            "(bool) Toggles DEFCON various protection features and notifications for the guild.",
            True
        )
        bot.config_keys.new(
            "defcon", "webhook_id",
            "(str) A webhook ID that the bot may use to for determining if the guild still exists.",
            None
        )
        bot.config_keys.new(
            "defcon", "webhook_token",
            "(str) Webhook token counterpart for defcon.webhook_id.",
            None
        )
        bot.config_keys.new(
            "defcon", "bot_protection",
            "(bool) Toggle for protecting bots against sanctions.",
            False
        )
        bot.config_keys.new(
            "defcon", "ownerless_safety",
            ("(bool) If the guild owner's account doesn't exist while this is enabled, member sanction "
             + "commands will be refused."),
            False
        )

        ## Utility
        bot.config_keys.new(
            "utility", "persistent_threads",
            "(list[int]) A list of thread IDs to automatically un-archive",
            None
        )

    @commands.Cog.listener()
    async def on_guild_setup(
        self,
        guild: Guild,
        config: GuildConfig
    ) -> None:
        config.set(
            "privileged", "mod_channel",
            guild.system_channel.id
        )
        config.set(
            "privileged", "system_channel",
            guild.system_channel.id
        )
        config.set(
            "privileged", "report_channel",
            guild.system_channel.id
        )

    @commands.Cog.listener()
    async def on_guild_ready(
        self,
        guild: Guild,
        config: GuildConfig
    ) -> None:
        logger.debug(f"Caching settings for guild {guild.id}")
        _config = config.dump()
        self._bot.config_data[guild.id] = _config
        # logger.debug(f"GuildConfig={_config}")

    @commands.Cog.listener()
    async def on_guild_delete(self, guild: Guild) -> None:
        logger.debug(f"De-integrated with guild {guild.id}")
        del self._bot.config_data[guild.id]


def setup(bot: Bot):
    bot.add_cog(GuildConfigManager(bot))
