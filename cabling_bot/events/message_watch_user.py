import logging
import ring
import psycopg_pool
from datetime import (datetime, timezone)

from typing import (Tuple, Optional)
from discord import (Guild, Member, Embed, Message)
from cabling_bot.bot.types import MessageableChannel
from psycopg.errors import IntegrityError
from cabling_bot.bot.constants import Emojis
from cabling_bot.bot import (
    CablesCog as Cog,
    CablesBot as Bot,
    CablesContext as Context,
)

from discord.ext import commands
from discord.utils import format_dt
from cabling_bot.util.permissions import writable_channels_by_member
from cabling_bot.util import matching
from cabling_bot.bot import checks
from time import time

logger = logging.getLogger(__name__)


class WatchManager(Cog):
    """Commands for managing watched users. See `help watch` for more info."""

    def __init__(self, bot: Bot) -> None:
        super().__init__()
        self._bot = bot

        self.db: psycopg_pool.ConnectionPool = bot.db
        q = """\
            -- TABLE
            -- Configuration
            CREATE TABLE IF NOT EXISTS user_watches (
                id          serial      PRIMARY KEY,
                guild       bigint      NOT NULL,
                "user"      bigint      NOT NULL,
                created_by  bigint      NOT NULL,
                created_at  timestamp   NOT NULL DEFAULT CURRENT_TIMESTAMP
            );
            CREATE UNIQUE INDEX IF NOT EXISTS idx_userwatch_guilduser
                ON user_watches (guild, "user");

            -- TABLE
            -- Audit log
            CREATE TABLE IF NOT EXISTS user_watches_history (
                id          serial      PRIMARY KEY,
                guild       bigint      NOT NULL,
                "user"      bigint      NOT NULL,
                created_by  bigint      NOT NULL,
                created_at  timestamp   NOT NULL,
                deleted_by  bigint      NULL DEFAULT NULL,
                deleted_at  timestamp   NULL DEFAULT NULL
            );
            CREATE UNIQUE INDEX IF NOT EXISTS idx_userwatchhistory_guilduser
                ON user_watches (guild, "user");

            -- TRIGGER
            -- Populate audit log on record insertion from configuration table
            -- Record deletion is handled by the application
            CREATE OR REPLACE FUNCTION sp_user_watch_history_insert()
                RETURNS TRIGGER AS $$
                BEGIN
                    INSERT INTO user_watches_history
                        (guild, "user", created_at, created_by)
                    VALUES
                        (NEW.guild, NEW."user", NEW.created_at, NEW.created_by);
                    RETURN NEW;
                END;
                $$ LANGUAGE plpgsql;
            CREATE OR REPLACE TRIGGER tg_user_watches_insert
                AFTER INSERT ON user_watches
                FOR EACH ROW EXECUTE PROCEDURE sp_user_watch_history_insert();
            """
        with self.db.connection() as conn:
            conn.execute(q)

        bot.config_keys.new(
            "feature_flags",
            "user_watch",
            "(bool) Toggles user activity watch commands and their associated reporting.",
            True
        )
        bot.config_keys.new(
            "user_watch",
            "interval",
            "(int) How frequently to notify staff over a watched user's activity, in minutes. Minimum of `5`, and default of `15`.",
            15
        )
        bot.config_keys.new(
            "user_watch",
            "staff_mentions",
            "(bool) Toggle for mentioning the staff role when an activity report is dispatched.",
            False
        )

    def enabled(self, guild: Guild) -> bool:
        return self._bot.get_guild_key(
            guild.id,
            "feature_flags", "user_watch"
        )

    def report_channel(self, guild: Guild) -> Optional[MessageableChannel]:
        channel_id = self._bot.get_guild_key(
            guild.id,
            "moderation", "report_channel"
        )
        if not channel_id:
            return guild.system_channel
        return guild.get_channel(channel_id)

    @commands.group(name="watch")
    async def watch_user(self, ctx: Context):
        """Commands for managing watched users

        Watched users have their post activity reported on to staff
        """
        if ctx.invoked_subcommand is None:
            await ctx.fail_msg("Invalid command passed")

    # TODO: paginate embed results
    def _list(self, guild: Guild) -> Optional[dict]:
        sql = """
            SELECT "user", created_by, created_at
            FROM user_watches
            WHERE guild = %(g)s
            ORDER BY created_at DESC;
        """
        with self.db.connection() as conn:
            results = conn.execute(sql, dict(g=guild.id))
            watch_count = results.rowcount

            fields = []
            for row in results:
                user, creator, watch_time = row

                watch_time: datetime = watch_time.replace(tzinfo=timezone.utc)
                watched = guild.get_member(user)
                watcher = guild.get_member(creator)

                body = [
                    f"Who: {watched.mention}",
                    f"By: {watcher.mention}",
                    f"When: {format_dt(watch_time, 'R')}",
                ]

                field = {
                    "name": f"{watched}",
                    "value": "\n".join(body),
                    "inline": True,
                }
                fields.append(field)

        if not fields:
            return None

        return {
            "title": "Watched Users",
            "description": f"Activity is being reported for `{watch_count}` user(s)",
            "fields": fields,
            "thumbnail": {
                "url": "https://cdn.catgirl.technology/img/eye_in_speech_bubble.png"
            },
        }

    @commands.check_any(
        checks.is_staff_member(),
        commands.has_guild_permissions(manage_messages=True)
    )
    @watch_user.command(aliases=["list", "ls"])
    async def show(self, ctx: Context) -> None:
        """Show a list of members presently having their activity reported on

        Requires being a staff member or having the Manage Messages guild permission"""

        embed_json = self._list(ctx.guild)
        if not embed_json:
            await ctx.reply("No watched users")
            return

        embed = Embed.from_dict(embed_json)
        await ctx.reply(embeds=[embed])

    @commands.check_any(
        checks.is_staff_member(),
        commands.has_guild_permissions(manage_messages=True)
    )
    @checks.is_feature_enabled("user_watch")
    @watch_user.command(aliases=["create"])
    async def add(self, ctx: Context, member: Member) -> None:
        """Start reporting on a user's activity

        Requires being a staff member or having the Manage Messages guild permission"""

        sql = """
            INSERT INTO user_watches
                (guild, "user", created_by)
            VALUES
                (%s, %s, %s)
        """
        values = (ctx.guild.id, member.id, ctx.author.id)
        with self.db.connection() as conn:
            try:
                conn.execute(sql, values)
            except IntegrityError:
                await ctx.fail_msg("User is already under watch", delete_after=30)

        await ctx.react_success()

    @commands.check_any(
        checks.is_staff_member(),
        commands.has_guild_permissions(manage_messages=True)
    )
    @checks.is_feature_enabled("user_watch")
    @watch_user.command(aliases=["rm", "del", "delete"])
    async def remove(self, ctx: Context, member: Member) -> None:
        """Stop reporting on a user's activity

        Requires being a staff member or having the Manage Messages guild permission"""

        is_watched = self._get.execute(ctx.guild.id, 0, member.id)
        if not is_watched:
            await ctx.fail_msg("User is not being watched")

        delete_sql = """
            DELETE FROM user_watches
            WHERE guild = %s AND "user" = %s
        """
        where_values = (ctx.guild.id, member.id)

        history_sql = """
            UPDATE user_watches_history SET
                deleted_at = current_timestamp,
                deleted_by = %s
            WHERE
                guild = %s
                AND "user" = %s
        """
        history_values = (ctx.author.id,) + where_values

        with self.db.connection() as conn:
            conn.execute(delete_sql, where_values)
            conn.execute(history_sql, history_values)

        channels = writable_channels_by_member(ctx.guild, member)
        for cid in channels:
            self._get.delete(ctx.guild.id, cid, member.id)

        await ctx.react_success()

    @ring.lru()
    def _get(self, gid: int, cid: int, uid: int) -> Tuple:
        sql = """
            SELECT "user" FROM user_watches
            WHERE guild = %s AND "user" = %s
        """
        values = (gid, uid)
        with self.db.connection() as conn:
            cursor = conn.execute(sql, values)
            if not cursor.rowcount:
                result = False
            else:
                result = cursor.fetchone()

        now = int(time())
        return (result, now)

    @_get.ring.key
    def _get_ring_key(self, a: int, b: int, c: int) -> str:
        return f"gid{a}cid{b}uid{c}"

    @commands.Cog.listener("on_watchable")
    async def handle_message(
        self,
        msg: Message,
        report_chan: MessageableChannel
    ) -> None:
        gid = msg.guild.id
        watch_interval = (
            # defaults to reporting every 15 minute, if not overriden
            self._bot.get_guild_key(gid, "user_watch", "interval") * 60
        )
        # enforce a minimum of 5 minutes
        if watch_interval < (5 * 60):
            watch_interval = (5 * 60)

        is_cached = self._get.has(msg.guild.id, msg.channel.id, msg.author.id)
        is_watched, last_active = self._get(msg.guild.id, msg.channel.id, msg.author.id)

        # the above functions as get or update cache, so
        # we delete the cache entry if the user isnt watched
        if not is_watched:
            self._get.delete(msg.guild.id, msg.channel.id, msg.author.id)
            return
        # trigger a report for first time posts
        elif is_watched and not is_cached:
            last_active = 0

        now = int(time())
        next_report = last_active + watch_interval
        if is_cached and now < next_report:
            return
        elif now > next_report:
            self._get.set(
                (msg.author.id, now),                           # new values
                msg.guild.id, msg.channel.id, msg.author.id     # cache params
            )

        text = str()

        if self._bot.get_guild_key(gid, "user_watch", "staff_mentions"):
            role = self._bot.get_privileged_role("staff", msg.guild)
            if role:
                text += f"{role.mention} "

        watch_user = f"{msg.author.mention} ({msg.author})"
        watch_chan = msg.channel.mention
        text += f"Watched user {watch_user} is active in {watch_chan} "
        text += Emojis.attachment if bool(msg.attachments) else ""
        text += Emojis.link if matching.has_url(msg.content) else ""
        text += f"\n{msg.jump_url}"

        await report_chan.send(text)


def setup(bot: Bot):
    bot.add_cog(WatchManager(bot))
