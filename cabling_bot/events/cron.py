import logging
import yaml
import aiocron

from textwrap import dedent
from discord.ext import commands

from typing import Optional
from discord import (Embed, Colour)
from cabling_bot.cmds.verifications import MemberVerificationManager
from cabling_bot.bot import (
    CablesCog as Cog,
    CablesBot as Bot,
    CablesContext as Context,
)

logger = logging.getLogger(__name__)


class CronManager(Cog):
    """Scheduled task management. See `help cron` for more info."""

    def __init__(self, bot: Bot) -> None:
        super().__init__()
        self._bot = bot

        self.jobs = dict()
        for name in dir(__class__):
            if not name.endswith('_job'):
                continue

            func = getattr(self, name)
            args = yaml.safe_load(
                dedent(func.__doc__)
            )
            name = name.removesuffix("_job")
            self.jobs[name] = dict(
                job=aiocron.crontab(
                    args["spec"],
                    func=func,
                ),
                spec=args["spec"],
                description=args["description"],
                state=True,
            )

    @property
    def verification_mgr(self) -> MemberVerificationManager:
        return self._bot.get_cog("MemberVerificationManager")

    def _list(self) -> Optional[dict]:
        return self.jobs

    def _exists(self, name: str) -> bool:
        if not self.jobs.get(name, False):
            return False

        return True

    def _fmt_details(self, name: str) -> str:
        details = self.jobs[name]
        fields = [
            f"**Name:** {name}",
            f"**Description:** {details['description']}",
            f"**Recurrence:** `{details['spec']}`",
            f"**Enabled:** {details['state']}",
        ]
        return "\n".join(fields)

    def _start(self, name: str) -> bool:
        if not self._exists(name):
            return False

        self.jobs[name]["job"].start()
        self.jobs[name]["state"] = True
        return True

    def _stop(self, name: str) -> bool:
        if not self._exists(name):
            return False

        self.jobs[name]["job"].stop()
        self.jobs[name]["state"] = False
        return True

    @commands.group(name="cron")
    async def scheduler(self, ctx: Context):
        """Commands for managing internal scheduled tasks."""
        if ctx.invoked_subcommand is None:
            await ctx.fail_msg("Invalid command passed")

    @commands.is_owner()
    @scheduler.command(aliases=["list"])
    async def show(self, ctx: Context) -> None:
        """Shows a list of the bot's scheduled tasks"""

        tasks = self._list()
        if not tasks:
            await ctx.fail_msg("No scheduled tasks loaded.")
            return

        embed = Embed(
            colour=Colour.teal(),
            title="Scheduled Tasks",
            description="A list of the bot's internal scheduled tasks and their state.",
        )

        for name, details in tasks.items():
            text = self._fmt_details(name)
            embed.add_field(
                name="Task Detail",
                value=text,
                inline=True
            )

        await ctx.reply(embeds=[embed])

    @commands.is_owner()
    @scheduler.command(aliases=["fetch"])
    async def get(self, ctx: Context, name: str) -> None:
        """Shows the schedule and state for a specific scheduled task."""

        if not self._exists(name):
            await ctx.fail_msg("Task doesn't exist.")
            return

        embed = Embed(
            title="Task Detail",
            description=self._fmt_details(name),
            colour=Colour.teal()
        )

        await ctx.reply(embeds=[embed])

    @commands.is_owner()
    @scheduler.command(aliases=["enable"])
    async def start(self, ctx: Context, name: str) -> None:
        """Enables a specific schedule task."""

        if not self._start(name):
            await ctx.fail_msg("Task doesn't exist.")
            return

        await ctx.react_success()

    @commands.is_owner()
    @scheduler.command(aliases=["disable"])
    async def stop(self, ctx: Context, name: str) -> None:
        """Disables a specific scheduled task."""

        if not self._stop(name):
            await ctx.fail_msg("Task doesn't exist.")
            return

        await ctx.react_success()

    @commands.is_owner()
    @scheduler.command(aliases=["shutdown"])
    async def stopall(self, ctx: Context) -> None:
        """Disables all scheduled tasks registered with the bot."""

        tasks = self.jobs.keys()
        if not tasks:
            await ctx.fail_msg("No scheduled tasks loaded.")
            return

        for task in tasks:
            self._stop(task)

        await ctx.react_success()

        text = "Tasks disabled: "
        text += ", ".join(self.jobs.keys())
        await ctx.reply(text)

    #############################
    # SCHEDULED TASK DEFINITONS #
    #############################

    # async def test_job(self):
    #     """
    #     spec: '*/1 * * * *'
    #     description: 'A simple demonstration of a functional cronjob.'
    #     """
    #
    #     await self._bot.wait_until_ready()
    #
    #     gid = self._bot.get_config("privileged.dev_guild")
    #     guild = discord.utils.find(lambda g: g.id == gid, self._bot.guilds)
    #     try:
    #         if guild.system_channel.can_send():
    #             text = "Scheduled task 'test' is executing."
    #             await guild.system_channel.send(text)
    #     except KeyError:
    #         pass

    async def autopurge_members_job(self):
        """
        spec: '12 */1 * * *'
        description: 'Purge unverified members across enabled guilds.'
        """
        self._bot.dispatch("member_purge")


def setup(bot: Bot):
    bot.add_cog(CronManager(bot))
