# Kudos to Tortoise-Community/Tortoise-BOT for the inspiration!
# https://github.com/Tortoise-Community/Tortoise-BOT/blob/master/bot/cogs/invite_tracker.py

from __future__ import annotations
from typing import TYPE_CHECKING

import json
import logging
import discord

from typing import (Optional, Union, NamedTuple)
from datetime import (datetime, timezone)
from discord.ext import commands
from discord.utils import format_dt, utcnow
from ..bot import (checks, constants)
from ..util.event_logging import EventLogController
from ..util.guild_invites import GuildInvites
from ..util.queue import QueueManager
from ..util.channels import check_configured_channel
from ..util.user import can_send_dm
from ..util.privileged import is_staff_member
from ..util.discord import list_bans, list_kicks

from discord.errors import NotFound
from discord import (Forbidden, InvalidArgument)
from discord import (
    Guild, AuditLogAction, Embed,
    Invite, Member, User,
)
from ..bot.exceptions import DMsDisallowed
from ..bot import (CablesCog as Cog, CablesContext as Context)

if TYPE_CHECKING:
    from cabling_bot.bot import CablesBot as Bot

logger = logging.getLogger(__name__)


class InviteTrackingManager(Cog):
    """Invite use and member join tracking. See `help invite` for more info."""

    def __init__(self, bot: Bot) -> None:
        super().__init__()

        self._bot = bot
        self.log = EventLogController(bot)
        self.queue = QueueManager(self._bot)

        self.tracker = GuildInvites(bot.db)
        bot.config_keys.new(
            "feature_flags",
            "invites",
            "(bool) Whether or not any member may use the `invite create` command.",
            False
        )
        bot.config_keys.new(
            "invites",
            "user_limit",
            "(int) How many concurrently active single-use invites a member is allowed.",
            3,
        )
        bot.config_keys.new(
            "invites",
            "join_channel",
            "(int) A channel ID for where joins from user-requested invites will land.",
            None
        )
        bot.config_keys.new(
            "notifications",
            "cables_invites",
            "(bool) Notify when an invite is created by cables",
            True
        )
        bot.config_keys.new(
            "notifications",
            "other_bot_invites",
            "(bool) Notify when invites are created by bots _other_ than cables",
            False
        )
        bot.config_keys.new(
            "notifications",
            "member_invites",
            "(bool) Notify when invites are created by your members",
            False
        )

    async def process_backlog(self):
        # Cursory data population- backfill missing invites and do revocations
        for guild in self._bot.guilds:
            self.queue.create_bucket(guild.id)

            response = await guild.invites()
            remote = self.tracker._transform_to_dict(response)
            known = await self.tracker._list_known_invites(
                guild, only_active=False
            )

            c = 0
            for code, invite in remote.items():
                if code not in known.keys():
                    self.queue.put(
                        guild.id,
                        self.tracker.add_new_invite, response[c]
                    )
                    break

                # TODO: query audit log for the revoker
                if invite["revoked"] > known[code]["revoked"]:
                    self.queue.put(
                        guild.id,
                        self.tracker.revoke_invite,
                        response[c], 0, utcnow()
                    )

                c += 1

    @commands.Cog.listener()
    async def on_queue_put(self, bucket: str):
        """Process the DB write queue when an item is put to a bucket."""
        await self.queue.consume(bucket)

    def _make_embed(
        self, title: str, text: str,
        icon: Optional[str] = constants.Icons.hash_blue,
        colour: Optional[discord.Colour] = discord.Colour.dark_blue()
    ) -> Embed:
        embed = Embed(description=text, colour=colour)
        embed.set_author(name=title, icon_url=icon)
        return embed

    def _get_requestor(
        self, created_by: int, requested_by: int
    ) -> Union[User, int]:
        if requested_by:
            member = self._bot.get_user(requested_by)
            if not member:
                member = requested_by
        else:
            member = self._bot.get_user(created_by)
            if not member:
                member = created_by
        return member

    async def _invite_check(
        self,
        guild: Guild,
        member: Member,
        code: str
    ) -> Union[bool, NamedTuple]:
        response = self.tracker.get_invite_detail(guild.id, code)
        if not response:
            return False

        is_staff = is_staff_member(self._bot, member, guild)
        is_owner = (member.id == guild.owner.id)
        created_by, requested_by = response[2], response[3]
        if not (
            member.id in [created_by, requested_by]
            or is_staff
            or is_owner
        ):
            return False

        return response

    ##############
    ## COMMANDS ##
    ##############

    @commands.group(aliases=["inv"])
    async def invite(self, ctx: Context) -> None:
        """Commands for managing guild invitations."""
        if ctx.invoked_subcommand is None:
            await ctx.fail_msg("Invalid command passed")

    @commands.guild_only()
    @invite.command(aliases=["describe"], extra={"cooldown": {"rate": 1, "per": 5}})
    async def get(self, ctx: Context, code: str) -> None:
        """Show details of a specific inivtation code for the guild.

        Staff members and those with the View Audit Log permission can see additional detail such as creator or revoker."""

        author = ctx.message.author
        check = await self._invite_check(ctx.message.guild, author, code)
        if not check:
            await ctx.fail_msg("Invalid input")
            return

        (uses, max_uses, created_by, requested_by,
            created_at, revoked_by, revoked_at) = check
        text = ""

        is_staff = is_staff_member(ctx.bot, author, ctx.guild)
        can_manage = author.guild_permissions.manage_channels
        if is_staff or can_manage:
            member = self._get_requestor(created_by, requested_by)
            text += f"**Requestor:** {member.mention} ({member})\n"

        text += "**Status:** "
        if not revoked_at:
            text += " Active"
            icon = constants.Icons.hash_green
            colour = constants.Colours.soft_green
        else:
            icon = constants.Icons.hash_red
            colour = constants.Colours.soft_red

            can_audit = author.guild_permissions.view_audit_log
            if is_staff or can_audit:
                if revoked_by:
                    revoker = ctx.bot.get_user(revoked_by)
                    text += " Revoked\n"
                    text += f"**Revoker:** {revoker.mention} ({revoker})"
                else:
                    text += " Expired"

        text += "\n**Uses:** "
        if not max_uses:
            max_uses = "∞"
        if not uses:
            text += f"Never (`{max_uses}` max)"
        else:
            text += f"`{uses}` times (`{max_uses}` max)"

        created_at: datetime = created_at.replace(tzinfo=timezone.utc)
        text += f"\n**Issued:** {format_dt(created_at, 'R')}"

        if revoked_at:
            revoked_at: datetime = revoked_at.replace(tzinfo=timezone.utc)
            text += f"\n**Invalidated:** {format_dt(revoked_at, 'R')}"
            # text += f"\n**Lifespan:** {created_at - revoked_at}"

        embed = self._make_embed(
            "Invitation Detail", text,
            icon=icon, colour=colour
        )
        await ctx.reply(embeds=[embed])

    # TODO: add pagination support
    @commands.guild_only()
    @commands.check_any(
        checks.is_staff_member(),
        commands.has_guild_permissions(manage_channels=True)
    )
    @invite.command(aliases=["ls", "show"])
    async def list(
        self, ctx: Context,
        only_active: Optional[bool] = True,
        only_used: Optional[bool] = False,
        sort: Optional[str] = "created_at"
    ) -> None:
        """List created guild invitations.

        only_active: whether to show all invites or only active ones
        * possible values: [false,true]
        * default: true

        only_used:  whether to show unused invites, or only those that've been used
        * possible values: [false,true]
        * default: false

        sort: how to sort the list of invites
        * possible values: [uses,created_at]
        * default: created_at

        Requires the staff role, or server-wide 'Manage Channels' permission."""

        if not (
            (only_active in [True, False])
            and (only_used in [True, False])
            and (sort in ["uses", "created_at"])
        ):
            raise InvalidArgument

        invites = await self.tracker.list_invites(
            ctx.message.guild,
            only_active=only_active,
            only_used=only_used,
            sort=sort
        )
        if not invites:
            text = "There aren't any invites matching that criteria. "
            text += "If you'd like to see all invites, try "
            text += f"`{ctx.bot.command_prefix}{ctx.command} false`"
            await ctx.fail_msg(text)
            return

        items, c = [], 1
        for code, created_by, requested_by, uses in invites:
            item = f"**{c}.** `{code}` "

            member = self._get_requestor(created_by, requested_by)
            item += f"({member.mention}): `{uses}`"

            items += [item]
            c += 1

        text = "\n".join(items)
        embed = self._make_embed("Guild Invitiations", text)
        await ctx.reply(embeds=[embed])

    @checks.is_feature_enabled("invites")
    @commands.guild_only()
    @commands.bot_has_guild_permissions(manage_channels=True)
    @invite.command(aliases=["new"])
    async def create(self, ctx: Context) -> None:
        """Generate a new single use invitation code."""

        guild, member = ctx.guild, ctx.author
        invites = self.tracker.list_member_invites(guild, member)
        count = 0 if not invites else len(invites)
        limit = ctx.get_guild_key(guild.id, "invites", "user_limit")
        if count >= limit:
            text = ("You've reached the limit of concurrent active invites. "
                    + "Revoke or use one before requesting another.")
            await ctx.fail_msg(text)
            return

        channel = await check_configured_channel(
            ctx, "invites", "join_channel"
        )
        if not channel:
            return

        if not await can_send_dm(member, self._bot.user):
            raise DMsDisallowed

        reason = f"Requested by {member} ({member.id})"
        invite = await channel.create_invite(max_uses=1, reason=reason)
        self.queue.put(
            guild.id,
            self.tracker.set_invite_requestor,
            invite, member
        )

        try:
            await member.send(invite.url)
        except Forbidden:
            await invite.delete()
            raise DMsDisallowed
        await ctx.react_success()

        if ctx.get_guild_key(guild.id, "notifications", "cables_invites"):
            await self.log.invite_create(
                invite, requestor=member, ctx=ctx
            )

    @commands.guild_only()
    @commands.bot_has_guild_permissions(manage_channels=True)
    @invite.command(aliases=["rm", "remove", "del", "delete"])
    async def revoke(self, ctx: Context, invite: Invite) -> None:
        """Revoke an invite.

        If you aren't the owner of the invite, requires staff role, or server-wide 'Manage Channels' permission."""

        if invite.guild != ctx.message.guild:
            await ctx.fail_msg("An invite must be managed from the server it belongs to")

        author = ctx.message.author
        check = await self._invite_check(ctx.message.guild, author, invite.code)
        if not check:
            await ctx.fail_msg("Invalid input")
            return

        try:
            reason = {
                "revoked_by": ctx.message.author.id,
                "revoked_at": datetime.timestamp(ctx.message.created_at)
            }
            await invite.delete(reason=json.dumps(reason))
        except NotFound:
            await ctx.fail_msg("Invalid input")

        await ctx.react_success()

    @commands.guild_only()
    @commands.bot_has_guild_permissions(manage_channels=True)
    @commands.check_any(
        checks.is_staff_member(),
        commands.has_guild_permissions(manage_channels=True)
    )
    @invite.command(aliases=["removeall", "rma", "deleteall", "revokeall"])
    async def lockdown(self, ctx: Context) -> None:
        """Revoke all invites.

        Requires the staff role, or server-wide 'Manage Channels' permission."""

        invites = await self.tracker._list_known_invites(ctx.message.guild)
        if not invites:
            await ctx.fail_msg("No active invites to revoke")

        if invites:
            await ctx.message.add_reaction("⏳")

        for code in invites.keys():
            invite = await ctx.bot.fetch_invite(code)
            reason = {
                "revoked_by": ctx.message.author.id,
                "revoked_at": datetime.timestamp(utcnow())
            }
            await invite.delete(reason=json.dumps(reason))

        await ctx.message.remove_reaction("⏳", ctx.message.guild.me)
        await ctx.react_success()

    def _ratio(self, a: int, b: int) -> int:
        raw = float(a / b)
        percent = raw * 100
        rounded = round(percent, 0)
        return int(rounded)

    @commands.guild_only()
    @invite.command(aliases=[], extra={"cooldown": {"rate": 1, "per": 30}})
    async def stats(
        self,
        ctx: Context
    ) -> None:
        """Shows a variety of statistics derived from invitation tracking.

        Bots are excluded from the member count."""

        guild = ctx.message.guild

        invites = await self.tracker.count_invites(guild)
        all_joins = await self.tracker.get_invite_uses(
            guild, unique=False
        )
        if not (invites or all_joins):
            await ctx.fail_msg("No tracked joins to aggregate")
            return
        text = f"**Invites:** `{invites}`\n"

        members = guild.member_count
        bots = len(await guild.integrations())
        chatters = members - bots
        text += f"**Current Users:** `{chatters}`\n"

        role = ctx.bot.get_guild_key(guild.id, "member_verification", "role")
        role = guild.get_role(role)
        if role:
            verified = len(role.members)
            verified_percent = self._ratio(verified, chatters)
            text += f"**Verified:** `{verified}` (`{verified_percent}%`)\n"

        unique_joins = await self.tracker.get_invite_uses(
            guild, unique=True
        )
        text += f"**Unique Joins:** `{unique_joins}`\n"
        partial_text = text

        if unique_joins > chatters:
            unretained = unique_joins - chatters
            unretained_percent = self._ratio(unretained, unique_joins)
            text += (f"**Unretained:** `{unretained}` "
                     + f"(`{unretained_percent}%`) ⏳\n")

        rejoins = all_joins - unique_joins
        if rejoins:
            rejoin_percent = self._ratio(rejoins, all_joins)
            rejoins_str = f"**Rejoins:** `{rejoins}` (`{rejoin_percent}%`)"
            text += rejoins_str

        embed = self._make_embed("Join Statistics", text)
        msg = await ctx.reply(embeds=[embed])

        banned = (await list_bans(guild) or list())
        kicked = (await list_kicks(guild) or list())
        removed = len(list(dict.fromkeys(banned + kicked)))
        removed_percent = self._ratio(removed, unique_joins)
        unretained = unique_joins - chatters - removed
        unretained_percent = self._ratio(unretained, unique_joins)
        text = partial_text
        if removed:
            text += (f"**Removed:** `{removed}` "
                     + f"(`{removed_percent}%`)\n")
        text += (f"**Unretained:** `{unretained}` "
                 + f"(`{unretained_percent}%`)\n"
                 + rejoins_str)
        embed = self._make_embed("Join Statistics", text)
        await msg.edit(embeds=[embed])

    ###############
    ## LISTENERS ##
    ###############

    @commands.Cog.listener()
    async def on_invite_create(self, invite: Invite) -> None:
        self.queue.put(
            invite.guild.id,
            self.tracker.add_new_invite, invite
        )

        if (
            invite.inviter != self._bot.user
            and ((
                invite.inviter.bot
                and self._bot.get_guild_key(
                    invite.guild.id,
                    "notifications", "other_bot_invites"
                )
            ) or (
                not invite.inviter.bot
                and self._bot.get_guild_key(
                    invite.guild.id,
                    "notifications", "member_invites"
                )
            ))
        ):
            await self.log.invite_create(invite)

    @commands.Cog.listener()
    async def on_invite_delete(self, invite: Invite) -> None:
        now = utcnow()
        revoked_by, revoked_at = 0, None
        # NOTE: querying the audit log is necessary for a couple reasons:
        # - expiration date isn't returned by the gateway for this event
        # - revoked invites aren't returned by service API requests, either
        async for entry in invite.guild.audit_logs(
            action=AuditLogAction.invite_delete,
            limit=10
        ):
            target: Invite = entry.target
            if target.code != invite.code:
                continue

            try:
                reason: dict = json.loads(entry.reason)
                revoked_by = reason["revoked_by"]
                revoked_at = datetime.utcfromtimestamp(reason["revoked_at"])
            except (json.JSONDecodeError, TypeError):
                revoked_by = entry.user.id
                revoked_at = entry.created_at

        if not revoked_at:
            revoked_at = now

        await self.tracker.revoke_invite(
            invite, revoked_by, revoked_at
        )

        revoker = invite.guild.get_member(revoked_by)
        if (
            entry.user.id == self._bot.user.id
            or (revoked_by != self._bot.user.id
                and revoker
                and ((
                    revoker.bot
                    and self._bot.get_guild_key(
                        invite.guild.id,
                        "notifications", "other_bot_invites"
                    )
                ) or (
                    revoker
                    and not revoker.bot
                    and self._bot.get_guild_key(
                        invite.guild.id,
                        "notifications", "member_invites"
                    )
                )))
        ):
            action = "revoked"
            if not revoked_by:
                revoked_by = "service"
                action = "deleted"
            await self.log.invite_delete(invite, agent=revoked_by, action=action)

    # @commands.Cog.listener()
    # async def on_guild_channel_delete(self, channel: GuildChannel) -> None:
    #     # TODO: need to log channel in db when invites are created
    #     # ...

    #     # TODO: rework this to accept a code?
    #     await self.tracker.revoke_invite(
    #         invite.code, 0, utcnow()
    #     )

    #     await self.log.invite_delete(invite, agent="service", action="deleted")

    # Called from meta-dispatcher's handling of the MEMBER_JOIN event
    async def invite_correlation(self, member: Member) -> tuple:
        await self.tracker.store_join(member)

        # retries occur within this referenced method
        invite = await self.tracker.track_join(member)
        if invite:
            self.queue.put(
                member.guild.id,
                self.tracker.track_invite_use,
                member, invite
            )

        # default/fallback notification level sends to system channel
        level: Optional[int] = None
        # emergency notification level uses @e and DMs guild owner
        if member.public_flags.staff:
            level = 0
        # warning notification level uses @here in system channel
        elif member.public_flags.value & constants.UserFlags.spammer:
            level = 1

        route: Optional[str] = None
        if invite:
            route = "Invitation"
        # NOTE: forcible joins cannot be detected if guild is discoverable!
        elif not invite and "DISCOVERABLE" in member.guild.features:
            route = "Server Discovery"
        else:
            # if invite correlation fails, this will also trigger :(
            guild = member.guild.id
            sajoin_guilds = self._bot.get_config(
                "invites.force_join_detection_guilds", []
            )
            if not sajoin_guilds or guild not in sajoin_guilds:
                route = "Application/Forcible"
            else:
                route = "Forcible"
                level = 0

        return (member, route, invite, level)


def setup(bot: Bot):
    bot.add_cog(InviteTrackingManager(bot))
