import logging
import inspect
from discord.ext import commands
import discord.utils
from cabling_bot.util import dev_guild
from cabling_bot.util.discord import check_webhook
from cabling_bot.util.user import can_send_dm
from cabling_bot.util.channels import is_channel_sendable

from cabling_bot.events.message_hello import SayHello
from cabling_bot.events.message_media_expand import MediaExpand
from cabling_bot.events.message_roll import RollManager
from cabling_bot.events.message_amulets import AmuletManager
from cabling_bot.events.message_watch_user import WatchManager
from cabling_bot.events.invites import InviteTrackingManager

from discord import (
    DMChannel, Message, VoiceState,
    Guild, Member, Thread,
    AuditLogAction,
    NotFound, Forbidden,
)
from cabling_bot.bot import (
    CablesCog as Cog,
    CablesBot as Bot,
    constants
)

logger = logging.getLogger(__name__)


class SocketEventManager(Cog):
    """Internal custom WS event dispatcher."""

    def __init__(self, bot: Bot, hidden=True) -> None:
        super().__init__()
        self._bot = bot

    @property
    def greeting_mgr(self) -> SayHello:
        return self._bot.get_cog("SayHello")

    @property
    def preview_mgr(self) -> MediaExpand:
        return self._bot.get_cog("MediaExpand")

    @property
    def roll_mgr(self) -> RollManager:
        return self._bot.get_cog("RollManager")

    @property
    def amulet_mgr(self) -> AmuletManager:
        return self._bot.get_cog("AmuletManager")

    @property
    def watch_mgr(self) -> WatchManager:
        return self._bot.get_cog("WatchManager")

    @property
    def invite_mgr(self) -> InviteTrackingManager:
        return self._bot.get_cog("InviteTrackingManager")

    @commands.Cog.listener()
    async def on_message(self, msg: Message) -> None:
        """Meta-dispatcher for MESSAGE_CREATE event."""
        logger.info(
            await self._bot.oplog.compose(
                'messages',
                inspect.stack()[0].function,
                messages=[msg]
            )
        )

        ## checks
        if ((msg.author == self._bot.user or msg.author.bot)
                or (msg.content.startswith(self._bot.command_prefix))):
            return

        ## hi2u, frend!!
        greeting = self.greeting_mgr.parse_message(msg)
        if greeting:
            self._bot.dispatch("greetz", msg, greeting)

        ## guild-only from here down!
        if type(msg.channel) is DMChannel:
            return

        ## previews
        previewable = self.preview_mgr.parse_message(msg)
        if previewable:
            url, fqdn = previewable
            self._bot.dispatch("previewable", msg, url, fqdn)

        ## swag: GETs
        get = self.roll_mgr.parse_message(msg)
        if get:
            fmt, match = get
            self._bot.dispatch("message_get_detected", msg, fmt, match)

        ## swag: Amulets
        amulet = self.amulet_mgr.identify_amulet(msg.clean_content)
        if amulet:
            self._bot.dispatch("message_amulet_detected", msg, amulet)

        ## user activity watch
        _channel = self.watch_mgr.report_channel(msg.guild)
        _sendable = await is_channel_sendable(self._bot, _channel)
        if self.watch_mgr.enabled(msg.guild) and _sendable:
            self._bot.dispatch("watchable", msg, _channel)

    @commands.Cog.listener()
    async def on_message_edit(self, before: Message, after: Message) -> None:
        """Meta-dispatcher for MESSAGE_EDIT event."""
        logger.info(
            await self._bot.oplog.compose(
                'messages',
                inspect.stack()[0].function,
                before=before, after=after
            )
        )

    @commands.Cog.listener()
    async def on_message_delete(self, msg: Message) -> None:
        """Meta-dispatcher for MESSAGE_DELETE event."""
        logger.info(
            await self._bot.oplog.compose(
                'messages',
                inspect.stack()[0].function,
                messages=[msg]
            )
        )

    # @commands.Cog.listener()
    # async def on_raw_message_delete(self, msg: RawMessageDeleteEvent) -> None:
    #     """Meta-dispatcher for MESSAGE_DELETE events targetting a non-cached message."""
    #     logger.info(
    #         await self._bot.oplog.compose(
    #             'messages',
    #             inspect.stack()[0].function,
    #             messages=[msg]
    #         )
    #     )

    @commands.Cog.listener()
    async def on_bulk_message_delete(self, messages: list[Message]) -> None:
        """Meta-dispatcher for BULK_MESSAGE_DELETE event."""
        logger.info(
            await self._bot.oplog.compose(
                'messages',
                inspect.stack()[0].function,
                messages=messages
            )
        )

    # @commands.Cog.listener()
    # async def on_raw_bulk_message_delete(
    #     self,
    #     messages: RawBulkMessageDeleteEvent
    # ) -> None:
    #     """Meta-dispatcher for BULK_MESSAGE_DELETE events targetting non-cached messages."""
    #     logger.info(
    #         await self._bot.oplog.compose(
    #             'messages',
    #             inspect.stack()[0].function,
    #             messages=messages
    #         )
    #     )

    @commands.Cog.listener("on_member_update")
    async def handle_member_update(self, before: Member, after: Member) -> None:
        """Meta-dispatcher for the MEMBER_UPDATE event."""
        if (
            before.display_avatar != after.display_avatar
            and before.nick != after.nick
            and before == after
        ):
            self._bot.dispatch("member_profile_update", before, after)
        # TODO: __str__ and Member.name are always the same before and after
        # we can't implement this without tracking ourselves in the db
        # elif before != after:
        #     self._bot.dispatch("member_tag_update", before, after)
        elif not before.timed_out and after.timed_out:
            self._bot.dispatch("member_timeout", after)
        elif before.public_flags and after.public_flags:
            before_flags = before.public_flags.value
            after_flags = after.public_flags.value
            if (
                not (before_flags & constants.UserFlags.spammer)
                and (after_flags & constants.UserFlags.spammer)
            ):
                self._bot.dispatch("member_flagged_spammer", before)
            elif (
                not (before_flags & constants.UserFlags.underage_blocked)
                and (after_flags & constants.UserFlags.underage_blocked)
            ):
                self._bot.dispatch("member_flagged_underage", before)

    @commands.Cog.listener("on_member_join")
    async def handle_join_event(self, member: Member) -> None:
        """Meta-dispatcher for GUILD_MEMBER_ADD event."""

        if member.bot:
            return

        if member.guild.id == self._bot.dev_guild.id:
            await dev_guild.handle_join(self._bot, member)

        correlation = await self.invite_mgr.invite_correlation(member)
        self._bot.dispatch("guild_member_join", member, correlation)

    @commands.Cog.listener("on_member_remove")
    async def handle_part_event(self, member: Member) -> None:
        """Meta-dispatcher for GUILD_MEMBER_REMOVE event."""

        now = discord.utils.utcnow()

        # edge case: bot de-integration
        if (
            member == self._bot.user
            and member.guild not in self._bot.guilds
        ):
            return
        elif (
            member != self._bot.user
            and member.bot
        ):
            async for entry in member.guild.audit_logs(
                action=AuditLogAction.integration_delete,
                oldest_first=False,
                limit=3
            ):
                diff = abs((now - entry.created_at).seconds)
                if diff > 10:
                    break
                if member.id == entry.target.id:
                    return

        # dispatch: MEMBER_KICK
        async for entry in member.guild.audit_logs(
            action=AuditLogAction.kick,
            oldest_first=False,
            limit=3
        ):
            diff = abs((now - entry.created_at).seconds)
            if diff > 10:
                break
            if member.id == entry.target.id:
                self._bot.dispatch(
                    "member_kick",
                    member.guild,
                    member,        # target
                    entry.user,    # issuer
                    entry.reason,
                )
                return

        # ignore bans; handled with MEMBER_BAN event in `cmds/bans.py`
        try:
            await member.guild.fetch_ban(member._user)
            return
        except NotFound:
            pass

        # dispatch: MEMBER_FLAGGED_UNDERAGE
        flags = member.public_flags.value
        if flags & constants.UserFlags.underage_blocked:
            self._bot.dispatch("member_flagged_underage", member)

        # dispatch: GUILD_MEMBER_PART
        self._bot.dispatch("guild_member_part", member)

    @commands.Cog.listener("on_voice_state_update")
    async def handle_voice_state_update(self, member: Member, before: VoiceState, after: VoiceState) -> None:
        """Meta-dispatcher for VOICE_STATE_UPDATE event."""
        if (
            before.channel is None
            and after.channel
        ):
            try:
                channel = await member.guild.fetch_channel(after.channel.id)
            except Forbidden:
                return
            self._bot.dispatch("guild_vc_join", member, channel)
        elif (
            before.channel
            and after.channel is None
        ):
            try:
                channel = await member.guild.fetch_channel(before.channel.id)
            except Forbidden:
                return
            self._bot.dispatch("guild_vc_part", member, channel)

    @commands.Cog.listener("on_guild_update")
    async def handle_guild_update(self, before: Guild, after: Guild) -> None:
        """Meta-dispatcher for the GUILD_UPDATE event."""
        if before.owner != after.owner:
            # `after` Guild object is sparse, and doesn't contain `system_channel` attribute
            self._bot.dispatch("guild_ownership_transfer", before, before.owner, after.owner)
        elif set(before.features) != set(after.features):
            common = set(before.features) - set(after.features)
            new = set(after.features) - set(before.features)
            self._bot.dispatch("guild_features_update", after, common, new)
        elif (
            before.nsfw_level != after.nsfw_level
            and not after.nsfw_level.default
        ):
            self._bot.dispatch("guild_nsfw_classified", before, after)

    @commands.Cog.listener("on_thread_update")
    async def handle_thread_update(self, before: Thread, after: Thread) -> None:
        """Meta-dispatcher for THREAD_UPDATE event."""

        thread = after.guild.get_thread(after.id)

        # NOTE: we can't do `(not before.archived and after.archived)`
        #   before for some reason, they're both true when a thread
        #   is archived??
        now = discord.utils.utcnow()
        diff = abs((now - after.archive_timestamp).seconds)
        if after.archived and diff < 10:
            self._bot.dispatch("thread_archived", thread)
            return

        # BUG: `Thread.me` and `Thread.members` are
        # inaccurately returned by the gateway
        members = await after.fetch_members()

        if (
            (before.archived and not after.archived)
            and self._bot.user not in members
        ):
            self._bot.dispatch("thread_unarchived", thread)

    async def _init_guild_configs(self, guild: Guild) -> None:
        _guild = self.init_guild_config(self._bot.db, guild.id)
        _settings = _guild.list(module=None, key=None)
        if not _settings:
            self._bot.dispatch("guild_setup", guild, _guild)
        self._bot.dispatch("guild_ready", guild, _guild)

    @commands.Cog.listener("on_guild_join")
    async def handle_guild_join(self, guild: Guild) -> None:
        await self._init_guild_configs(guild)

    @commands.Cog.listener("on_guild_available")
    async def handle_guild_up(self, guild: Guild) -> None:
        await self._init_guild_configs(guild)

    # TODO: manage a cache of the guild's webhooks, updating when
    # they're modified or removed. would allow us to be better
    # assured of proof positive guild deletion
    @commands.Cog.listener("on_guild_remove")
    async def handle_nuke_event(self, guild: Guild) -> None:
        if not self._bot.get_guild_key(guild.id, "feature_flags", "defcon"):
            return

        webhook = self._bot.get_guild_key(guild.id, "defcon", "webhook_id")
        token = self._bot.get_guild_key(guild.id, "defcon", "webhook_token")
        if not (webhook and token):
            return

        state = check_webhook(self, webhook, token)
        # case: integration removed voluntarily
        if state is False:
            # e = f"GUILD_REMOVE ({guild.id}): Unhandled webhook payload: {json}"
            # logger.debug(e)
            return
        elif state is True:
            # TODO: try admins if guild owner doesn't exist
            if not guild.owner:
                return
            if await can_send_dm(guild.owner, self._bot.user):
                text = (f"De-integrated and removed from *{guild.name}*\n"
                        + "So long and thanks for all the fish! 👋")
                try:
                    await guild.owner.send(text)
                except Forbidden:
                    pass
        # case: webhook removed or guild deleted
        elif state is None:
            self._bot.dispatch("guild_delete", guild)


def setup(bot: Bot):
    bot.add_cog(SocketEventManager(bot))
