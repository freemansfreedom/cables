import logging
import random
from typing import Optional

from discord import (ApplicationContext, Forbidden, Guild, Member, Status)
from cabling_bot.bot import (
    CablesCog as Cog,
    CablesBot as Bot,
    constants,
)

from discord.ext import commands
from discord.utils import (find, format_dt, utcnow)
from cabling_bot.util.embeds import EmbedManager

logger = logging.getLogger(__name__)


class GuildReportsManager(Cog):
    """Facility for users to report potentially problematic activity to server staff. \
    See `help anonreport` for more info."""
    def __init__(self, bot: Bot) -> None:
        super().__init__()
        self._bot = bot
        self.log = EmbedManager(bot)

    def list_moderators(self, guild: Guild) -> Optional[list[Member]]:
        role = self._bot.get_privileged_role("staff", guild)
        if not role:
            return None
        mods = find(lambda m: role in m.roles, guild.members)
        return mods

    def list_online_moderators(self, guild: Guild) -> Optional[list[Member]]:
        mods = self.list_moderators(guild)
        if not mods:
            return None
        if type(mods) is Member:
            return mods
        return find(lambda o: o.status == Status.online, mods)

    def random_online_moderator(self, guild: Guild):
        """Returns a random online moderator of a guild, or the guild owner."""
        mods = self.list_online_moderators(guild)
        if mods and type(mods) is list:
            moderator: Member = random.choice(mods)
            return moderator
        return mods or guild.owner

    @commands.slash_command(guild_only=True)
    async def anonreport(self, ctx: ApplicationContext):
        """Anonymously draw the attention of a moderator to the channel."""
        moderator = self.random_online_moderator(ctx.guild)
        when = format_dt(utcnow(), "R")
        where = f"[#{ctx.channel.name}]({ctx.channel.jump_url})"
        fields = [
            dict(name="Who", value="Anonymous", inline=True),
            dict(name="When", value=when, inline=True),
            dict(name="Where", value=where, inline=True),
        ]
        try:
            await self.log.send(
                constants.Colours.soft_orange,
                "User report",
                "Your attention is required by request of a member!",
                guild=ctx.guild,
                title_icon=constants.Icons.exclaim_red,
                fields=fields,
                mentionable=moderator,
            )
        except Forbidden:
            await ctx.respond("Failed to issue report due to permissions error!")
        reply = f"{moderator.mention} has been alerted of their attention being required."
        await ctx.respond(reply, ephemeral=True)


def setup(bot: Bot):
    bot.add_cog(GuildReportsManager(bot))
