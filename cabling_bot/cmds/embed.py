import logging
import json
from typing import Optional, Text, Union

import discord
from discord.channel import TextChannel
from discord.threads import Thread
from discord.errors import HTTPException
from discord.ext import commands
from discord import Message, VoiceChannel
from discord.ext.commands.errors import UserInputError

from cabling_bot.util import transform
from cabling_bot.bot import CablesContext as Context, CablesBot as Bot

logger = logging.getLogger(__name__)


@commands.group()
async def embed(ctx: Context):
    """Create and edit embed posts from JSON/markdown"""
    if ctx.invoked_subcommand is None:
        await ctx.fail_msg(
            "Invalid subcommand passed.\n"
            + f"Try doing `{ctx.prefix}help embed` for more info.",
        )


@embed.command()
async def simple(ctx: Context, title: str, description: str) -> None:
    """Create a simple embed post with only a title and description"""
    embed = discord.Embed(title=title, description=description)
    await ctx.react_success()
    await ctx.send(embed=embed)


async def get_user_json(msg: Message, raw_json: Optional[str]) -> dict:
    """Grab the first attachment of a post and decode its json into a dict

    :raises UserInputError: Bad or missing json attachment
    """
    contents = None
    try:
        raw_contents = raw_json if raw_json else await msg.attachments[0].read()
        contents = json.loads(raw_contents)
    except IndexError:
        raise UserInputError("Missing json attachment!")
    except json.JSONDecodeError as e:
        raise UserInputError(
            "Uploaded attachment has malformed json!\n"
            + transform.codeblock(e)
        )
    else:
        return contents


@embed.command(name="json")
async def embed_json(ctx: Context, *, raw_json: Optional[str]) -> None:
    """Create an embed post from json

    Json information can be passed either as an attached file or inline.
    """
    try:
        contents = await get_user_json(ctx.message, raw_json)
        embed = discord.Embed.from_dict(contents)
        await ctx.send(embed=embed)
    except UserInputError as e:
        await ctx.fail_msg(str(e))
    except HTTPException as e:
        await ctx.fail_msg(
            "Failed to send embed:\n" + transform.codeblock(e),
        )
    else:
        await ctx.react_success()


@embed.command(name="edit")
async def embed_edit(
    ctx: Context,
    msg: Message,
    *,
    raw_json: Optional[str]
) -> None:
    """Edit an existing message embed with updated json information

    Json information can be passed either as an attached file or inline.
    """

    if len(msg.embeds) != 1:
        fail_text = (
            "Messsage either has zero or multiple embeds, "
            + "and I'm unfortuntaely not smart enough to handle "
            + "editing posts with multiple embeds.",
        )
        await ctx.fail_msg(fail_text)
        return

    try:
        edits_dict = await get_user_json(ctx.message, raw_json)
        embed = msg.embeds[0].to_dict()
        embed |= edits_dict
        await msg.edit(embed=discord.Embed.from_dict(embed))
    except UserInputError as e:
        await ctx.fail_msg(str(e))
    except HTTPException as e:
        await ctx.fail_msg("Failed to edit embed:\n" + transform.codeblock(e))
    else:
        await ctx.react_success()


@embed.command(name="dump")
async def embed_dump(ctx: Context, msg: Message) -> None:
    """Dump the json representation of an embed"""

    if len(msg.embeds) < 1:
        await ctx.fail_msg("Message doesn't have an embed.")
        return

    embed_json = json.dumps(msg.embeds[0].to_dict())
    jump_embed = discord.Embed(description=f"[Jump to message]({msg.jump_url})")
    await ctx.reply(content=transform.codeblock(embed_json), embed=jump_embed)


@embed.command(name="copy", aliases=["cp"])
async def embed_copy(
    ctx: Context,
    msg: Message,
    channel: Union[TextChannel, Thread, VoiceChannel]
) -> None:
    """Copy an embed post to another channel or thread"""

    if len(msg.embeds) < 1:
        await ctx.fail_msg("Message doesn't have an embed.")
        return

    copy = await channel.send(embed=msg.embeds[0])
    if copy.channel != msg.channel:
        jump_embed = discord.Embed(description=f"[Jump to message]({copy.jump_url})")
        await ctx.reply(
            content=f"Copied message to {channel}",
            embed=jump_embed
        )

    await ctx.react_success()


def setup(bot: Bot):
    bot.add_command(embed)
