import logging
from typing import (Optional, Union)
from discord import (Embed, Forbidden, Member, User, Guild)
from cabling_bot.bot import (
    CablesCog as Cog,
    CablesBot as Bot,
    CablesContext as Context,
    checks
)

from discord.ext import commands
from discord.utils import format_dt
from cabling_bot.bot import constants
from cabling_bot.bot.errors import is_privileged_invoke
from cabling_bot.util import transform
import cabling_bot.util.user
import cabling_bot.util.discord

logger = logging.getLogger(__name__)


class InspectManager(Cog):
    """Commands for power users and the paranoid. See `help inspect` for more info."""

    def __init__(self, bot: Bot) -> None:
        super().__init__()
        self._bot = bot

    @commands.group(aliases=[])
    async def inspect(self, ctx: Context):
        """Commands for power users and the paranoid."""
        if ctx.invoked_subcommand is None:
            await ctx.fail_msg("Invalid command passed")

    # TODO: convert to embed, using fields
    @inspect.command(aliases=["u"])
    async def user(self, ctx: Context, user: Optional[Union[Member, User]] = None) -> None:
        """Inspect a Discord user account."""
        if not user:
            user = ctx.author
        text = ""

        acct_age, age_warning = format_dt(user.created_at, 'R'), str()
        if not cabling_bot.util.user.account_age_check(ctx.bot, user, ctx.guild):
            age_warning += constants.Emojis.warning
        text += f"**Account Birth:** {acct_age} {age_warning}\n"

        badges = cabling_bot.util.user.collect_badges(user)
        if badges:
            text += f"**Sash:** {' '.join(badges)}\n"

        # privileged: bot operators only
        if await is_privileged_invoke(ctx):
            mutual_guilds = user.mutual_guilds
            if mutual_guilds:
                text += "**Guilds:**\n"
            for guild in mutual_guilds:
                attributes = cabling_bot.util.discord.guild_attributes(
                    guild, user
                )
                text += (f"{constants.Symbols.mdash} "
                         + f"__{guild.name}__ ({guild.id})")
                tab = "     "
                if any(attributes.values()):
                    text += f"\n{tab}*Sash:*"
                    for v in attributes.values():
                        text += f" {v}" if v else ""
                created_at = format_dt(guild.created_at, "R")
                text += f"\n{tab}*Created:* {created_at}"
                joined_at = format_dt(guild.me.joined_at, "R")
                text += f"\n{tab}*Joined:* {joined_at}\n"

        await ctx.reply(text, mention_author=False)

    @checks.is_guild_available()
    @checks.has_guild_membership()
    @inspect.command(aliases=["g", "s", "server"])
    async def guild(self, ctx: Context, guild: Optional[Guild] = None) -> None:
        """Inspect a Discord guild that the bot is a member of.

        For non-privileged users, they too must be a member."""

        if not guild:
            guild = ctx.guild

        reply = await ctx.reply("⏳", mention_author=False)
        loading = "⏳ Loading"

        # TODO: get the color from the guild's icon
        description = guild.description or ""
        embed = Embed(
            color=constants.Colours.thistle,
            title=f"{guild.name}",
            description=description,
        )

        # only add a link if we're not querying
        # the guild that the cmd was issued on
        if guild != ctx.guild:
            embed.url = guild.jump_url

        if guild.icon:
            embed.set_thumbnail(url=guild.icon.url)

        created_at = format_dt(guild.created_at, "R")
        joined_at = format_dt(guild.me.joined_at, "R")
        dates = (
            f"🎂 {created_at}\n"
            + f"🛬 {joined_at}"
        )

        admins = cabling_bot.util.discord.get_administrators(guild)
        jannies = cabling_bot.util.discord.get_janitors(guild)
        leadership = (
            f"👑 {guild.owner.mention}\n"
            + (f"🪄 `{len(admins) - 1}` Admins\n"
               if len(admins) > 1 else "")
            + (f"🧹 `{len(jannies)}` Jannies\n"
               if len(jannies) > 1 else "")
        )

        emoji_emoji = ("🙂" if guild.emojis else "🙁")
        assets = (f"{emoji_emoji} `{len(guild.emojis)}` Emoji\n"
                  + f"🥟 `{len(guild.stickers)}` Stickers")

        channels = str()
        text_channels = cabling_bot.util.discord.get_text_channels(guild)
        if text_channels:
            channels += f"💬 `{len(text_channels)}` Text\n"
        nsfw_channels = [c for c in text_channels if c.is_nsfw()]
        if nsfw_channels:
            channels += f"🔞 `{len(nsfw_channels)}` R-18\n"
        if guild.voice_channels or guild.stage_channels:
            voice_channels = len(
                cabling_bot.util.discord.get_voice_channels(guild)
            )
            channels += f"🔉 `{voice_channels}` Voice\n"

        tup = cabling_bot.util.discord.get_member_breakdown(guild)
        bots, humans, ratio = tup
        bot_farm = cabling_bot.util.discord.has_excessive_bots(
            bots, humans, ratio
        )
        online = len(cabling_bot.util.discord.list_members(
            guild, constants.UserStatuses.online
        ))
        # present = len(cabling_bot.util.discord.list_members(
        #     guild, constants.UserStatuses.present
        # ))
        # idle = len(cabling_bot.util.discord.list_members(
        #     guild, constants.UserStatuses.afk
        # ))

        members = (f"👥 `{online}` (`{humans}`)\n"
                   # + f"🟢 `{present}` Online\n"
                   # + f"🟠 `{idle}` AFK\n"
                   + f"🤖 `{bots}` Bots"
                   + (" " + constants.Emojis.warning
                      if bot_farm else "")
                   + "\n")

        if guild.premium_subscribers:
            members += f"🚀 `{guild.premium_subscription_count}` Boosters\n"

        prune_estimate = 0
        try:
            prune_estimate = await guild.estimate_pruned_members(days=30)
        except Forbidden:
            pass
        if prune_estimate:
            members += f"\n💤 `{prune_estimate}` Prunable"

        roles = str()
        colored_roles = [r for r in guild.roles if r.colour.value]
        roles = (
            (f"🖌 `{len(colored_roles)}` Colored\n" if colored_roles else "")
            + (f"🧮 `{len(guild.roles)}` Total")
        )

        settings = await cabling_bot.util.discord.get_guild_settings(guild)
        filter_state, mfa_level, widget_state, notes = settings
        filter_state = constants.GuildNsfwFilter[filter_state]
        staff_mfa = transform.bool_to_emoji(bool(mfa_level))
        widget = transform.bool_to_emoji(bool(widget_state))
        notes = constants.GuildNoteLevel[notes]
        # if not widget_state:
        #     widget += " Widget"
        # else:
        #     widget += f" [Widget]({widget_state.json_url})"
        settings = f"""
            {notes.value} Default Notifs.
            {filter_state.value} R-18 filter
            {staff_mfa} Staff MFA
            {widget} Widget
        """

        # NOTE: API returns the vanity url but this isn't exposed by the library
        #   also, we hit the field limit, so adding that would require restructuring
        fields = [
            dict(name="ID", value=guild.id, inline=False),
            dict(name="Dates", value=dates),
            # dict(name="Owner", value=guild.owner.mention),
            # dict(name="Admins", value=admins),
            dict(name="Leadership", value=leadership),
            dict(name="Assets", value=assets),
            dict(name="Members", value=(members + loading)),
            dict(name="Channels", value=(channels + loading)),
            dict(name="Roles", value=roles),
            dict(name="Settings", value=settings),
        ]

        for f in fields:
            try:
                inline = f["inline"]
            except KeyError:
                inline = True
            embed.add_field(
                name=f["name"],
                value=f["value"],
                inline=inline,
            )

        features = cabling_bot.util.discord.guild_attributes(guild)
        features = " ".join([a for a in features.values() if a])
        if features:
            embed.insert_field_at(index=8, name="Features", value=features)

        ## NOTE: this shrinks the embed dimensions
        ##       while causes field text to overflow,
        ##       and the whole think to look jank 😩
        # if guild.banner:
        #     embed.set_image(url=guild.banner.url)

        # post initial data
        await reply.edit(content=None, embed=embed)

        # wait for remote calls, then update again
        bans = await cabling_bot.util.discord.list_bans(guild)
        if bans:
            members += f"🚫 `{len(bans)}` Bans\n"
        embed.set_field_at(index=4, name="Members", value=members)
        await reply.edit(content=None, embed=embed)

        _, threads, _ = await cabling_bot.util.discord.get_threads(guild)
        if threads:
            channels += f"🧵 `{len(threads)}` Threads\n"
        embed.set_field_at(index=5, name="Channels", value=channels)
        await reply.edit(content=None, embed=embed)


def setup(bot: Bot):
    bot.add_cog(InspectManager(bot))
