import asyncio
from distutils.util import strtobool
import logging
import psycopg
import psycopg_pool

from discord.ext import commands
from discord.utils import find
from cabling_bot.bot.errors import report_database_error, report_guild_error
from cabling_bot.bot import constants
from cabling_bot.util import transform
from cabling_bot.util.embeds import split_embeds
from cabling_bot.bot import (
    checks,
    CablesContext as Context,
    CablesCog as Cog,
    CablesBot as Bot,
)

from discord import (Message, Role, TextChannel, Guild, Member)
from discord.embeds import Embed
from discord.errors import Forbidden
from typing import (NamedTuple, Optional, Sequence, Union, List)
from discord.raw_models import (RawMessageDeleteEvent, RawReactionActionEvent)
from cabling_bot.util.embeds import EmbedManager
from cabling_bot.util import privileged

logger = logging.getLogger(__name__)

Emoji = str


class RoleReact(Cog):
    """Commands for managing role-reacts. See `help role-react` for more info."""

    def __init__(self, bot: Bot) -> None:
        super().__init__()
        self._bot = bot
        self.db: psycopg_pool.ConnectionPool = bot.db
        self.log = EmbedManager(bot)

        q = """\
            -- Messages
            CREATE TABLE IF NOT EXISTS role_react_messages (
                id          serial      PRIMARY KEY,
                guild       bigint      NOT NULL,
                channel     bigint      NOT NULL,
                message     bigint      NOT NULL,
                author      bigint      NOT NULL,
                mutex       bool        NOT NULL DEFAULT 'false',
                sweep       bool        NOT NULL DEFAULT 'false'
            );
            CREATE INDEX IF NOT EXISTS idx_rrm_guild_chan
                ON role_react_messages (guild, channel);
            CREATE UNIQUE INDEX IF NOT EXISTS idx_rrm_msg
                ON role_react_messages (message);
            -- Reactions
            CREATE TABLE IF NOT EXISTS role_react_reactions (
                id          serial      PRIMARY KEY,
                msg_ref     bigint      REFERENCES role_react_messages ON DELETE CASCADE,
                emoji       varchar(64) NOT NULL,
                role        bigint      NOT NULL,
                honeypot    bool        NOT NULL DEFAULT 'false'
            );
            CREATE UNIQUE INDEX IF NOT EXISTS idx_rrr_msg_emoji
                ON role_react_reactions (msg_ref, emoji);
            CREATE UNIQUE INDEX IF NOT EXISTS idx_rrr_msg_role
                ON role_react_reactions (msg_ref, role);
            """
        with self.db.connection() as conn:
            conn.execute(q)

        self.ignored = []
        self.cooldown = []

    @commands.group(name="role-react", aliases=["rr"])
    async def role_react(self, ctx: Context):
        """Commands for managing role reacts

        Role reacts are messages that users can add/remove specific emoji reactions to in order to add/remove themselves to/from a role."""
        if ctx.invoked_subcommand is None:
            await ctx.fail_msg("Invalid command passed")

    def _get_msg_details(self, msg: Message) -> Optional[NamedTuple]:
        q = """
            SELECT id, mutex, sweep
            FROM role_react_messages
            WHERE message = %s;
        """
        with self.db.connection() as conn:
            cursor = conn.execute(q, (msg.id,))
            if cursor.rowcount == 0:
                return None
            return cursor.fetchone()

    def _db_add_message(
        self, msg: Message, requestor: Member
    ) -> Union[NamedTuple, Exception]:
        q = """
            INSERT INTO role_react_messages
                (guild, channel,
                message, author)
            VALUES
                (%(guild)s, %(channel)s,
                %(message)s, %(author)s);
        """
        row_values = dict(
            guild=msg.guild.id,
            channel=msg.channel.id,
            message=msg.id,
            author=requestor.id,
        )
        with self.db.connection() as conn:
            try:
                conn.execute(q, row_values)
            except psycopg.errors.IntegrityError:
                pass
            except psycopg.errors.DatabaseError as e:
                return e

        return self._get_msg_details(msg)

    def _db_add_reaction(
        self, msg_ref: int, emoji: Emoji, role: Role
    ) -> Union[bool, Exception]:
        q = """
            INSERT INTO role_react_reactions
                (msg_ref, emoji, role)
            VALUES
                (%(ref)s, %(emoji)s, %(role)s);
        """
        row_values = dict(
            ref=msg_ref,
            emoji=emoji,
            role=role.id,
        )
        with self.db.connection() as conn:
            try:
                conn.execute(q, row_values)
                return True
            except psycopg.errors.IntegrityError:
                return False
            except psycopg.errors.DatabaseError as e:
                return e

    @commands.check_any(
        checks.is_guild_owner(),
        checks.is_admin(),
        commands.has_guild_permissions(manage_roles=True),
    )
    @role_react.command(aliases=["create"])
    async def add(
        self, ctx: Context, msg: Message, emoji: Emoji, role: Role
    ) -> None:
        """Add role-react emoji to specified message.

        The `msg` parameter must either be the ID of a message in the same channel or a url to a message."""

        bot_member = ctx.guild.get_member(ctx.bot.user.id)
        permissions = msg.channel.permissions_for(bot_member)
        if not permissions.add_reactions:
            err = "Add Reactions permission missing for me in that channel."
            await ctx.fail_msg(err, delete_after=-1)
            return

        message = self._db_add_message(msg, ctx.author)
        if isinstance(message, psycopg.errors.DatabaseError):
            await report_database_error(
                self._bot, ctx,
                "create role-react message", message
            )
            return

        result = self._db_add_reaction(message.id, emoji, role)
        if result is False:
            await ctx.fail_msg("Role or reaction is already on that message.")
        elif isinstance(result, psycopg.errors.DatabaseError):
            self._db_delete_message(msg.id)
            await report_database_error(
                self._bot, ctx,
                "create role-react reaction", result
            )
        if not result:
            return

        await msg.add_reaction(emoji)
        await ctx.react_success()

    # TODO: assess if any members violate the constraint. for any that do,
    #   remove all of those roles, and send them a DM notification. this
    #   would allow us to simplify `self._remove_previous_selections()`
    @commands.check_any(
        checks.is_guild_owner(),
        checks.is_admin(),
        commands.has_guild_permissions(manage_roles=True),
    )
    @role_react.command()
    async def mutex(self, ctx: Context, msg: Message, value: bool) -> None:
        """Make the roles on a message mutually exclusive of one another

        The `msg` parameter must either be the ID of a message in the same channel or a url to a message.

        The `value` parameter must be a bool-like string."""

        q = """
            UPDATE role_react_messages
            SET mutex=%(mutex)s
            WHERE message = %(message)s;
        """
        row_values = dict(
            mutex=value,
            message=msg.id
        )
        with self.db.connection() as conn:
            conn.execute(q, row_values)

        await ctx.react_success()

    @commands.check_any(
        checks.is_guild_owner(),
        checks.is_admin(),
        commands.has_guild_permissions(manage_roles=True),
    )
    @role_react.command()
    async def sweep(self, ctx: Context, msg: Message, value: bool) -> None:
        """Removes the contributed reaction from the message after granting the role

        Useful in verification models where the role _denies_ access to the channel it was obtained from"""

        role_reacts = self._get_role_react(msg)
        if role_reacts:
            for rr in role_reacts:
                if not rr.honeypot:
                    continue
                err = (f"That message contains a honeypot ({rr.emoji}). "
                       + "Disable it if you want to use sweep.")
                await ctx.fail_msg(err)
                return

        q = """
            UPDATE role_react_messages
            SET sweep=%(sweep)s
            WHERE message = %(message)s;
        """
        row_values = dict(
            sweep=value,
            message=msg.id
        )
        with self.db.connection() as conn:
            conn.execute(q, row_values)

        await ctx.react_success()

    @commands.check_any(
        checks.is_guild_owner(),
        checks.is_admin(),
        commands.has_guild_permissions(manage_roles=True),
    )
    @role_react.command()
    async def honeypot(
        self, ctx: Context,
        msg: Message, emoji: Emoji,
        value: bool
    ) -> None:
        details = self._get_msg_details(msg)
        if details.sweep:
            err = ("That message has sweep enabled. "
                   + "Disable it if you want to install a honeypot.")
            await ctx.fail_msg(err)
            return
        q = """
            UPDATE role_react_reactions
            SET honeypot = 'true'
            WHERE msg_ref = %s AND emoji = %s;
        """
        v = (details.id, emoji)
        with self.db.connection() as conn:
            cursor = conn.execute(q, v)
            if cursor.rowcount:
                await ctx.react_success()

    async def _get_for_message(
        self,
        msg: Message,
        privileged: bool,
        msg_ref: Optional[int] = None,
        mutex: Optional[bool] = None,
        sweep: Optional[bool] = None,
    ) -> Optional[dict]:
        if not msg_ref:
            logger.debug(msg)
            details = self._get_msg_details(msg)
            logger.debug(details)
            msg_ref = details.id
        q = """
            SELECT emoji, role, honeypot
            FROM role_react_reactions
            WHERE msg_ref = %s
        """
        fields = []
        with self.db.connection() as conn:
            result = conn.execute(q, (msg_ref,))
            if not result.rowcount:
                return None
            for emoji, role_id, honeypot in result:
                role = msg.guild.get_role(role_id)
                value = f"`{role}`"
                if privileged:
                    value += " 🍯" if honeypot else ""
                field = dict(name=emoji, value=value, inline=True)
                fields.append(field)
        if fields:
            chan = dict(name="Channel", value=msg.channel.mention, inline=True)
            fields.insert(0, chan)
            mutex = transform.bool_to_emoji(mutex)
            fields.insert(1, dict(name="Mutex", value=mutex, inline=True))
            sweep = transform.bool_to_emoji(sweep)
            fields.insert(2, dict(name="Sweep", value=sweep, inline=True))
            quote = transform.truncate_text(msg.content, limit=512)
            desc = ""
            if quote:
                desc = f">>> {quote}"
            embed = {
                "author": {
                    "name": str(msg.author),
                    "icon_url": msg.author.display_avatar.url,
                },
                "title": "Role-react details",
                "url": msg.jump_url,
                "description": desc,
                "fields": fields,
            }
            return embed

    async def _get_for_channel(
        self, channel: TextChannel, privileged: bool
    ) -> List[dict]:
        q = "SELECT message FROM role_react_messages WHERE channel = %s"
        messages = []
        with self.db.connection() as conn:
            cursor = conn.execute(q, (channel.id,))
            for row in cursor.fetchall():
                messages.append(
                    await channel.fetch_message(row.message)
                )
        embeds = [await self._get_for_message(msg, privileged) for msg in messages]
        return embeds

    async def _get_for_guild(self, guild: Guild, privileged: bool) -> List[dict]:
        q = """
            SELECT
                id,
                channel, message,
                mutex, sweep
            FROM role_react_messages
            WHERE guild = %s;
        """
        messages = []
        with self.db.connection() as conn:
            for tup in conn.execute(q, (guild.id,)):
                msg_ref, chan_id, msg_id, mutex, sweep = tup
                if not (channel := guild.get_channel(chan_id)):
                    logger.debug(f"Can't access role-react channel {chan_id}")
                    continue
                msg = await channel.fetch_message(msg_id)
                messages.append({
                    msg: dict(msg_ref=msg_ref, mutex=mutex, sweep=sweep)
                })
        return [
            await self._get_for_message(
                k, privileged,
                msg_ref=v["msg_ref"],
                mutex=v["mutex"],
                sweep=v["sweep"],
            ) for item in messages for k, v in item.items()
        ]

    @role_react.command(aliases=["ls", "list"])
    async def show(
        self, ctx: Context, scope: Optional[Union[TextChannel, Message]]
    ) -> None:
        """Show role-react messages for the guild (default), a channel, or a message."""
        is_staff = privileged.is_staff_member(ctx.bot, ctx.author, ctx.guild)
        if isinstance(scope, type(None)):
            embeds = [Embed.from_dict(e)
                      for e in await self._get_for_guild(ctx.guild, is_staff)]
        elif isinstance(scope, TextChannel):
            embeds = [Embed.from_dict(e)
                      for e in await self._get_for_channel(scope, is_staff)]
        elif isinstance(scope, Message):
            embeds = [Embed.from_dict(e)
                      for e in [await self._get_for_message(scope, is_staff)]]
        if embeds == []:
            await ctx.reply("No role-reacts found")
            return
        for post_embeds in split_embeds(embeds):
            # split embeds across posts for embed limits
            await ctx.reply(embeds=post_embeds)

    def _db_delete_reaction(self, msg: Message, emoji: Emoji) -> bool:
        message = self._get_msg_details(msg)
        q = """
            DELETE FROM role_react_reactions
            WHERE msg_ref = %(msg_id)s AND emoji = %(emoji)s
        """
        with self.db.connection() as conn:
            cur = conn.execute(q, dict(msg_id=message.id, emoji=emoji))
            if cur.rowcount == 0:
                return False
            return True

    @commands.check_any(
        checks.is_guild_owner(),
        checks.is_admin(),
        commands.has_guild_permissions(manage_roles=True),
    )
    @role_react.command(aliases=["rm", "del", "delete"])
    async def remove(self, ctx: Context, msg: Message, emoji: Emoji) -> None:
        """Remove a role-react binding from a role-react message"""
        if self._db_delete_reaction(msg, emoji):
            await msg.clear_reaction(emoji)
            await ctx.react_success()
        else:
            await ctx.react_fail()

    def _db_delete_message(self, msg: int) -> bool:
        q = "DELETE FROM role_react_messages WHERE message = %s"
        with self.db.connection() as conn:
            cur = conn.execute(q, (msg,))
            if not cur.rowcount:
                return False
            return True

    @commands.check_any(
        checks.is_guild_owner(),
        checks.is_admin(),
        commands.has_guild_permissions(manage_roles=True),
    )
    @role_react.command(aliases=["delete-msg", "remove-msg"])
    async def destroy(self, ctx: Context, msg: Union[Message, int]) -> None:
        """Remove all role-reacts from the given message and th bot's database."""
        msg_id = msg.id if isinstance(msg, Message) else msg
        result = self._db_delete_message(msg_id)
        if result:
            if isinstance(msg, Message):
                await msg.clear_reactions()
            await ctx.react_success()
        else:
            await ctx.react_fail()

    def _get_role_react(
        self, msg: Message, emoji: Optional[Emoji] = None
    ) -> Optional[Sequence[NamedTuple]]:
        q = """
            SELECT
                msgs.id, msgs.mutex,
                msgs.guild, msgs.channel, msgs.message,
                reacts.emoji, reacts.role, reacts.honeypot
            FROM role_react_messages AS msgs
            JOIN role_react_reactions AS reacts ON reacts.msg_ref = msgs.id
            WHERE message = %(msg_id)s
        """
        v = dict(msg_id=msg.id)
        if emoji:
            q += " AND emoji = %(emoji)s"
            v |= dict(emoji=emoji)
        with self.db.connection() as conn:
            cursor = conn.execute(q, v)
            if not cursor.rowcount:
                raise ValueError
            return cursor.fetchall()

    async def _remove_previous_selections(
        self, msg: Message, member: Member, chosen_role: int,
        role_reacts: Sequence[NamedTuple],
    ):
        self.cooldown += [(msg.id, member.id)]

        # support multiple roles for the case where the role-react message
        #   previously wasn't mutually exclusive
        other_selections = []
        for r in member.roles:
            if (
                [True for rr in role_reacts if r.id == rr.role]
                and r.id != chosen_role
            ):
                rr = find(lambda rr: r.id == rr.role, role_reacts)
                other_selections += [(
                    r.id,
                    rr.emoji
                )]

        if not other_selections:
            self.cooldown.remove((msg.id, member.id))
            return

        for role, emoji in other_selections:
            # when both mutex and sweep are enabled, we have to query for
            #   reactors to know if we need to remove other roles granted
            #   by this role-react message.
            # we only do this if it doesn't add too much wait time due
            #   to API pagination. thankfully, in this use case, there
            #   _shouldn't_ be many reactions stacked up, even if the bot
            #   were offline for awhile.
            reaction = find(lambda r: str(r.emoji) == emoji, msg.reactions)
            reactors = []
            if reaction and reaction.count <= 100:
                reactors = await reaction.users(limit=100).flatten()
                if member in reactors:
                    await msg.remove_reaction(emoji, member)
                else:
                    await member.remove_roles(msg.guild.get_role(role))
            else:
                self._bot.loop.create_task(
                    msg.remove_reaction(emoji, member)
                )

            # wait for the role to be removed before we'll remove the
            #   cooldown and let the user pick from this role-react again
            while True:
                member = msg.guild.get_member(member.id)
                if not find(lambda r: r.id == role, member.roles):
                    asyncio.wait(0.25)
                break

        self.cooldown.remove((msg.id, member.id))

    async def _sweep_reaction(
        self,
        msg: Message,
        member: Member,
        reaction: Emoji,
        role: Role,
        had_role: bool
    ) -> bool:
        guild = msg.guild
        channel = msg.channel
        outcome = None
        try:
            self.ignored += [(member.id, msg.id)]
            await msg.remove_reaction(reaction, member)
            outcome = True
        except Forbidden:
            text = ("'Manage Messages' permission missing "
                    + f"in {channel.mention}. Failed to sweep "
                    + f"reaction by {member.mention}.")
            await report_guild_error(self._bot, None, None, guild, text, None)
            outcome = False
        if had_role:
            return outcome
        try:
            # we notify them because people get confused
            # when they see that the reaction is removed
            # and end up hitting it again 😩
            text = (f"Granted you the `{role.name}` role "
                    + f"in *{role.guild.name}*")
            await member.send(text)
        except Forbidden:
            pass
        return outcome

    async def _report_honeypot(
        self,
        member: Member,
        emoji: Emoji,
        role: Role,
    ):
        fields = [
            dict(name="User", value=member.mention),
            dict(name="Role", value=f"{emoji} `{role.name}`"),
        ]
        await self.log.send(
            constants.Colours.dark_goldenrod,
            "Honeypot report",
            None, guild=member.guild,
            title_icon=constants.Icons.honeypot,
            fields=fields,
        )

    async def _delayed_reaction_removal(
        self, delay: float,
        msg: Message, emoji: Emoji, member: Member
    ):
        await asyncio.sleep(delay)
        try:
            await msg.remove_reaction(emoji, member)
        except Forbidden:
            pass
        return True

    async def _handle_add_reaction_event(
        self,
        guild: Guild, msg: Message,
        details: NamedTuple,
        member: Member, role: Role, emoji: Emoji,
        role_reacts: tuple[NamedTuple, Sequence[NamedTuple]],
        errors: tuple[str, str],
    ) -> None:
        role_react, role_reacts = role_reacts
        staff_err, user_err = errors

        if role_react.honeypot:
            await self._report_honeypot(member, emoji, role)
            self._bot.loop.create_task(
                self._delayed_reaction_removal(
                    300.0, msg, emoji, member
                )
            )

        if details.mutex:
            _, pending = await asyncio.wait(
                [self._remove_previous_selections(
                    msg, member, role_react.role,
                    role_reacts,
                )],
                timeout=300
            )
            if pending:
                err = "Discord service congestion detected."
                logger.debug(err)
                return

        had_role = bool(role in member.roles)
        if not had_role:
            try:
                await member.add_roles(role)
            except Forbidden:
                await report_guild_error(
                    self._bot, None, member,
                    guild, staff_err, user_err,
                )
        else:
            try:
                err = (f"You already have the `{role.name}` role"
                       + f"in *{guild.name}*")
                await member.send(err)
            except Forbidden:
                pass

        if details.sweep:
            await self._sweep_reaction(
                msg, member, emoji, role, had_role
            )

    async def _handle_remove_reaction_event(
        self,
        guild: Guild, msg: Message,
        details: NamedTuple,
        member: Member, role: Role, emoji: Emoji,
        role_reacts: tuple[NamedTuple, Sequence[NamedTuple]],
        errors: tuple[str, str],
    ):
        role_react, role_reacts = role_reacts
        staff_err, user_err = errors

        if role_react.honeypot:
            return
        if (member.id, msg.id) in self.ignored:
            self.ignored.remove((member.id, msg.id))
            return

        if role in member.roles:
            try:
                await member.remove_roles(role)
            except Forbidden:
                await report_guild_error(
                    self._bot, None, member,
                    guild, staff_err, user_err
                )
        else:
            try:
                await member.send(
                    f"You removed your react for the `{role.name}` "
                    f"role in `{guild.name}`, but you didn't have that role."
                )
            except Forbidden:
                pass

    @commands.Cog.listener("on_raw_reaction_add")
    @commands.Cog.listener("on_raw_reaction_remove")
    async def handle_reaction_event(self, p: RawReactionActionEvent) -> None:
        """Add/remove role from user according to role-react binding"""
        if p.user_id == self._bot.user.id:
            return  # ignore bot reacts

        # get role reacts for message
        try:
            guild = self._bot.get_guild(p.guild_id)
            member = guild.get_member(p.user_id)
            channel = guild.get_channel_or_thread(p.channel_id)
            msg = await channel.fetch_message(p.message_id)
            details = self._get_msg_details(msg)
            role_reacts = self._get_role_react(msg)
            emoji = (self._bot.get_emoji(p.emoji.id) or p.emoji)
            role_react = find(lambda rr: rr.emoji == str(emoji), role_reacts)
            role = guild.get_role(role_react.role)
            assert role is not None
        except (AttributeError, TypeError, ValueError) as e:
            if (
                self._bot.environment == 'development'
                and guild.id == 1006771410103959663
            ):
                raise e
            return
        except AssertionError:
            err = ("Role-react interaction failed; role for "
                   + f"{str(p.emoji)} on {msg.jump_url} no longer exists")
            await report_guild_error(self._bot, None, None, guild, err)
            return

        if (
            (p.message_id, p.user_id) in self.cooldown
            and (p.message_id, p.user_id) not in self.ignored
        ):
            try:
                await member.send("🏇 Slow down!")
            except Forbidden:
                pass

        staff_err = ("'Manage Roles' permission missing, or my role is "
                     + f"below `{role.name}` in the hierarchy. Failed to "
                     + f"grant it to {member.mention} via role-react.")
        user_err = (f"Lacking permission to give you the `{role.name}` "
                    + f"role in *{guild.name}*. Staff have been "
                    + "notified of the problem!")
        if p.event_type == "REACTION_ADD":
            await self._handle_add_reaction_event(
                guild, msg, details, member, role, emoji,
                (role_react, role_reacts), (staff_err, user_err),
            )
        elif p.event_type == "REACTION_REMOVE":
            await self._handle_remove_reaction_event(
                guild, msg, details, member, role, emoji,
                (role_react, role_reacts), (staff_err, user_err),
            )

    @commands.Cog.listener("on_raw_message_delete")
    async def handle_message_delete(self, p: RawMessageDeleteEvent) -> None:
        q = "DELETE FROM role_react_messages WHERE message = %s"
        with self.db.connection() as conn:
            conn.execute(q, (p.message_id,))


def setup(bot: Bot):
    bot.add_cog(RoleReact(bot))
