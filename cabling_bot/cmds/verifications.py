import logging
import asyncio
import psycopg_pool

from typing import (NamedTuple, Sequence, Optional, Union)
from psycopg import DatabaseError
from discord.utils import utcnow
from discord.ext import commands
from ..util.privileged import is_staff_member
from ..util.transform import truncate_text

from discord import (
    Embed, Guild, NotFound, Role, Member,
    SlashCommandGroup, option,
    Forbidden, HTTPException
)
from ..bot import (
    CablesCog as Cog,
    CablesBot as Bot,
    CablesAppContext as ApplicationContext,
    checks,
    constants,
    errors
)

logger = logging.getLogger(__name__)
mdash = constants.Symbols.mdash


class MemberVerificationManager(Cog):
    def __init__(self, bot: Bot, hidden=True) -> None:
        super().__init__()
        self._bot = bot
        self.db: psycopg_pool.ConnectionPool = bot.db
        q = """\
            -- Guild member autopurge list
            CREATE TABLE IF NOT EXISTS member_autopurge_guilds(
                id              serial      PRIMARY KEY,
                guild           bigint      NOT NULL UNIQUE
            );
            -- Exclusion list
            CREATE TABLE IF NOT EXISTS member_autopurge_exclusions(
                id              serial      PRIMARY KEY,
                guild_ref       bigint      REFERENCES member_autopurge_guilds ON DELETE CASCADE,
                member          bigint      NOT NULL,
                created_at      timestamp   NOT NULL DEFAULT CURRENT_TIMESTAMP,
                created_by      bigint      NOT NULL
            );
            CREATE UNIQUE INDEX IF NOT EXISTS idx_mapexclusions_guildmember
                ON member_autopurge_exclusions (guild_ref, member);
            """
        with self.db.connection() as conn:
            try:
                conn.execute(q)
            except DatabaseError as e:
                logger.debug("Cog init error: " + e)

        bot.config_keys.new(
            "feature_flags", "member_verification",
            "(bool) Whether or not the member verification module is enabled",
            False
        )
        bot.config_keys.new(
            "member_verification", "role",
            "(int) Role which is given to indicate the member has been verified",
            None
        )
        bot.config_keys.new(
            "member_verification", "application_time_limit",
            ("(int) Length in days which unverified members are alotted to "
             + "complete the application process"),
            3
        )
        bot.config_keys.new(
            "member_verification", "purge_summaries",
            ("(bool) Whether or not to notify staff of unverified member "
             + "purge outcomes each time the cronjob fires"),
            False
        )

    def enabled(self, guild: Guild) -> bool:
        return self._bot.get_guild_key(
            guild.id,
            "feature_flags", "member_verification"
        )

    ## METHOD HELPERS

    def _get_allow_list(
        self, guild: Guild,
    ) -> Sequence[NamedTuple]:
        q = """
            SELECT member
            FROM member_autopurge_exclusions AS acl
            JOIN member_autopurge_guilds AS guilds
                ON acl.guild_ref = guilds.id
            WHERE guild = %(guild)s;
        """
        with self.db.connection() as conn:
            cursor = conn.execute(q, dict(guild=guild.id))
            if not cursor.rowcount:
                return list()
            return cursor.fetchall()

    def _populate_queue(
        self, guild: Guild, role: Role, time_limit: int
    ) -> Optional[list[Member]]:
        now = utcnow()
        exclusions = [row.member for row in self._get_allow_list(guild)]
        queue = [m for m in guild.members if
                 # Role check
                 role not in m.roles
                 # Membership length check
                 and (now - m.joined_at).days >= time_limit
                 # ACL check
                 and m.id not in exclusions
                 # Staff check
                 and not is_staff_member(self._bot, m, guild)
                 # Bot check
                 and not m.bot]
        if not queue:
            return []
        return queue

    async def _summarize(self, guild: Guild, counts: tuple) -> None:
        gid = guild.id
        total, dm_failed, kick_failed = counts
        fail_count = (dm_failed + kick_failed)

        text = f"✅ Purged `{total}` unverified members\n"
        if fail_count > 0:
            fail_count = dm_failed + kick_failed
            if dm_failed == kick_failed:
                text += f"- `{fail_count}` full failures"
            else:
                text += (f"{mdash} `{dm_failed}` notes failed\n"
                         + f"{mdash} `{kick_failed}` kicks failed")

        chid = self._bot.get_guild_key(gid, "moderation", "report_channel")
        channel = guild.get_channel(chid)
        await channel.send(text)

    ## METHODS

    async def purge(
        self,
        guild: Guild, role: Role,
        time_limit: int,
        notify: Optional[bool] = False,
        dry_run: bool = False,
    ) -> Optional[Union[tuple[int, int], list[Member]]]:
        if not (queue := self._populate_queue(guild, role, time_limit)):
            return None
        if dry_run:
            return queue
        total, i = len(queue), 0
        kick_failed, dm_failed = 0, 0
        for m in queue:
            membership_length = (utcnow() - m.joined_at).days
            dm_text = (
                f"You have been auto-removed from *{guild.name}*.\n\n"
                + "A membership application wasn't fully completed within the "
                + f"{membership_length} days since you'd joined.\n\nYou're welcome "
                + "to rejoin when you have the time, interest and energy to finish"
            )
            if guild.id == 992464815958269962:
                dm_text += "; you'll be able to pick up right where you left off"
            dm_text += ("~\n\nIf you have any feedback or would like to make an "
                        + "accommodation request, please reach out to "
                        + f"{guild.owner.mention}!")

            try:
                await m.send(dm_text)
            except (Forbidden, HTTPException):
                dm_failed += 1

            try:
                await m.kick(reason="auto-purge unverified")
            except (Forbidden, HTTPException):
                kick_failed += 1

            i += 1
            await asyncio.sleep(2.5)

        if (
            notify
            or self._bot.get_guild_key(
                guild.id, "member_verification", "purge_summaries"
            )
        ):
            await self._summarize(guild, (total, dm_failed, kick_failed))

        return (total, (dm_failed + kick_failed))

    ## COMMAND HELPERS

    def _get_guild_config(
        self, guild: Union[Guild, int]
    ) -> tuple[Guild, Role, int]:
        if (
            type(guild) is int
            and not (guild := self._bot.get_guild(guild))
        ):
            try:
                guild = self._bot.fetch_guild(guild)
            except NotFound:
                self._db_toggle_autopurge(guild, False)

        role_id = self._bot.get_guild_key(
            guild.id, "member_verification", "role"
        )
        if not (role := guild.get_role(role_id)):
            raise TypeError

        time_limit = self._bot.get_guild_key(
            guild.id, "member_verification", "application_time_limit"
        )

        return (guild, role, time_limit)

    def _db_get_autopurge(self, guild: int) -> bool:
        q = "SELECT 'true' FROM member_autopurge_guilds WHERE guild = %(guild)s;"
        with self.db.connection() as conn:
            cursor = conn.execute(q, dict(guild=guild))
            if not cursor.rowcount:
                return False
            return True

    def _db_toggle_autopurge(self, guild: int, value: bool) -> bool:
        if value:
            q = "INSERT INTO member_autopurge_guilds (guild) VALUES (%(guild)s);"
        else:
            q = "DELETE FROM member_autopurge_guilds WHERE guild = %(guild)s;"
        with self.db.connection() as conn:
            cursor = conn.execute(q, dict(guild=guild))
            if not cursor.rowcount:
                return False
            return True

    ## COMMANDS

    cmd_group = SlashCommandGroup(
        "verifications",
        "Setting management and commands for the member verification module",
        guild_only=True,
    )

    @checks.is_feature_enabled("member_verification")
    @checks.is_guild_owner()
    @cmd_group.command(name="autopurge")
    @option("toggle", bool, description="Flip the switch on or off?", default=None)
    async def toggle_autopurge(
        self, ctx: ApplicationContext,
        toggle: Optional[bool] = None,
    ) -> None:
        """Get setting for, or toggle automatic purging of unverified members."""
        if toggle is None:
            setting = self._db_get_autopurge(ctx.guild.id)
            setting = "on" if setting else "off"
            await ctx.respond(f"Autopurge is {setting}", ephemeral=True)
            return
        if not self._db_toggle_autopurge(ctx.guild.id, toggle):
            await errors.report_database_error(
                self._bot, ctx,
                "toggling autopurge", None
            )
            return
        await ctx.respond("Done! ✅", ephemeral=True)

    @checks.is_feature_enabled("member_verification")
    @checks.is_admin()
    @cmd_group.command(name="purge")
    @option("time_limit", int, description="Days since member joined", default=None)
    @option("dry_run", bool, description="Simulate what would happen?", default=False)
    async def purge_cmd(
        self, ctx: ApplicationContext,
        time_limit: Optional[int] = None,
        dry_run: Optional[bool] = False,
    ) -> None:
        try:
            config = self._get_guild_config(ctx.guild)
        except TypeError:
            err = "❌ Verified role isn't configured."
            await ctx.respond(err, ephemeral=True)
            return
        if not time_limit:
            guild, role, time_limit = config
        else:
            guild, role, _ = config

        if not dry_run:
            await ctx.response.defer(ephemeral=True)

        result = await self.purge(
            guild, role, time_limit,
            notify=True,
            dry_run=dry_run,
        )

        if dry_run:
            if not result:
                await ctx.respond("Nothing to do. 🧐")
                return
            members = "\n".join([f"{mdash} {m.mention}" for m in result])
            text = (f"Would kick `{len(result)}` members idle for "
                    + f"`{time_limit}` day(s):\n{members}")
            embed = Embed(description=truncate_text(text, limit=2000))
            await ctx.respond(embeds=[embed])
        else:
            await ctx.followup.send("Done! ✅", ephemeral=True)

    ## LISTENERS

    @commands.Cog.listener("on_member_purge")
    async def cron_job(self) -> None:
        q = "SELECT guild FROM member_autopurge_guilds;"
        with self.db.connection() as conn:
            cursor = conn.execute(q)
            if not cursor.rowcount:
                return
            targets = cursor.fetchall()

        for target in targets:
            start = utcnow()

            if not (
                self.enabled(self._bot.get_guild(target.guild))
                and (config := self._get_guild_config(target.guild))
            ):
                continue

            response = await self.purge(*config)
            runtime = (utcnow() - start).seconds
            if not response:
                return

            total, failed = response
            log = (f"Purged {total - failed} unverified members from "
                   + f"{config[0].id} in {runtime} secs")
            logger.debug(log)

            await asyncio.sleep(60)


def setup(bot: Bot):
    bot.add_cog(MemberVerificationManager(bot))
