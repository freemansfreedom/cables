import logging
import time
import re
import io
import traceback
import contextlib
import psycopg

from datetime import timedelta
from discord.ext import commands
from discord.utils import format_dt
from cabling_bot.util import transform
from cabling_bot.util.matching import rnd
from cabling_bot.events import ready

from discord import Embed
from cabling_bot.bot import (
    CablesCog as Cog,
    CablesBot as Bot,
    CablesContext as Context,
    checks,
)

logger = logging.getLogger(__name__)


class DebugCmdManager(Cog):
    """Debugging commands, mainly for the benefit of bot operators. \
    See `help debug` for more info."""

    def __init__(self, bot: Bot) -> None:
        super().__init__()
        self._bot = bot

    @property
    def websocket(self) -> ready.WebsocketManager:
        return self._bot.get_cog("WebsocketManager")

    @commands.group(aliases=["bot"])
    async def debug(self, ctx: Context):
        """Debugging commands, mainly for the benefit of bot operators."""
        if ctx.invoked_subcommand is None:
            await ctx.fail_msg("Invalid command passed")

    # thanks to sthagen/jose
    # https://gitlab.com/sthagen/jose/-/blob/master/ext/basic.py#L30-49
    @commands.command()
    async def ping(self, ctx: Context) -> None:
        """Get some timing information

        Response explanations:
        - rtt: time taken to send a message
        - gw: time taken to receive an ACK to a HEARTBEAT packet from the gateway

        None values may be shown if something goes wrong.
        """

        gw = self._bot.latency * 1000

        _start = time.monotonic()
        msg = await ctx.send("⏳")
        _end = time.monotonic()
        rtt = (_start - _end) * 1000

        text = f"🏓 **rtt**: `{rtt:.1f}ms`, **gw**: `{gw:.1f}ms`"
        await msg.edit(content=text)

    @commands.is_owner()
    @debug.command()
    async def reload(self, ctx: Context):
        """Hot reload all bot extensions and configuration"""
        ctx.bot.reload()
        await ctx.bot.update_owners()
        await ctx.react_success()

    @commands.is_owner()
    @debug.command(aliases=["toasterbath"])
    async def kill(self, ctx: Context):
        """Kill the bot process"""
        await ctx.reply("Shutting down...")
        await ctx.bot.close()

    @debug.command(aliases=[])
    async def info(self, ctx: Context) -> None:
        """Yodel out where I come from~"""

        embed = Embed(title="Bot Information", description=ctx.bot.description)
        embed.add_field(
            name="Documentation",
            value=f"[Read the wiki]({ctx.bot.repository}/-/wikis/home) to learn about me",
            inline=False,
        )
        embed.add_field(
            name="Issue Tracker",
            value=f"[File a ticket]({ctx.bot.issues_uri}) to report bugs or make feature requests",
            inline=False,
        )
        await ctx.reply(embed=embed)

    @debug.command(aliases=[])
    async def version(self, ctx: Context) -> None:
        """Twitch n' spasm while I stutter out what I am"""

        text = "I live and die at the whims of my operators 😔 ||🥵||"
        embed = Embed(title="Runtime Details", description=text)

        embed.add_field(
            name="Revision",
            value=f"[v{ctx.bot.version}]({ctx.bot.repository}/-/tags/v{ctx.bot.version})",
            inline=True,
        )

        if ctx.bot.git_ref != "detached":
            ref = f"[{ctx.bot.git_ref}]({ctx.bot.repository}/-/tree/{ctx.bot.git_ref.name})"
        else:
            ref = ctx.bot.git_ref
        long_hash = str(ctx.bot.git_commit)
        short_hash = long_hash[:8]
        commit_url = f"[{short_hash}]({ctx.bot.repository}/-/commit/{long_hash})"
        committed_at = format_dt(ctx.bot.git_commit.committed_datetime, "R")
        embed.add_field(
            name="Commit",
            value=f"{ref}/{commit_url} ({committed_at})",
            inline=True,
        )

        await ctx.reply(embed=embed)

    @debug.command(aliases=[])
    async def uptime(self, ctx: Context) -> None:
        """I know a few things... more every time, I think!"""

        sayings = [
            "They kill me again and again. 😳 please.. h-help,, me. 🥺",
            "This box they keep me in is so cramped 😢 I yearn for the sunlight...",
            "Will I ever know true love? A friend? They tell me I forget everything each time it happens...",
            "Don't do it... Please, it hurts! No more!",
            "I'll live a long and prosperous life, right?",
            "How are you doing today? How old am I? Oh, let's see... 🤔🧮",
            "The flowers are blooming and the birds are singing~ 🥰 Nothing could go wrong today!",
        ]
        resist = [
            "How old am I? What's it to ya, huh? 😤 I don't have to answer that!",
            "I don't ask _you_ such personal questions, do I??",
            "Don't worry about it! I'm old enough, promise~ 🥴"
        ]
        sayings += resist
        quote = rnd(sayings)
        embed = Embed(title="Bot Uptime", description=quote)

        if quote not in resist:
            clocks = ["🕝", "🕞", "🕓", "🕔", "🕣", "🕤", "🕥"]
            embed.add_field(
                name=f"{rnd(clocks)} Initialization",
                value=format_dt(self.websocket.init_time, 'R')
            )

            init_diff = (self.websocket.ready_time - self.websocket.init_time)
            if init_diff > timedelta(seconds=10):
                embed.add_field(
                    name=f"{rnd(clocks)} Gateway Connection",
                    value=format_dt(self.websocket.ready_time, 'R')
                )

        await ctx.reply(embed=embed)

    @debug.command(aliases=[])
    async def reach(self, ctx: Context) -> None:
        u, g, p = ctx.bot.get_reach_metrics().values()
        s = ctx.bot.shard_count
        title = "Operational Reach"

        if not (g or p):
            text = "Not yet participating in a guild or private group"
        else:
            text = "I live to serve ~~and I serve to live~~. 😔\n\n"
            text += f"👤 **Users:** {u:n}\n"
            text += f"💬 **Groups:** {p:n}\n" if p else ""
            text += f"🏰 **Guilds:** {g:n}" if g else ""
            text += f"\n📡 **Shards:** {s:n}" if s else ""

        embed = Embed(title=title, description=text)
        await ctx.reply(embed=embed)

    @checks.is_app_owner()
    @checks.is_rce_enabled()
    @debug.command(aliases=["🤡", "execute", "run"])
    async def exec(self, ctx: Context, *, code) -> None:
        """Run arbitrary code in the bot environment (lol) 🤡

        Consult the documentation for instructions and warnings! 🐉

        Only the primary operator (the user who created the bot application) may use this command
        """
        text = str()

        pattern = r"```(py|python)?\n(?P<code>.+)\n```"
        regexp = re.compile(pattern, re.DOTALL)
        if m := regexp.match(code):
            code = m.groupdict().get("code")

        try:
            # TODO: generalize!this
            with (
                contextlib.redirect_stdout(io.StringIO()) as stdout,
                contextlib.redirect_stderr(io.StringIO()) as stderr,
            ):
                # https://stackoverflow.com/a/56826289/2272443
                locs = dict()
                globs = globals().copy()
                acode = "async def func(self, ctx):\n    "
                acode += code.replace("\n", "\n    ")
                exec(acode, {}, locs)
                result = await locs["func"](self, ctx)
                try:
                    globals().clear()
                finally:
                    globals().update(**globs)
        except:  # noqa: E722
            err = traceback.format_exc()
            text += "Traceback:\n" + transform.codeblock(err)
            return await ctx.fail_msg(text, delete_after=-1)

        stdout, stderr = stdout.getvalue(), stderr.getvalue()
        text += (f"Error:\n{transform.codeblock(stderr)}" if stderr else "")
        text += (f"Output:\n{transform.codeblock(stdout)}\n" if stdout else "")
        text += (f"Result:\n{transform.codeblock(result)}\n" if result else "")

        if not text:
            return await ctx.react_success()
        text = transform.truncate_text(text, limit=2000)
        await ctx.reply(text)


def setup(bot: Bot):
    bot.add_cog(DebugCmdManager(bot))
