import logging
from typing import (Optional, Union, List)

from discord.ext import commands
from discord import (
    SlashCommandGroup,
    Guild, TextChannel, VoiceChannel, Member, Role,
    User, Asset, File, Embed,
)
from discord.errors import (
    Forbidden, NotFound, InvalidArgument,
)

from cabling_bot.bot import (
    CablesCog as Cog,
    CablesBot as Bot,
    CablesAppContext as ApplicationContext,
    CablesContext as Context,
    constants,
    checks,
    exceptions,
)
from cabling_bot.util import (
    embeds,
    interactions,
    transform,
    queue,
)

logger = logging.getLogger(__name__)


class TestCmdManager(Cog):
    """Experimental commands not for public consumption (yet?)"""

    def __init__(self, bot: Bot) -> None:
        super().__init__()
        self._bot = bot
        self.log = embeds.EmbedManager(bot)

    ##############################
    ## MESSAGE COMMAND EXAMPLES ##
    ##############################

    ## Single command

    # @commands.command("test")
    # async def test(self, ctx: Context):
    #     """Are you feeling lucky?"""
    #     pass

    ## Command group

    # @commands.group(name="test", aliases=[])
    # async def example_group(self, ctx: Context):
    #     """Experimental commands"""
    #     if ctx.invoked_subcommand is None:
    #         await ctx.fail_msg("Invalid command passed")

    # @example_group.command(name="function")
    # async def example_function(self, ctx: Context):
    #     """Are you feeling lucky?"""
    #     pass

    ############################
    ## SLASH COMMAND EXAMPLES ##
    ############################

    ## Single command

    # @commands.slash_command(name="test", guild_only=True)
    # async def test(self, ctx: ApplicationContext):
    #     """Are you feeling lucky?"""
    #     pass

    ## Command group

    # test_group = SlashCommandGroup("test", "Test commands")

    # @test_group.command(name="function", guild_only=True)
    # async def example_function(self, ctx: ApplicationContext):
    #     """Are you feeling lucky?"""
    #     pass

    ######################
    ## CUSTOM TEST CODE ##
    ######################

    ## Your code here

    ## ...

    ## But not past here...

    #####################################
    ## DO NOT COMMIT CODE BELOW HERE!! ##
    #####################################


def setup(bot: Bot):
    bot.add_cog(TestCmdManager(bot))
