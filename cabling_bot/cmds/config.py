import logging
from cabling_bot.util import transform
from discord.ext import commands
from cabling_bot.bot import checks

from typing import Optional, Union
from discord import Embed, Object
from cabling_bot.bot import (
    CablesCog as Cog,
    CablesContext as Context,
    CablesBot as Bot
)


logger = logging.getLogger(__name__)


class ConfigManager(Cog):
    """Manage server or member settings. See `help config` for more info."""
    def __init__(self, bot: Bot):
        self._bot = bot

    def _format_embed(self, desc: str) -> Embed:
        embed = Embed(title="Configuration Keys")
        text = "\n".join(desc)
        if len(text) > 4086:
            embed.add_field(
                name="Error",
                value="Too many keys to list. Filter to a module."
            )
            text = transform.truncate_text(text)
        embed.description = text
        return embed

    @commands.guild_only()
    @commands.group(name="config")
    async def config_cmd(self, ctx: Context):
        """Manage server or member settings.

        config
            ├── keys [module] [key]
            ├── guild
            |   ├── get   [module] [key]
            |   ├── set   <module> <key> <value>
            |   └── unset <module> <key> <value>
            |
            └── user
                ├── get   [module] [key]
                ├── set   <module> <key> <value>
                └── unset <module> <key> <value>
        """
        if ctx.invoked_subcommand is None:
            await ctx.send_help(self.config_cmd)

    ##########
    ## KEYS ##
    ##########

    @config_cmd.command(name="keys")
    async def config_keys(
        self, ctx: Context,
        module: Optional[str],
        key: Optional[str]
    ):
        """Show available configuration keys for the given scope"""
        keys = ctx.config.get(module, key)
        desc = [
            f"**`{key.module}.{key.key}`**: {key.description}"
            for key in keys
        ]
        embed: Embed = self._format_embed(desc)
        if embed.description:
            await ctx.reply(embeds=[embed])
        else:
            await ctx.fail_msg("No configuration keys defined yet...")

    @commands.is_owner()
    @config_cmd.command(name="new", aliases=["add"])
    async def config_add(
        self, ctx: Context,
        module: str, key: str,
        description: str,
        default: Optional[str]
    ):
        """Create a new configuration key"""
        ctx.config.new(module, key, description, default)
        await ctx.react_success()

    @commands.is_owner()
    @config_cmd.command(name="delete", aliases=["rm"])
    async def config_delete(
        self, ctx: Context,
        module: str, key: str
    ):
        """Delete a configuration key"""
        output = ctx.config.delete(module, key)
        if output.rowcount:
            await ctx.react_success()
        else:
            await ctx.fail_msg("That module key didn't exist.")

    ###########
    ## GUILD ##
    ###########

    @config_cmd.group(name="guild")
    @commands.has_permissions(administrator=True)
    async def guild_config(self, ctx: Context):
        """Manage configuration for the guild"""
        if ctx.invoked_subcommand is None:
            await ctx.send_help(self.guild_config)

    @commands.check_any(
        checks.is_guild_owner(),
        checks.is_dev_guild_reconfig()
    )
    @guild_config.command(name="get")
    async def guild_get(
        self, ctx: Context,
        module: Optional[str], key: Optional[str]
    ):
        """Get configuration key(s) for this guild at the given scope"""
        rows = ctx.guild_config.list(module, key)
        if not rows:
            await ctx.fail_msg("No configuration keys found.")
            return
        desc = [f"`{row.module}.{row.key}` = {row.value}" for row in rows]
        embed: Embed = self._format_embed(desc)
        if embed.description:
            await ctx.reply(embeds=[embed])

    @commands.check_any(
        checks.is_guild_owner(),
        checks.is_dev_guild_reconfig()
    )
    @guild_config.command(name="set")
    async def guild_set(
        self, ctx: Context,
        module: str, key: str, *,
        value: Union[Object, str]
    ):
        """Set configuration key at the guild level

        May only be used by the guild owner"""
        if type(value) is Object:
            value = value.id
        result = ctx.guild_config.set(module, key, value)
        if result:
            # update cache used by Cogs to reference configuration data
            if type(value) is str:
                value = transform.strtoliteral(value)
            self._bot.config_data[ctx.guild.id][module][key] = value
            await ctx.react_success()
        else:
            await ctx.fail_msg("That configuration key doesn't exist.")

    @commands.check_any(
        checks.is_guild_owner(),
        checks.is_dev_guild_reconfig()
    )
    @guild_config.command(name="unset")
    async def guild_unset(self, ctx: Context, module: str, key: str):
        """Set configuration key at the guild level

        May only be used by the guild owner"""
        result = ctx.guild_config.unset(module, key)
        if result:
            # update cache used by Cogs to reference configuration data
            del self._bot.config_data[ctx.guild.id][module][key]
            await ctx.react_success()
        else:
            await ctx.fail_msg("That configuration key wasn't set.")

    ############
    ## MEMBER ##
    ############

    @config_cmd.group(name="member", aliases=["user"])
    async def member_config(self, ctx: Context):
        """Manage configuration for yourself in this guild"""
        if ctx.invoked_subcommand is None:
            await ctx.send_help(self.member_config)

    @member_config.command(name="get")
    async def member_get(
        self, ctx: Context,
        module: Optional[str], key: Optional[str]
    ):
        """Get configuration key(s) for this member at the given scope"""
        rows = ctx.member_config.get(module, key)
        if not rows:
            await ctx.fail_msg("No configuration keys found.")
            return
        if rows:
            desc = [f"`{row.module}.{row.key}` = {row.value}" for row in rows]
            await ctx.reply("\n".join(desc))

    @member_config.command(name="set")
    async def member_set(
        self, ctx: Context,
        module: str, key: str, *,
        value: str
    ):
        """Set configuration key at the member level"""
        output = ctx.member_config.set(module, key, value)
        if output.rowcount:
            await ctx.react_success()
        else:
            await ctx.fail_msg("That configuration key doesn't exist.")

    @member_config.command(name="unset")
    async def member_unset(self, ctx: Context, module: str, key: str):
        """Set configuration key at the member level"""
        output = ctx.member_config.unset(module, key)
        if output:
            await ctx.react_success()
        else:
            await ctx.fail_msg("That configuration key doesn't exist.")


def setup(bot: Bot):
    bot.add_cog(ConfigManager(bot))
