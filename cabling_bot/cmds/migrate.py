import logging
import inspect
from cabling_bot.bot import (
    CablesCog as Cog,
    CablesBot as Bot,
    CablesContext as Context,
)
from cabling_bot.util.migrations import MigrationManager
from cabling_bot.util.guild_invites import GuildInvites
from discord.ext import commands

logger = logging.getLogger(__name__)


class DbMigrations(Cog):
    """Database migration functions for bot upgrades. See `help migrate` for more info."""

    def __init__(self, bot: Bot) -> None:
        super().__init__()
        self.migration_mgr = MigrationManager(bot)
        self.invite_mgr = GuildInvites(bot.db)

    @commands.is_owner()
    @commands.command("migrate")
    async def migrate(self, ctx: Context) -> None:
        """Run the latest database migration code.

        May only be used by the bot operators."""

        if ctx.bot.version not in self.migration_mgr.revs:
            await ctx.fail_msg("No migration to run!")
            return

        _rev = "v" + ctx.bot.version.replace(".", "_")
        _method = getattr(self.migration_mgr, _rev)

        failures, successes = [], 0
        for guild in ctx.bot.guilds:
            response = await guild.invites()
            remote = self.invite_mgr._transform_to_dict(response)
            known = await self.invite_mgr._list_known_invites(
                guild, only_active=False
            )

            _guild_args = []
            if _rev == "v0_8_24":
                _guild_args += [guild, known]

            for code, invite in remote.items():
                if _rev == "v0_8_24":
                    _method_args = [*_guild_args, code, invite]

                if inspect.isawaitable(_method):
                    _failures, _successes = await (_method)(*_method_args)
                else:
                    _failures, _successes = (_method)(*_method_args)

                failures += _failures
                successes += _successes

        if failures:
            _failures = ', '.join(failures)
            logger.error(f"Failed to execute all queries: {_failures}")

        text = ("Migration finishd! Query success rate: "
                + f"{successes}/{successes+len(failures)}")
        await ctx.reply(text)


def setup(bot: Bot):
    bot.add_cog(DbMigrations(bot))
