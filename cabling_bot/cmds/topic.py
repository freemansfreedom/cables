import logging
import json
from psycopg.types.json import Json as PostgresJson
from typing import NamedTuple, Optional, Union
from discord import (Embed, VoiceChannel, SlashCommandGroup)
from discord.channel import TextChannel
from discord.ext import commands
from discord.threads import Thread

from cabling_bot.bot import (
    CablesAppContext as ApplicationContext,
    CablesBot as Bot,
    CablesCog as Cog,
    checks
)

logger = logging.getLogger(__name__)

Channel = Union[TextChannel, Thread, VoiceChannel]


class TopicManager(Cog):
    """Commands for managing channel topic posts. See `help topic` for more info."""

    def __init__(self, bot: Bot) -> None:
        super().__init__()
        self.db = bot.db
        q = """\
            CREATE TABLE IF NOT EXISTS topic (
                id       serial  PRIMARY KEY,
                guild    bigint  NOT NULL,
                channel  bigint  NOT NULL UNIQUE,   -- channel/thread id
                message  json    NOT NULL           -- topic contents
            );
            """
        with self.db.connection() as conn:
            conn.execute(q)

    def _get_topic(self, channel_id: int) -> Optional[NamedTuple]:
        """Get topic record for a given channel/thread

        :param channel_id: channel/thread ID for topic to fetch
        """
        q = "SELECT * FROM topic WHERE channel = %s"
        with self.db.connection() as conn:
            result = conn.execute(q, (channel_id,)).fetchone()
        return result

    topic_group = SlashCommandGroup("topic", "Change a channel's topic")

    @topic_group.command(name="get", guild_only=True)
    async def topic(self, ctx: ApplicationContext):
        """Display the current channel description and additional information if set with `topic set`."""
        if (chan_topic := self._get_topic(ctx.channel.id)) is not None:
            topic_embed = Embed.from_dict(chan_topic.message)
        else:
            topic_embed = None
        description = getattr(ctx.channel, "topic", None)
        if description or topic_embed:
            await ctx.respond(content=description, embed=topic_embed)
        else:
            await ctx.respond("No topic set for this channel.")

    @commands.check_any(
        checks.is_channel_mod(),
        checks.is_global_mod(),
        commands.has_guild_permissions(manage_channels=True)
    )
    @topic_group.command(guild_only=True)
    async def set(
        self,
        ctx: ApplicationContext,
        topic_message: str,
        channel: Optional[Channel],
    ):
        """Set additional information to be displayed by the `topic` command

        This does not change the channel's description through Discord APIs. Accepts embed JSON.

        Requires being a staff member or the 'Manage Channels' permission.
        """
        if not channel:
            channel = ctx.channel
        # default embed settings
        contents = {
            "title": f"#{channel} topic",
            "description": topic_message,
            "color": 8174071,
            "author": {
                "name": str(ctx.author),
                "icon_url": ctx.author.display_avatar.url,
            },
        }
        try:
            # override defaults if user gives valid json
            contents |= json.loads(topic_message)
        except json.JSONDecodeError:
            pass
        q = """\
            INSERT INTO topic (guild, channel, message)
            VALUES (%(guild)s, %(channel)s, %(message)s)
            ON CONFLICT (channel)
            DO UPDATE SET message = %(message)s
            """
        row_values = dict(
            guild=ctx.guild.id,
            channel=channel.id,
            message=PostgresJson(contents),
        )

        with self.db.connection() as conn:
            conn.execute(q, row_values)
        await ctx.respond(
            f"Set {ctx.channel.mention} topic:", embed=Embed.from_dict(contents)
        )

    @commands.check_any(
        checks.is_channel_mod(),
        checks.is_global_mod(),
        commands.has_guild_permissions(manage_channels=True)
    )
    @topic_group.command(guild_only=True)
    async def unset(self, ctx: ApplicationContext, channel: Optional[Channel]):
        """Remove additional information set by `topic set`.

        Requires being a staff member or the `manage_channels` permission.
        """
        if not channel:
            channel = ctx.channel
        q = "DELETE FROM topic WHERE channel = %s"
        with self.db.connection() as conn:
            conn.execute(q, (channel.id,))
        await ctx.respond("Done!", ephemeral=True)


def setup(bot: Bot):
    bot.add_cog(TopicManager(bot))
