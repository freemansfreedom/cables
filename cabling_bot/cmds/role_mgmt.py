from discord import Role, Member
from discord.ext import commands
import logging
from typing import Optional, Union

from cabling_bot.bot import (
    constants,
    CablesContext as Context,
    CablesBot as Bot
)
from cabling_bot.bot.checks import (is_staff_member, is_role_superior)
from cabling_bot.util import transform

logger = logging.getLogger(__name__)


@commands.group(name="role", aliases=["r"])
async def role_mgmt(ctx: Context):
    """Role management commands"""
    if ctx.invoked_subcommand is None:
        await ctx.fail_msg("Invalid subcommand passed")


@is_role_superior()
@commands.check_any(
    is_staff_member(),
    commands.has_guild_permissions(manage_roles=True)
)
@role_mgmt.command(name="add")
async def role_add(ctx: Context, role: Role, *users: Member, reason: Optional[str]):
    """Assign a role to one or more users

    Requires the staff role or Manage Roles permission
    """

    if reason is None:
        reason = f"On behalf of {ctx.author}"
    for user in users:
        await user.add_roles(role, reason=reason)
    await ctx.reply_success(
        f"Added the {role.mention} role to"
        f"{','.join([user.mention for user in users])}"
    )


@is_role_superior()
@commands.check_any(
    is_staff_member(),
    commands.has_guild_permissions(manage_roles=True)
)
@role_mgmt.command(name="remove", aliases=["rm"])
async def role_remove(ctx: Context, role: Role, *users: Member, reason: Optional[str]):
    """Remove a role from one or more users. If no users are specified, remove all currently assigned users from the role.

    Requires the staff role or Manage Roles permission
    """
    if reason is None:
        reason = f"On behalf of {ctx.author}"
    if len(users) == 0:
        users = role.members
    for user in users:
        await user.remove_roles(role, reason=reason)
    await ctx.reply_success(
        f"Removed the {role.mention} role from "
        f"{','.join([user.mention for user in users])}"
    )


@commands.check_any(
    is_staff_member(),
    commands.has_guild_permissions(manage_roles=True)
)
@role_mgmt.command(name="show-perms")
async def show_permissions(ctx: Context, member_or_role: Union[Member, Role]):
    """Show permissions for a member or role in the guild

    Requires the staff role or manage roles permission
    """
    perms = (
        member_or_role.guild_permissions
        if isinstance(member_or_role, Member)
        else member_or_role.permissions
    )
    msg = []
    for flag in perms.VALID_FLAGS.keys():
        if getattr(perms, flag):
            text = f"{constants.Emojis.confirm} {flag}"
        else:
            text = f"{constants.Emojis.deny} {flag}"
        msg.append(text)
    await ctx.reply(
        f"Permissions for `{member_or_role}`\n"
        + transform.codeblock("\n".join(msg))
    )


def setup(bot: Bot):
    bot.add_command(role_mgmt)
