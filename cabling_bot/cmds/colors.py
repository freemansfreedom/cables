import logging
from discord.ext import commands
from psycopg import (DatabaseError, Transaction)
from cabling_bot.bot import checks
from discord.utils import find
from typing import (List, NamedTuple, Optional, Sequence)
from discord import (Color, Forbidden, Member, Role, Embed)
from cabling_bot.bot import (
    CablesCog as Cog,
    CablesContext as Context,
    CablesBot as Bot
)

logger = logging.getLogger(__name__)


class ColorsRoleManager(Cog):
    """Color role management. See `help color` for more info."""

    def __init__(self, bot: Bot) -> None:
        super().__init__()
        self._bot = bot
        self.db = bot.db

        q = """\
            -- Role definition
            CREATE TABLE IF NOT EXISTS color_roles (
                id      serial  PRIMARY KEY,
                guild   bigint  NOT NULL,
                "role"  bigint  NOT NULL
            );
            CREATE INDEX IF NOT EXISTS idx_colorroles_guild
                ON color_roles (guild);
            -- Member-role mapping
            CREATE TABLE IF NOT EXISTS color_role_map (
                id      serial  PRIMARY KEY,
                "role"  bigint  NOT NULL REFERENCES color_roles ON DELETE CASCADE,
                member  bigint  NOT NULL
            );
            CREATE INDEX IF NOT EXISTS idx_colorrolemap_roles
                ON color_role_map ("role");
        """
        with self.db.connection() as conn:
            conn.execute(q)

        self._bot.config_keys.new(
            "colors",
            "role_position",
            ("(int) A role that any color role will be positioned below (so "
             + "long as it has a position set. if this is not set, color "
             + "roles may be overridden by higher-position roles that are "
             + "also colored"),
            "@everyone",
        )
        self._bot.config_keys.new(
            "colors",
            "allowed_role",
            "(int) A role that members who are allowed custom roles must have",
            None
        )

    def _get_member_color(self, ctx: Context, member: Member) -> Optional[Role]:
        q = """
            SELECT roles.role
            FROM color_roles AS roles
            JOIN color_role_map AS map
                ON roles.id = map."role"
            WHERE
                roles.guild = %(guild)s
                AND map.member = %(member)s;
        """
        v = dict(guild=ctx.guild.id, member=member.id)
        with self.db.connection() as conn:
            cursor = conn.execute(q, v)
            if not cursor.rowcount:
                return None
            row = cursor.fetchone()
            return ctx.guild.get_role(row.role)

    def _get_role_members(
        self, role: Role
    ) -> Optional[Sequence[NamedTuple]]:
        q = """
            SELECT map.member
            FROM color_role_map AS map
            JOIN color_roles AS roles
                ON map."role" = roles.id
            WHERE roles."role" = %(role)s;
        """
        v = dict(role=role.id)
        with self.db.connection() as conn:
            cursor = conn.execute(q, v)
            if not cursor.rowcount:
                return None
            return cursor.fetchall()

    def _db_add_role(self, guild_id: int, role_id: int, member_id: int) -> bool:
        q1 = """
            INSERT INTO color_roles (guild, "role")
                VALUES (%(guild)s, %(role)s);
        """
        q2 = """
            INSERT INTO color_role_map ("role", member)
                SELECT id, %(member)s FROM color_roles
                WHERE guild = %(guild)s AND role = %(role)s;
        """
        v = dict(guild=guild_id, role=role_id, member=member_id)
        with self.db.connection() as conn:
            try:
                with conn.transaction():
                    conn.execute(q1, v)
                    conn.execute(q2, v)
            except DatabaseError:
                return False
            else:
                return True

    def _db_get_guild_roles(self, guild_id: int) -> List[NamedTuple]:
        q = """
            SELECT roles."role", map.member
            FROM color_roles AS roles
            JOIN color_role_map AS map
                ON roles.id = map.role
            WHERE guild = %s;
        """
        with self.db.connection() as conn:
            rows = conn.execute(q, (guild_id,)).fetchall()
        return rows

    async def _find_role(
        self, ctx: Context,
        member: Member, name: str
    ) -> tuple[Optional[Role], Optional[Role], Optional[Role]]:
        color_role = None
        known_role = self._get_member_color(ctx, member)
        name_role = None
        if not known_role:
            name_role = find(lambda r: r.name == name, member.guild.roles)
            if name_role and member in name_role.members:
                color_role = name_role
        else:
            color_role = known_role
        return (color_role, name_role, known_role)

    async def _set_color(
        self, ctx: Context,
        member: Member, color: Color, name: Optional[str] = None
    ) -> Optional[bool]:
        guild = member.guild
        gid = guild.id
        if name is None:
            name = member.name

        # check if the named role already exists
        roles = await self._find_role(ctx, member, name)
        color_role, name_role, known_role = roles

        # catch noop
        if (
            (name_role or known_role)
            and color_role.color == color
        ):
            return False

        # if it doesn't exist, create it
        if not (known_role or name_role):
            try:
                color_role = await guild.create_role(
                    color=color, name=name,
                    reason=f"Custom color role requested by {member}"
                )
            except Forbidden:
                await ctx.fail_msg("Inadequate permissions.")
                return None

        # log the role if we don't know about it
        if (
            (name_role and not known_role)
            or not (name_role or known_role)
        ):
            if not self._db_add_role(guild.id, color_role.id, member.id):
                await color_role.delete()
                await ctx.react_fail()
                return

        # assign and move, if we freshly created it
        if not (name_role or known_role):
            await member.add_roles(color_role)
            p_id = ctx.get_guild_key(gid, "colors", "role_position")
            if p_id:
                position_role = guild.get_role(p_id)
                if position_role and position_role.position > 0:
                    position = (position_role.position - 1)
                    await color_role.edit(position=position)
            return True

        try:
            await color_role.edit(color=color, name=name)
        except Forbidden:
            err = "A role by that name exists, but I can't modify it."
            await ctx.fail_msg(err)
            return False
        return True

    @commands.group(name="color")
    async def color(self, ctx: Context):
        """Color role commands"""
        if ctx.invoked_subcommand is None:
            await ctx.send_help("color")

    @color.command()
    async def set(
        self, ctx: Context, color: Color, name: Optional[str]
    ) -> None:
        """Set or update your color role.

        When a name isn't chosen, it'll be named after your username."""

        # test requestor's roles against ACL
        acl = self._bot.get_guild_key(ctx.guild.id, "colors", "allowed_role")
        if acl and not bool(ctx.author.get_role(acl)):
            acl_role = ctx.guild.get_role(acl)
            err = (f"The `{acl_role.name}` role is required to use fully "
                   + "custom color roles. You _may_ have access to some "
                   + "color selections though. Ask a staff member!")
            await ctx.fail_msg(err, delete_after=30)
            await ctx.message.add_reaction("🙅‍♀️")
            return

        if not await self._set_color(ctx, ctx.author, color, name):
            await ctx.react_fail()
            return
        await ctx.react_success()

    @commands.check_any(
        checks.is_staff_member(),
        commands.has_guild_permissions(manage_roles=True)
    )
    @color.command(aliases=["saset"])
    async def assign(
        self, ctx: Context, member: Member, color: Color, name: Optional[str]
    ) -> None:
        """Set another user's color role

        Requires a staff role or the Manage roles permission."""

        await self._set_color(ctx, member, color, name)
        await ctx.react_success()

    @commands.check_any(
        checks.is_staff_member(),
        commands.has_guild_permissions(manage_roles=True)
    )
    @color.command(aliases=["show", "ls"])
    async def list(self, ctx: Context) -> None:
        """Show all bot-managed color roles in this guild

        Requires a staff role or the Manage roles permission."""

        roles = self._db_get_guild_roles(ctx.guild.id)
        role_strings = []
        for role_id, member_id in roles:
            role = ctx.guild.get_role(role_id)
            member = ctx.guild.get_member(member_id)
            if role is None:
                role_strings.append(f"{member.mention} - {role_id} (deleted)")
            else:
                role_strings.append(f"{member.mention} - {role.mention}")

        if role_strings == []:
            await ctx.reply("No color roles.")
            return

        embed = Embed(title="Color roles", description="\n".join(role_strings))
        await ctx.reply(embed=embed)

    async def _delete_role(self, role: Role, member: Member) -> Transaction:
        q = """
            DELETE FROM color_role_map AS map
            USING color_roles AS roles
            WHERE
                roles."role" = %(role)s
                AND map."role" = roles.id
                AND map.member = %(member)s;
        """
        v = dict(role=role.id, member=member.id)
        with self.db.connection() as conn:
            try:
                with conn.transaction():
                    conn.execute(q, v)
                    await role.delete()
            except (DatabaseError, Forbidden):
                return False
            else:
                return True

    def _db_delete_role(self, role: Role) -> bool:
        q = """
            DELETE FROM color_roles
            WHERE guild = %(guild)s AND role = %(role)s;
        """
        v = dict(guild=role.guild.id, role=role.id)
        with self.db.connection() as conn:
            cursor = conn.execute(q, v)
            return bool(cursor.rowcount)

    @color.command(aliases=["unset"])
    async def off(self, ctx: Context) -> None:
        """Remove your color role."""

        role = self._get_member_color(ctx, ctx.author)
        if role is None:
            await ctx.fail_msg("You don't have a color role 😕")
            return

        if not await self._delete_role(role, ctx.author):
            await ctx.fail_msg("Inadequate permissions.")
            return

        if not self._get_role_members(role):
            self._db_delete_role(role)

        await ctx.react_success()


def setup(bot: Bot):
    bot.add_cog(ColorsRoleManager(bot))
