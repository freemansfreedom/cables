from __future__ import annotations
from typing import TYPE_CHECKING, Optional

import logging
from discord import Embed, Guild, Member, Role
from discord.ext import commands
from discord import option
from cabling_bot.bot.errors import report_guild_error

from cabling_bot.bot import (
    CablesCog as Cog,
    CablesAppContext as ApplicationContext
)
from cabling_bot.bot.exceptions import FeatureDisabled

if TYPE_CHECKING:
    from cabling_bot.bot import CablesBot as Bot

logger = logging.getLogger(__name__)


class RoleStatsManager(Cog):
    """Statistics for cosmetic roles. See `help rolestats` for more info."""

    def __init__(self, bot: Bot) -> None:
        super().__init__()
        self._bot = bot
        bot.config_keys.new(
            "stats", "role_categories",
            ("(dict[dict]) Object whose keys are named role categories, and "
             + "whose value contains a `color` key with an RGB hex value."),
            None
        )

    def enabled(self, guild: Guild) -> dict:
        response = self.get_guild_key(
            guild.id,
            "stats", "role_categories"
        )
        if not response:
            raise FeatureDisabled
        return response

    async def _get_guild_settings(
        self,
        ctx: ApplicationContext,
        category: Optional[str]
    ) -> Optional[dict]:
        settings = self.enabled(ctx.guild)

        err = False
        prefix = f"User tried to use `rolestats {category}`, but "
        try:
            settings[category]
        except KeyError:
            err = (prefix + "that role category isn't configured.")
        try:
            settings[category]["color"]
        except KeyError:
            err = (prefix + "its configuration is invalid. `color` attribute "
                   + "must be an RBG hex code.")
        try:
            settings["title"]
        except KeyError:
            settings["title"] = f"{category.capitalize()} Statistics"
        try:
            settings["user_label"]
        except KeyError:
            settings["user_label"] = "users"

        if not err:
            return settings[category]

        await report_guild_error(self._bot, ctx, None, ctx.guild, err)
        return None

    def _get_roles(self, guild: Guild, color: int) -> Optional[list[Role]]:
        return list(filter(
            lambda r: (r.color.value == color),
            guild.roles
        ))

    def _parse_roles(
        self,
        roles: list[Role],
        color: int
    ) -> tuple[dict, int, list, dict]:
        role_map, selections, selectors = dict(), 0, list()
        for role in roles:
            role: Role
            role_members = len(role.members)
            role_map |= {
                f"{role}": {
                    "id": role.id,
                    "count": role_members
                }
            }
            selections += role_members
            selectors += list(set(role.members) - set(selectors))
        sorted_roles = sorted(
            role_map.items(),
            key=lambda tup: (-tup[1]["count"])
        )

        member_map = dict()
        for m in selectors:
            m: Member
            member_roles: list[Role] = list(filter(
                lambda r: (r.color.value == color),
                m.roles
            ))
            member_map |= {
                f"{m}": {
                    "id": m.id,
                    "count": len(member_roles),
                }
            }
        sorted_members = sorted(
            member_map.items(),
            key=lambda tup: (-tup[1]["count"])
        )

        return (sorted_roles, selections, selectors, sorted_members)

    def _craft_description(
        self,
        category: str, settings: dict,
        roles: list, parsed: tuple
    ) -> str:
        choices = len(roles)
        if choices < 10:
            return str()

        _, selections, selectors, member_roles = parsed
        users = len(selectors)
        user_label = settings['user_label']
        median_idx = int(((len(member_roles) + 1) / 2) - 1)
        median = member_roles[median_idx][1]["count"]
        mean = int(selections / users)
        desc = (f"Out of **{choices}** {category}, "
                + f"**{users}** {user_label} made an average of "
                + f"**{mean}** choices")
        if mean != median:
            desc += f", with the median being **{median}**."

        high = member_roles[0][1]["count"]
        leaders = list(filter(
            lambda tup: tup[1]["count"] == high,
            member_roles
        ))
        if leaders:
            desc += " At the top end, "
            leader_count = len(leaders)
            if leader_count > 1:
                desc += f"**{leader_count}** {user_label} "
            else:
                desc += f"<@{leaders[0][1]['id']}> "
            desc += f"made **{high}** choices."

        return desc

    def _add_embed_fields(self, embed: Embed, role_counts: dict) -> Embed:
        c = 1
        for name, attribs in role_counts:
            if c > 12:
                break
            embed.add_field(
                name=name,
                value=f"`{attribs['count']}`",
                inline=True
            )
            c += 1
        return embed

    @commands.slash_command(name="rolestats", guild_only=True)
    @option("role_category", str, description="Name of a category of roles to show statistics for.")
    async def show(self, ctx: ApplicationContext, role_category: str) -> None:
        """Show role election statistics for a given category."""
        guild = ctx.guild
        if not role_category:
            await ctx.respond("A role category is required.", ephemeral=True)
            return

        settings = await self._get_guild_settings(ctx, role_category)
        if not settings:
            return

        role_color = int(settings["color"], 16)
        roles = self._get_roles(guild, role_color)
        if not roles:
            await ctx.respond("No roles exist in that category.")
            return

        parsed = self._parse_roles(roles, role_color)
        role_counts, _, selectors, member_roles = parsed
        if not len(member_roles):
            await ctx.respond("No one has elected roles from that category.")
            return
        users = len(selectors)
        if (users < 10):
            await ctx.respond("Need more votes to produce statistics.")
            return

        title = f"📊 {settings['title']}"
        desc = self._craft_description(role_category, settings, roles, parsed)
        embed = Embed(title=title, description=desc, color=role_color)
        embed = self._add_embed_fields(embed, role_counts)
        await ctx.respond(embeds=[embed])


def setup(bot: Bot):
    bot.add_cog(RoleStatsManager(bot))
