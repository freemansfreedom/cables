from __future__ import annotations
from typing import TYPE_CHECKING

import json
import logging
from discord.ext import commands

from cabling_bot.util.privileged import (
    list_administrators,
    remove_staff_roles
)
from cabling_bot.util.sanctions import (
    check_safeties,
    process_vote,
    notify_user
)

from typing import (Optional, Union)
from discord import Member, User
from cabling_bot.bot import (
    CablesCog as Cog,
    CablesContext as Context,
    checks
)

if TYPE_CHECKING:
    from typing import NamedTuple
    from cabling_bot.bot import CablesBot as Bot


logger = logging.getLogger(__name__)


class BanCmdManager(Cog):
    """Manage user bans for a guild. See `help ban` for more info."""

    def __init__(self, bot: Bot) -> None:
        super().__init__()
        self._bot = bot
        self.db = bot.db

        q = """\
            -- TABLE
            -- Configuration
            CREATE TABLE IF NOT EXISTS guild_bans (
                guild       bigint      NOT NULL,
                "user"      bigint      NOT NULL,
                reason      text        NULL,
                created_by  bigint      NOT NULL,
                created_at  timestamp   NOT NULL DEFAULT CURRENT_TIMESTAMP
            );
            CREATE UNIQUE INDEX IF NOT EXISTS idx_guildbans_guilduser
                ON guild_bans (guild, "user");

            -- TABLE
            -- Audit log
            CREATE TABLE IF NOT EXISTS guild_bans_history (
                guild       bigint      NOT NULL,
                "user"      bigint      NOT NULL,
                reason      text        NULL,
                created_by  bigint      NOT NULL,
                created_at  timestamp   NOT NULL,
                deleted_by  bigint      NULL DEFAULT NULL,
                deleted_at  timestamp   NULL DEFAULT NULL
            );
            CREATE UNIQUE INDEX IF NOT EXISTS idx_guildbanshistory_guilduser
                ON user_watches (guild, "user");

            -- TRIGGER
            -- Populate audit log on record insertion from configuration table
            -- Record deletion is handled by the application
            CREATE OR REPLACE FUNCTION sp_guild_bans_history_insert()
                RETURNS TRIGGER AS $$
                BEGIN
                    INSERT INTO guild_bans_history
                        (guild, "user", reason, created_by, created_at)
                    VALUES
                        (NEW.guild, NEW."user", NEW.reason, NEW.created_by, NEW.created_at);
                    RETURN NEW;
                END;
                $$ LANGUAGE plpgsql;
            CREATE OR REPLACE TRIGGER tg_guild_bans_insert
                AFTER INSERT ON guild_bans
                FOR EACH ROW EXECUTE PROCEDURE sp_guild_bans_history_insert();
            """
        with self.db.connection() as conn:
            conn.execute(q)

        bot.config_keys.new(
            "moderation", "ban_appeals",
            "(bool) Allows banned users to appeal the judgement.",
            False
        )
        bot.config_keys.new(
            "moderation", "bans_revoke_privs",
            ("(bool) When banning a staff member, try to remove their privileged roles first. "
             + "Useful for the situation where member roles are restored on rejoin."
             + "Requires the bot's role to be higher than all staff roles."),
            False
        )

    @commands.group()
    async def ban(self, ctx: Context):
        """Manage server-wide user bans."""
        if ctx.invoked_subcommand is None:
            await ctx.fail_msg("Invalid command passed")

    def _get(self, gid: int, uid: int) -> NamedTuple:
        sql = """
            SELECT created_by, created_at, reason
            FROM guild_bans
            WHERE guild = %s AND "user" = %s
        """
        values = (gid, uid)
        with self.db.connection() as conn:
            result = conn.execute(sql, values).fetchone()

        return result

    @commands.check_any(
        checks.is_guild_owner(),
        checks.is_admin(),
        commands.has_guild_permissions(ban_members=True),
    )
    @checks.guild_in_purgatory()
    @checks.check_sanction_reason()
    @commands.guild_only()
    @ban.command(aliases=[])
    async def add(
        self, ctx: Context,
        target: Union[Member, User],  # NOTE: Union'd types are Converted serially
        *, reason: Optional[str]
    ) -> None:
        """Ban a user

        `ban add <user> [reason]`

        To ban someone not on the server, paste a mention or their user ID

        Requires an administrator role or the 'Ban Members' permission
        """
        guild, issuer = ctx.guild, ctx.author

        if self._get(ctx.guild.id, target.id):
            text = f"{target.mention} is _already_ banned, sweetie 🤭"
            await ctx.fail_msg(text, delete_after=-1)
            return

        admins = list_administrators(ctx, guild)
        if not await check_safeties(ctx, target, issuer, admins):
            return

        vote = await process_vote(ctx, "ban", target, issuer, admins)
        if vote is False:
            return
        elif vote is True:
            issuer = "administrator quorum"

        if (
            ctx.get_guild_key(guild, "moderation", "bans_revoke_privs")
            and await remove_staff_roles(ctx, target, reason="Banned") is False
        ):
            text = ("Failed to remove staff roles. "
                    + f"{target.mention} was **not** banned.")
            await ctx.fail_msg(text)
            return

        await notify_user(ctx, target, issuer, reason)
        audit = json.dumps(dict(issuer=ctx.author.id, text=reason))
        await guild.ban(target, reason=audit, delete_message_days=0)

        if ctx.channel == guild.system_channel:
            return
        if not vote:
            await ctx.react_success()
        else:
            text = f"{target.mention} ({target}) has been banned"
            text += (f" by {issuer}" if issuer else "")
            await ctx.reply(text)


def setup(bot: Bot):
    bot.add_cog(BanCmdManager(bot))
