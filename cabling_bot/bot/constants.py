from enum import Enum
from discord import Status


class DevGuildTemplate():
    v1: str = "https://discord.new/uESRDzecSVaK"
    v2: str = "https://discord.new/s3VZtVYZRDSV"
    current = v2


class UserStatuses():
    available: list[str] = [Status.online.name]
    busy: list[str] = [Status.dnd.name, Status.streaming.name]
    present: list[str] = available + busy
    afk: list[str] = [Status.idle.name]
    online: list[str] = present + afk
    unknown: list[str] = [Status.offline.name]
    all: list[str] = online + unknown


class BotRatio(Enum):
    min = 1.7
    max = 1.1


class Colours():
    blue: int = 0x3775a8
    bright_green: int = 0x01d277
    orange: int = 0xe67e22
    pink: int = 0xcf84e0
    purple: int = 0xb734eb
    soft_green: int = 0x68c290
    soft_orange: int = 0xf9cb54
    soft_red: int = 0xcd6d6d
    white: int = 0xfffffe
    yellow: int = 0xffd241
    greyblue: int = 0x7aa7bf
    steelblue: int = 0x4682b4
    lightsteelblue: int = 0xb0c4de
    deepskyblue: int = 0x00bfff
    wizzypurple: int = 0xaea8d3
    barely_red: int = 0xbf9f9d
    role_red: int = 0x855d5d
    degenerate_black: int = 0x646464
    midnight_bluish: int = 0x303287
    dark_pastel_purple: int = 0x974376
    dark_goldenrod: int = 0xbd8c26
    dark_orange: int = 0xc5562a
    worn_leather: int = 0x8d6753
    blood_red: int = 0x880808
    purplish_red: int = 0x92113f
    thistle: int = 0xd8bfd8


class Icons():
    # Symbolic
    pencil: str = 'https://cdn.discordapp.com/emojis/470326272401211415.png'  # generic modification
    checkmark_green: str = 'https://cdn.discordapp.com/emojis/470326274519334936.png'  # confirmation
    exclaim_red: str = 'https://cdn.discordapp.com/emojis/470326273298792469.png'  # exclamation point
    qmark_blue: str = 'https://cdn.discordapp.com/emojis/512367613339369475.png'  # question mark
    alarm_green: str = 'https://cdn.discordapp.com/emojis/477907607785570310.png'  # reminder added
    alarm_blue: str = 'https://cdn.discordapp.com/emojis/477907609215827968.png'  # reminder edit or sounding off
    alarm_red: str = 'https://cdn.discordapp.com/emojis/477907608057937930.png'  # reminder removed or expired
    star_red: str = 'https://cdn.discordapp.com/emojis/636288153044516874.png'
    star_green: str = 'https://cdn.discordapp.com/emojis/636288201258172446.png'
    hash_blue: str = 'https://cdn.discordapp.com/emojis/469950142942806017.png'  # invite detail
    hash_green: str = 'https://cdn.discordapp.com/emojis/469950144918585344.png'  # invite created
    hash_red: str = 'https://cdn.discordapp.com/emojis/469950145413251072.png'  # invite revoked or expired
    crown_blue: str = 'https://cdn.discordapp.com/emojis/469964153289965568.png'  # admin modification
    crown_green: str = 'https://cdn.discordapp.com/emojis/469964154719961088.png'  # admin confirmation
    crown_red: str = 'https://cdn.discordapp.com/emojis/469964154879344640.png'  # admin denial
    shield_red_deny: str = 'https://cdn.discordapp.com/emojis/472475292078964738.png'  # defcon action (defcon_denied)
    shield_red: str = 'https://cdn.discordapp.com/emojis/470326273952972810.png'  # defcon disarm (defcon_shutdown)
    shield_green_check: str = 'https://cdn.discordapp.com/emojis/470326274213150730.png'  # defcon armed (defcon_unshutdown)
    shield_blue_gear: str = 'https://cdn.discordapp.com/emojis/472472638342561793.png'  # decon policy change (defcon_update)

    # Chat
    user_join: str = 'https://cdn.discordapp.com/emojis/469952898181234698.png'
    user_part: str = 'https://cdn.discordapp.com/emojis/469952898089091082.png'
    user_ban: str = 'https://cdn.discordapp.com/emojis/469952898026045441.png'
    user_unban: str = 'https://cdn.discordapp.com/emojis/469952898692808704.png'
    user_mute: str = 'https://cdn.discordapp.com/emojis/472472640100106250.png'
    user_unmute: str = 'https://cdn.discordapp.com/emojis/472472639206719508.png'
    user_update: str = 'https://cdn.discordapp.com/emojis/469952898684551168.png'
    user_warn: str = 'https://cdn.discordapp.com/emojis/470326274238447633.png'
    message_bulk_delete: str = 'https://cdn.discordapp.com/emojis/469952898994929668.png'
    message_delete: str = 'https://cdn.discordapp.com/emojis/472472641320648704.png'
    message_edit: str = 'https://cdn.discordapp.com/emojis/472472638976163870.png'
    voice_state_blue: str = 'https://cdn.discordapp.com/emojis/656899769662439456.png'
    voice_state_green: str = 'https://cdn.discordapp.com/emojis/656899770094452754.png'
    voice_state_red: str = 'https://cdn.discordapp.com/emojis/656899769905709076.png'

    # Emoji
    emergency: str = 'https://cdn.discordapp.com/emojis/808420164081156196.gif'
    clown: str = 'https://cdn.catgirl.technology/img/clown-face_1f921.png'
    goblin: str = 'https://cdn.catgirl.technology/img/japanese-goblin_1f47a.png'
    honeypot: str = 'https://cdn.catgirl.technology/img/honeypot.png'


class Emojis():
    attachment: str = "📎"
    link: str = "🔗"
    confirm: str = "✅"
    deny: str = "❌"
    cancel: str = "🚫"
    warning: str = "⚠"
    over_18: str = "🔞"
    speech_bubble: str = "💬"
    thinking: str = "💭"
    sauron: str = "👁️‍🗨️"
    eyes: str = "👀"
    ahegao: str = "🥵"
    no_gesture: str = "🙅‍♀️"
    voice: str = "🔉"
    speaker_muted: str = "🔇"
    mega: str = "📢"
    stand_mic: str = "🎙"
    hand_mic: str = "🎤"
    notes_on: str = "📳"
    notes_muted: str = "📵"
    departure: str = "🛫"
    arrival: str = "🛬"
    users: str = "👥"
    profile: str = "👤"
    thread: str = "🧵"
    idle: str = "💤"
    abacus: str = "🧮"
    brush: str = "🖌"
    cake: str = "🎂"
    calendar: str = "📅"
    sticker: str = "🥟"
    smile: str = "🙂"
    frown: str = "🙁"
    loading: str = "⏳"
    owner: str = "👑"
    wand: str = "🪄"
    janny: str = "🧹"
    rocket: str = "🚀"
    sparkle: str = "✨"
    cop: str = "👮"
    backpack: str = "🎒"
    magnify: str = "🔍"
    robot: str = "🤖"
    money_bag: str = "💰"
    knot: str = "🪢"
    neighborhood: str = "🏘"
    mammoth: str = "🦣"
    administrator: str = "🧑‍💼"
    amulet: str = "<:amulet:1009960578116223006>"
    nitro: str = "<:nitro:1009682516808060958>"
    nitro_disabled: str = "<:nitro_disabled:1010687716066861168>"
    hypesquad: str = "<:hypesquad:1010684515695464539>"


class Symbols():
    mdash: str = "—"


# https://github.com/Delitefully/DiscordLists/blob/master/flags.md
class UserFlags():
    mfa_sms_recovery: int = (1 << 4)
    partnership_pending: int = (1 << 11)
    unread_system_messages: int = (1 << 13)
    underage_blocked: int = (1 << 15)  # account pending deletion
    spammer: int = (1 << 20)
    nitro_disabled: int = (1 << 21)


class GuildNsfwFilter(Enum):
    disabled: str = Emojis.deny
    no_role: str = Emojis.confirm
    all_members: str = Emojis.over_18


class GuildNsfwLevel(Enum):
    default: str = "be unclassified"
    explicit: str = "be focused on explicit content"
    safe: str = "be safe for all ages"
    age_restricted: str = "have age-restricted content"


class GuildNoteLevel(Enum):
    all_messages: str = Emojis.notes_on
    only_mentions: str = Emojis.notes_muted


class BadTags():
    ehentai: tuple = (
        "female:lolicon",
        "female:oppai loli",
        "male:shotacon",
        "female:toddlercon",
        "male:toddlercon",
        "female:low lolicon",
        "male:low shotacon",
        "other:forbidden content",
        "male:tomgirl"
    )
    nhentai: tuple = (
        "lolicon",
        "oppai loli",
        "shotacon",
        "low lolicon",
        "low shotacon",
        "forbidden content",
        "tomgirl"
    )
    fragments: tuple = (
        "highschool",
        "high school",
        "middle school",
        "grade school",
        "years old",
        "year old",
        "lolicon",
        "pedophile",
        "preteen",
        "little girl"
        "little sister",
        "younger sister",
        "imouto",
        "little boy",
        "little brother",
        "younger brother",
        "otouto",
        "shougakkou",
        "chuugaku"
    )
    hydrus: tuple = (
        "meta:blocked artist",
        "meta:contentious content",
        "age difference",
        "size difference",
        "straight shota",
        "underage sex",
        "sholicon",
        "imouto",
        "loli",
        "lolidom",
        "underaged female",
        "young female",
        "shota",
        "underaged male",
        "young male",
        "toddler",
        "toddlercon",
        "oppai loli",
        "underaged",
        "teenager"
        "age:teen",
        "teenage girl and younger boy",
        "teenage girl and younger girl",
        "high-school girl",
        "high school student",
        "middle schooler",
        "elementary school student",
        "age:preteen",
        "age:toddler",
        "child",
        "cub",
        "cubs",
        "cub porn",
        "cub sex",
        "cub/adult",
        "adult/cub",
        "foal",
        "foalcon",
        "implied foalcon",
        "young",
        "age:young",
        "younger",
        "younger anthro",
        "younger human",
        "younger brother",
        "younger penetrated",
        "age:17",
        "age:16",
        "age:15",
        "age:14",
        "age:13",
        "age:12",
        "age:11",
        "age:10",
        "age:9",
        "age:8",
        "age:7",
        "age:6",
        "age:5",
        "age:4",
        "age:3",
        "age:2",
        "twintails",
        "twin braids",
        "randoseru",
        "site:lolibooru.moe",
        "site:exhentai",
        "character:pokemon character",
    )
