import logging
import json
import discord.utils
from typing import Union

from discord import (Guild, InvalidArgument, NotFound, Role)
from discord.ext.commands.errors import (RoleNotFound)
from cabling_bot.bot import exceptions as CablesException
from cabling_bot.bot import (
    CablesContext as Context,
    CablesAppContext as ApplicationContext
)

from discord.ext.commands import check
from cabling_bot.util.privileged import (
    list_administrators,
    list_global_moderators,
    is_channel_moderator
)


logger = logging.getLogger(__name__)


def is_feature_enabled(feature: str) -> bool:
    async def predicate(ctx: Union[ApplicationContext, Context]):
        gid = ctx.guild.id
        if not ctx.get_guild_key(gid, "feature_flags", feature):
            raise CablesException.FeatureDisabled
        return True
    return check(predicate)


def is_rce_enabled() -> bool:
    async def predicate(ctx: Context):
        _enabled = bool(
            ctx.bot.get_config("rce_exploit", False)
            and ctx.bot.get_config("clown_mode", False)
        )
        if not _enabled or ctx.bot.environment == "production":
            raise CablesException.RceDisabled
        return True
    return check(predicate)


def _is_app_owner(ctx: Context) -> bool:
    return ctx.author.id == ctx.bot.operator.id


def is_app_owner() -> bool:
    """Returns truthy only when the requestor is that who created the bot application.

    This differs from the built-in `is_owner()` check, which returns truthy for:
    - Any user listed in `Bot.owner_ids`, inclusive of the user who created the bot application`
    - Any application tester as configured from the Discord Developer panel for the application
    """
    async def predicate(ctx: Context):
        if not _is_app_owner(ctx):
            raise CablesException.GuildOwnerOnlyError
        return True
    return check(predicate)


def is_dev_guild_reconfig() -> bool:
    async def predicate(ctx: Context):
        if not ctx.bot.dev_guild:
            return None
        else:
            return (
                ctx.bot.dev_guild.id == ctx.guild.id
                and _is_app_owner(ctx)
            )
    return check(predicate)


def is_guild_owner() -> bool:
    async def predicate(ctx: Context):
        guild = ctx.guild
        try:
            owner = await guild.fetch_member(guild.owner.id)
        except NotFound:
            owner = None
        if (
            ctx.get_guild_key(ctx.guild.id, "defcon", "ownerless_safety")
            and not owner
        ):
            raise CablesException.DefconConstraintViolation
        elif ctx.author != owner:
            raise CablesException.GuildOwnerOnlyError
        return True
    return check(predicate)


def is_admin() -> bool:
    async def predicate(ctx: Context):
        admins = list_administrators(ctx, ctx.guild)
        if ctx.author not in admins:
            raise CablesException.StaffOnlyError
        return True
    return check(predicate)


def is_global_mod() -> bool:
    async def predicate(ctx: Context):
        global_mods = list_global_moderators(ctx, ctx.guild)
        if ctx.author not in global_mods:
            raise CablesException.GmodsOnlyError
        return True
    return check(predicate)


def is_channel_mod() -> bool:
    async def predicate(ctx: Context):
        if not is_channel_moderator(ctx):
            raise CablesException.ChanopsOnlyError
        return True
    return check(predicate)


def is_staff_member() -> bool:
    async def predicate(ctx: Context):
        role = ctx.bot.get_privileged_role("staff", ctx.guild)
        if (role and role not in ctx.author.roles):
            raise CablesException.StaffOnlyError
        return True
    return check(predicate)


def is_role_superior() -> bool:
    async def predicate(ctx: Context):
        _content = ctx.message.clean_content.split(" ")
        cmd = _content[0].strip(ctx.bot.command_prefix)
        cmd_args = _content[1:]
        if cmd == "help":
            return True

        # examples:
        #   ['^role' 'add' 1234567890 'userid' ...]
        #   ['^role' 'add' 'rolename' 'userid' ...]
        #   ['^role' 'add' '"role' 'name"' 'userid' ...]
        try:
            proposed_role: Role = ctx.message.role_mentions[0]
        except IndexError:
            # assume cmd has a subcommand 😩
            role_name = cmd_args[1]
            # detect multi-word role names
            if role_name.startswith("\""):
                role_name = ctx.message.content.split("\"")[1]

            # get role object from name
            proposed_role: Role = discord.utils.find(
                lambda r: r.name == role_name, ctx.guild.roles
            )
            # NOTE: are there performance differences between `find` and `get`?
            # proposed_role: Role = discord.utils.get(
            #     ctx.guild.roles,
            #     name=role_name
            # )

        if not proposed_role:
            raise RoleNotFound(role_name)

        # check given role against bot's highest role
        if proposed_role > ctx.guild.me.top_role:
            raise CablesException.BotRoleInferior
        # check given role against command issuer's highest role
        elif proposed_role > ctx.author.top_role:
            raise CablesException.MemberRoleInferior

        return True

    return check(predicate)


def guild_in_purgatory() -> bool:
    async def predicate(ctx: Context):
        guild = ctx.message.guild
        try:
            owner = await guild.fetch_member(guild.owner.id)
        except NotFound:
            owner = None
        if (
            ctx.get_guild_key(ctx.guild.id, "defcon", "ownerless_safety")
            and not owner
        ):
            raise CablesException.DefconConstraintViolation
        return True
    return check(predicate)


def check_sanction_reason() -> bool:
    """Requires a reason for the action be provided as the 2nd arg of a subcommand.

    Example: `ban add <user> <reason>`"""
    async def predicate(ctx: Context):
        msg = ctx.message.content
        if msg.startswith(f"{ctx.prefix}help"):
            return True

        enabled = ctx.bot.get_guild_key(
            ctx.guild.id,
            "moderation", "require_sanction_reasons"
        )
        if not enabled:
            return True

        fields = len(msg.split(" "))
        if fields < 4:
            raise CablesException.AuditReasonRequired

        reason = " ".join(msg.split(" ")[3:])
        audit = json.dumps(dict(issuer=ctx.author.id, text=reason))
        length = len(reason)
        max_length = 512 - (len(audit) - len(reason))
        if length > max_length:
            # NOTE: couldn't figure out how to pass this to the exception handler
            bounds = f"**{length}**/{max_length}"
            reply = f"Sorry, the reason must\\* be shorter! ({bounds})\n\n"
            reply += "\\*Audit log messages are limited to 512 characters, "
            reply += "but we use some of that for metadata."
            await ctx.fail_msg(reply)
            raise CablesException.AuditReasonOverloaded

        return True

    return check(predicate)


async def _find_guild_in_args(ctx: Context):
    args = ctx.message.content.split(" ")
    guild = None
    while len(args) > 2:
        arg = args.pop(2)
        if type(arg) is str:
            guild = discord.utils.get(ctx.bot.guilds, name=arg)
            if guild:
                break
        elif type(arg) is int:
            try:
                guild = await ctx.bot.fetch_guild(arg)
                break
            except NotFound:
                pass
        else:
            raise InvalidArgument
    if not guild:
        return None
    return guild


def has_guild_membership():
    """Require the requestor to be a member of the given guild.

    Bot owners are immune to this check."""
    async def predicate(ctx: Context):
        guild = await _find_guild_in_args(ctx)
        if (
            type(guild) is Guild
            and guild not in ctx.author.mutual_guilds
            and ctx.author.id not in ctx.bot.owner_ids
        ):
            raise CablesException.GuildMembershipError
        return True
    return check(predicate)


def is_guild_available():
    """Checks if a guild passed is available through the gateway."""
    async def predicate(ctx: Context):
        guild = await _find_guild_in_args(ctx)
        if not guild:
            guild = ctx.guild
        if guild.unavailable:
            raise CablesException.GuildUnavailable
        return True
    return check(predicate)
