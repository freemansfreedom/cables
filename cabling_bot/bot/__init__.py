from typing import TYPE_CHECKING

import inspect
import json
import logging
import os
import toml
import time
import discord

from typing import (Any, Dict, List, Optional, Union, TypeVar)
from discord import (
    ApplicationCommand, Interaction, Option,
    ClientUser, User,
    Guild, Member, Role,
    CategoryChannel, StageChannel, VoiceChannel,
    TextChannel, DMChannel, Thread,
    Message, PartialMessageable, Colour, Embed,
)
from discord.ext.commands import Command
from discord.errors import NotFound
from discord.ext.commands.view import StringView
from discord.state import ConnectionState
from discord.ext.commands.bot import (AutoShardedBot, Bot)

import discord.commands
import discord.ext.commands
import discord.utils
from discord.utils import (cached_property, find)
from cabling_bot.bot.config import (
    ConfigKeys,
    GuildConfig,
    MemberConfig,
    get_with_env_override,
)
import cabling_bot.bot.config as bot_config
from cabling_bot.bot import constants
from git.repo import Repo
from cabling_bot.services.postgresql import DatabaseService
from ec2_metadata import ec2_metadata
from cabling_bot.services.aws_secrets_manager import SecretsManagerService
from cabling_bot.services.cryptography import CryptographyService
from cabling_bot.services.reddit import RedditService
from cabling_bot.services.hydrus import HydrusService
from cabling_bot.bot.oplog import OpLog


T = TypeVar("T")
BotT = TypeVar("BotT", bound="Union[Bot, AutoShardedBot, CablesBot]")
InteractionChannel = TypeVar(
    "InteractionChannel",
    bound="Union[VoiceChannel,StageChannel,TextChannel,CategoryChannel,Thread,PartialMessageable]"
)
MISSING: Any = discord.utils.MISSING

logger = logging.getLogger(__name__)


class CablesCog(discord.ext.commands.Cog):
    @staticmethod
    def init_bot_config(db) -> ConfigKeys:
        return bot_config.ConfigKeys(db)

    @staticmethod
    def init_guild_config(db, guild: int) -> Optional[GuildConfig]:
        return bot_config.GuildConfig(db, guild)

    @staticmethod
    def init_member_config(db, guild: int, member: int) -> Optional[MemberConfig]:
        return bot_config.MemberConfig(db, guild, member)

    def get_guild_key(self, guild: int, module: str, key: str) -> Any:
        if TYPE_CHECKING:
            self._bot: CablesBot
        return self._bot.get_guild_key(guild, module, key)


class CablesContext(discord.ext.commands.Context):
    # we override this to modify the bot type
    # enables intelligent type checking in IDEs to for our methods
    def __init__(
        self,
        *,
        message: Message,
        bot: BotT,
        view: StringView,
        args: List[Any] = MISSING,
        kwargs: Dict[str, Any] = MISSING,
        prefix: Optional[str] = None,
        command: Optional[Command] = None,
        invoked_with: Optional[str] = None,
        invoked_parents: List[str] = MISSING,
        invoked_subcommand: Optional[Command] = None,
        subcommand_passed: Optional[str] = None,
        command_failed: bool = False,
        current_parameter: Optional[inspect.Parameter] = None,
    ):
        self.message: Message = message
        self.bot: BotT = bot
        self.args: List[Any] = args or []
        self.kwargs: Dict[str, Any] = kwargs or {}
        self.prefix: Optional[str] = prefix
        self.command: Optional[Command] = command
        self.view: StringView = view
        self.invoked_with: Optional[str] = invoked_with
        self.invoked_parents: List[str] = invoked_parents or []
        self.invoked_subcommand: Optional[Command] = invoked_subcommand
        self.subcommand_passed: Optional[str] = subcommand_passed
        self.command_failed: bool = command_failed
        self.current_parameter: Optional[inspect.Parameter] = current_parameter
        self._state: ConnectionState = self.message._state

    # TODO: update to support interactions
    async def fail_msg(
        self,
        content: str,
        channel: Union[TextChannel, DMChannel] = None,
        reply: Optional[bool] = True,
        delete_after: Optional[float] = 15.0,
    ):
        """Add a failure reaction and send a pretty error message with an embed.

        By default, the message will be deleted after 15 seconds.

        If no channel is provided, the context will be the message target.

        :param ctx: Context
        :param content: Message content of reply to author
        :param channel: A channel object to send the message to
        :param reply: Reply to the message in context
        :param delete_after: Number of seconds to wait in background before deleting our message
        """
        embed = Embed(description=f"{content}", color=Colour.red())
        try:
            if delete_after >= 0:
                footer = f"This error message will clear in {int(delete_after)} seconds."
                embed.set_footer(text=footer)
            else:
                delete_after = None

            reference = self.message if reply else None

            if not channel:
                await self.send(embed=embed, reference=reference, delete_after=delete_after)
            else:
                await channel.send(embed=embed, reference=reference, delete_after=delete_after)
        except NotFound:
            await self.author.send(content)

    async def react_success(self):
        """Add a ✅ react to the user's command message"""
        await self.message.add_reaction(constants.Emojis.confirm)

    async def react_fail(self):
        """Add a ❌ react to the user's command message"""
        await self.message.add_reaction(constants.Emojis.deny)

    async def reply_success(self, content: str):
        embed = Embed(description=content, color=0x3BA55D)
        try:
            await self.reply(embed=embed)
        except NotFound:
            await self.author.send(embed=embed)

    def get_guild_key(self, guild: int, module: str, key: str) -> Any:
        return self.bot.get_guild_key(guild, module, key)

    @property
    def config(self) -> ConfigKeys:
        return bot_config.ConfigKeys(self.bot.db)

    @property
    def guild_config(self) -> Optional[GuildConfig]:
        if self.guild:
            return bot_config.GuildConfig(self.bot.db, self.guild.id)
        else:
            return None

    @property
    def member_config(self) -> Optional[MemberConfig]:
        if self.guild:
            return bot_config.MemberConfig(self.bot.db, self.guild.id, self.author.id)
        else:
            return None


class CablesAppContext(discord.commands.ApplicationContext):
    def __init__(self, bot: BotT, interaction: Interaction):
        self.bot = bot
        self.interaction = interaction
        self.command: ApplicationCommand = None
        self.focused: Option = None
        self.value: str = None
        self.options: dict = None
        self._state: ConnectionState = self.interaction._state

    def get_guild_key(self, guild: int, module: str, key: str) -> Any:
        return self.bot.get_guild_key(guild, module, key)

    @property
    def config(self) -> ConfigKeys:
        return bot_config.ConfigKeys(self.bot.db)

    @property
    def guild_config(self) -> Optional[GuildConfig]:
        if self.guild:
            return bot_config.GuildConfig(self.bot.db, self.guild.id)
        else:
            return None

    @property
    def member_config(self) -> Optional[MemberConfig]:
        if self.guild:
            return bot_config.MemberConfig(self.bot.db, self.guild.id, self.author.id)
        else:
            return None

    @cached_property
    def channel(self) -> Optional[InteractionChannel]:
        return self.interaction.channel

    @cached_property
    def guild(self) -> Optional[Guild]:
        return self.interaction.guild

    @cached_property
    def me(self) -> Optional[Union[Member, ClientUser]]:
        return (
            self.interaction.guild.me
            if self.interaction.guild is not None
            else self.bot.user
        )

    @cached_property
    def message(self) -> Optional[Message]:
        return self.interaction.message

    @cached_property
    def user(self) -> Optional[Union[Member, User]]:
        return self.interaction.user

    author: Optional[Union[Member, User]] = user


class CablesBot(discord.ext.commands.Bot):
    def __init__(
        self,
        command_prefix,
        environment,
        app_path,
        config_path,
        help_command=None,
        description: str = None,
        **options
    ):
        super().__init__(
            command_prefix,
            help_command=help_command,
            description=description,
            **options,
        )

        self.api_endpoint = "https://canary.discord.com/api/v10"

        # runtime attributes
        self.environment = environment
        self.app_path = app_path
        self.config_path = config_path
        self.repo = Repo(self.app_path)
        self.dev_guild: Optional[Guild] = None
        self.debug_channel: Optional[TextChannel] = None
        self.operator: Optional[User] = None

        # configuration
        with open(f"{self.app_path}/pyproject.toml", "r") as f:
            _toml = toml.load(f)
            self.project_config = _toml["tool"]["poetry"]
        with open(f"{config_path}/bot.json", "r") as f:
            self.config_dict = json.load(f)
        self._load_config()
        self.config_data: dict[dict[str, Any]] = dict()

        # deployment environment
        self.imds = None
        if self.get_config("aws.running_from_ec2", False):
            self.imds = ec2_metadata
            aws_region = self.imds.region
        else:
            aws_region = os.environ.get("AWS_DEFAULT_REGION", None)

        # temporary file space
        self.tmp_path = "/tmp/cables"
        if not os.path.isdir(self.tmp_path):
            os.mkdir(self.tmp_path)

        # cryptography
        self.secrets = None
        if self.get_config("aws.use_secrets_manager", False):
            self.secrets = SecretsManagerService(region=aws_region).cache
        self.cryptography = CryptographyService(self, self.secrets).controller

        # postgres database client
        self.db = DatabaseService(self).client
        self.config_keys.init_tables()

        # reddit api
        self.reddit = RedditService(self).client

        # hydrus client api
        _endpoint = self.get_config(
            "hydrus.endpoint",
            "http://127.0.0.1:45869/"
        )
        _key = self.get_config("hydrus.api_key", None)
        self.hydrus = None
        if self.get_config("hydrus.enabled", False):
            self.hydrus = HydrusService(_endpoint, _key)

        self.oplog = OpLog(self)

    @property
    def config(self):
        return self.config_dict

    @property
    def config_keys(self):
        return bot_config.ConfigKeys(self.db)

    def get_config(self, config_key, default=None) -> Optional[str]:
        maybe_value = get_with_env_override(config_key, self.config_dict)
        if maybe_value:
            self.config_dict[config_key] = maybe_value
        return maybe_value or default

    def get_guild_key(self, guild: int, module: str, key: str) -> Any:
        try:
            return self.config_data[guild][module][key]
        except KeyError:
            # err = f"Guild config key uninitialized: ({guild}:{module}:{key})"
            # logger.debug(err)
            return None

    def _load_config(self):
        self.command_prefix = self.get_config("command_prefix", ".")

        _owner_ids = self.config_dict["privileged"].get("bot_admins", [])
        if (self.owner_ids and type(self.owner_ids) is not list):
            _fmt = "'owner_ids' must be a collection, not {0.__class__!r}"
            err = _fmt.format(self.owner_ids)
            raise TypeError(err)
        self.owner_ids = [int(o) for o in _owner_ids]

        logger.info("Loaded configuration from file")
        logger.debug(f"Bot admins: {self.owner_ids}")

        # load project keys into context
        self.issues_uri = self.project_config["urls"]["issues"]
        self.description = (
            self.project_config["description"]
            or "Cables is a bot that isn't all that smart yet."
        )
        self.version, self.repository = "", ""
        pyproject_keys = ["version", "repository"]
        [setattr(self, key, self.project_config[key]) for key in pyproject_keys]

        # get local git repo state
        self.git_commit = self.repo.head.commit
        try:
            self.git_ref = self.repo.head.ref
        except TypeError:
            self.git_ref = 'detached'

    async def update_owners(self) -> User:
        app = await self.application_info()
        if app.owner.id not in self.owner_ids:
            self.owner_ids += [app.owner.id]
        return app.owner

    def get_reach_metrics(self):
        return dict(
            users=len(self.users),
            guilds=len(self.guilds),
            groups=len(self.private_channels)
        )

    def get_privileged_role(self, name: str, guild: Guild) -> Optional[Role]:
        if name not in (
            "staff",
            "global_moderator",
            "channel_moderator",
            "admin",
            "admin_proxy"
        ):
            raise ValueError("Not a valid staff role.")

        role_id = self.get_guild_key(
            guild.id,
            "privileged", f"{name}_role"
        )
        if not role_id:
            return None
        return guild.get_role(role_id)

    def load_extensions(self, extensions: list):
        for ext in extensions:
            result = self.load_extension(f"cabling_bot.{ext}", store=True)
            if result[f"cabling_bot.{ext}"] is not True:
                logger.exception(result[f"cabling_bot.{ext}"])
                time.sleep(2.0)
                continue
            else:
                logger.debug(f"Initialized cog '{ext}'")

    def reload(self):
        """Hot reload all bot extensions/commands and configuration"""
        logger.info("Reloading config...")
        self._load_config()
        logger.info(f"Reloading extensions: {self.extensions}")
        for ext in list(self.extensions.keys()):
            logger.debug(f"Reloading extension {ext}...")
            self.reload_extension(ext)
        logger.info("Finished reloading extensions")

    async def get_context(self, message, *, cls=CablesContext):
        """Required for using custom context"""
        return await super().get_context(message, cls=cls)

    async def get_application_context(self, interaction: Interaction, cls=CablesAppContext):
        """Required for using custom context"""
        return await super().get_application_context(interaction, cls)
