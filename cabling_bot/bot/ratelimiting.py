import asyncio
from typing import (Any, Coroutine, Optional)
from discord import Guild, Member
from .errors import report_guild_error


async def congestion_detector(
    bot,
    guild: Guild,
    coro: Coroutine,
    timeout: float = 600,
    callback: Any = None,
    err: Optional[str] = "Interaction",
    member: Optional[Member] = None,
) -> bool:
    _, pending = await asyncio.wait(coro, timeout=timeout)
    if not pending:
        return False
    err = f"{err} failed, probably due to Discord service congestion."
    text = ("Discord service congestion is slowing me down! "
            + "Try taking that role again later, please!")
    await report_guild_error(
        bot, None, member,
        guild, err, text
    )
    return True
