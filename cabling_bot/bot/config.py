import logging
import ring

from os import environ as env
from collections import namedtuple
from cabling_bot.util import transform

from typing import (Any, Optional, Sequence)
from psycopg_pool import ConnectionPool as Db

logger = logging.getLogger(__name__)


def get_from_env(config_key: str) -> Optional[str]:
    """Translate configuration key to environment variable key and fetch it from the environment, if it is defined"""
    env_key = config_key.replace(".", "_").upper()
    return env.get(env_key) or env.get(f"CABLES_{env_key}")


def get_from_dict(config_key: str, config_dict: dict) -> Optional[str]:
    """Fetch configuration key from dict, if it is defined"""
    if config_dict is None:
        return None
    elif "." not in config_key:
        return config_dict.get(config_key, None)
    else:
        split_key = config_key.split(".", maxsplit=1)
        return get_from_dict(split_key[1], config_dict.get(split_key[0], None))


def get_with_env_override(config_key: str, config_dict: dict) -> Optional[str]:
    """Get the configuration key from the environment; if not present, try the configuration dict"""
    return get_from_env(config_key) or get_from_dict(config_key, config_dict)


CONFIG_SCHEMA = """
CREATE TABLE IF NOT EXISTS config_keys (
    "id"            serial  PRIMARY KEY,
    "module"        text    NOT NULL,
    "key"           text    NOT NULL,
    "description"   text    NOT NULL,
    "default"       text    NULL
);
CREATE UNIQUE INDEX IF NOT EXISTS idx_module_key
    ON config_keys ("module", "key");

CREATE TABLE IF NOT EXISTS config_guild (
    "id"            serial    PRIMARY KEY,
    "config_ref"    bigint    REFERENCES config_keys ON DELETE CASCADE,
    "guild"         bigint    NOT NULL,
    "value"         text      NOT NULL
);
CREATE UNIQUE INDEX IF NOT EXISTS idx_configref_guild
    ON config_guild ("config_ref", "guild");

CREATE TABLE IF NOT EXISTS config_member (
    "id"            serial  PRIMARY KEY,
    "config_ref"    bigint  REFERENCES config_keys ON DELETE CASCADE,
    "member"        bigint  NOT NULL,
    "guild"         bigint  NOT NULL,
    "value"         text    NOT NULL
);
CREATE UNIQUE INDEX IF NOT EXISTS idx_configref_member_guild
    ON config_member ("config_ref", "member", "guild");
"""


def scoped_key(module: Optional[str], key: Optional[str]) -> tuple:
    where = ""
    row_values = {}
    if key and not module:
        raise ValueError("The key parameter cannot be specified without module.")
    if module:
        where += ' "module" = %(module)s'
        row_values["module"] = module
    if key:
        where += ' AND "key" = %(key)s'
        row_values["key"] = key
    return (str(where), row_values)


class ConfigKeys:
    def __init__(self, db: Db):
        self._db = db

    def init_tables(self):
        with self._db.connection() as conn:
            conn.execute(CONFIG_SCHEMA)

    def new(self, module: str, key: str, description: str, default: Optional[str]):
        """Create a new configuration key in the database

        :param module: module associated with the configuration key
        :param key: key name
        :param description: helpful description of the configuration key
        :param default: default configuration value
        """
        q = """
        INSERT INTO config_keys ("module", "key", "description", "default")
        VALUES (%(module)s, %(key)s, %(description)s, %(default)s)
        ON CONFLICT ("module", "key")
        DO UPDATE SET "description" = %(description)s, "default" = %(default)s;
        """
        row_values = dict(
            module=module, key=key, description=description, default=default
        )
        with self._db.connection() as conn:
            conn.execute(q, row_values)

    @ring.lru()
    def get(self, module: Optional[str], key: Optional[str]) -> Sequence[tuple]:
        """Get available configuration key(s) defined in the database

        :param module: (optional) limit scope to module
        :param key: (optional) limit scope to key
        """
        where_q, where_dict = scoped_key(module, key)
        q = "SELECT * FROM config_keys "
        if where_q:
            q += "WHERE " + where_q
        q += "ORDER BY module, key ASC;"
        keys = []
        with self._db.connection() as conn:
            keys = conn.execute(q, where_dict).fetchall()
        # logger.debug(f"ConfigKeys.get={keys}")
        return keys

    @get.ring.key
    def _keys_get_cache_key(self, module, key):
        return f"module={module},key={key}"

    def list_modules(self) -> Sequence[tuple]:
        q = "SELECT DISTINCT module FROM config_keys;"
        with self._db.connection() as conn:
            modules = conn.execute(q).fetchall()
        # logger.debug(f"ModuleNames={modules}")
        return modules

    def delete(self, module: str, key: str):
        """Remove a configuration key from the database

        :param module: module associated with the configuration key
        :param key: key name
        """
        q = 'DELETE FROM config_keys WHERE "module" = %(module)s AND "key" = %(key)s'
        with self._db.connection() as conn:
            rows = conn.execute(q, (module, key)).fetchall()
        self.get.delete(module, key)
        return rows


class GuildConfig:
    def __init__(self, db: Db, guild: int):
        self._db = db
        self.guild = guild

    def set(self, module: str, key: str, value: str) -> Sequence[tuple]:
        """Set configuration key at the guild level"""
        q = """
        INSERT INTO config_guild ("config_ref", "guild", "value")
            SELECT config_keys.id, %(guild)s, %(value)s
            FROM config_keys
            WHERE "module" = %(module)s AND "key" = %(key)s
        ON CONFLICT ("config_ref", "guild")
        DO UPDATE SET "value" = %(value)s;
        """
        row_values = dict(module=module, key=key, guild=self.guild, value=value)
        with self._db.connection() as conn:
            cursor = conn.execute(q, row_values)
            rowcount = cursor.rowcount
        if rowcount:
            if self.list.has(None, None):
                self.list.delete(None, None)
            if self.list.has(module, None):
                self.list.delete(module, None)
            if self.get.has(module, key):
                self.get.set(value, module, key)
                self.list.set(value, module, key)
        return bool(rowcount)

    @ring.lru()
    def list(self, module: Optional[str], key: Optional[str]) -> Sequence[tuple]:
        """Get guild configuration key(s), optionally scoped to the module and key"""

        row_values = dict(guild=self.guild)
        q = """
        SELECT "module", "key", "value" FROM config_guild
        JOIN config_keys ON config_guild.config_ref = config_keys.id
        WHERE "guild" = %(guild)s
        """
        where_q, where_dict = scoped_key(module, key)
        if where_q:
            q += "AND" + where_q
            row_values.update(where_dict)
        with self._db.connection() as conn:
            rows = conn.execute(q, row_values).fetchall()
            # logger.debug(f"GuildConfig.list={rows}")

        return rows

    @list.ring.key
    def _guild_list_cache_key(self, module, key):
        return f"guild={self.guild},module='{module}',key='{key}'"

    @ring.lru()
    def get(self, module: str, key: str) -> Any:
        """Get a single guild configuration key."""

        rows = self.list(module, key)
        if len(rows) > 1:
            err = f"Multiple results for '{module}:{key}'. Did you mean list()?"
            raise ValueError(err)

        value = None
        try:
            value = rows[0].value
        except IndexError:
            pass

        if not value:
            try:
                row = ConfigKeys(self._db).get(module, key)[0]
                # logger.debug(f"GuildConfig.list[{module}:{rows[0].key}].default={value}")
                return transform.strtoliteral(row.default)
            except IndexError:
                logger.debug(f"Config key '{module}:{key}' non-existent.")

        return transform.strtoliteral(value)

    @get.ring.key
    def _guild_get_cache_key(self, module, key):
        return f"guild={self.guild},module='{module}',key='{key}'"

    def list_module(self, module: str) -> dict[str, Any]:
        config = dict()
        items = ConfigKeys(self._db).get(module, None)
        for item in items:
            config |= {item.key: self.get(module, item.key)}
        # logger.debug(f"Module={module}, Items={config}")
        return config

    def dump(self) -> dict[str, Any]:
        config = dict()
        modules = ConfigKeys(self._db).list_modules()
        for row in modules:
            module = row.module
            config |= {module: self.list_module(module)}
        return config

    def unset(self, module: str, key: str):
        """Unset configuration key at the guild level"""
        q = """
        DELETE FROM config_guild
        USING config_keys
        WHERE
            config_ref = config_keys.id
            AND module = %(module)s
            AND key = %(key)s
            AND guild = %(guild)s;
        """
        row_values = dict(module=module, key=key, guild=self.guild)
        with self._db.connection() as conn:
            cursor = conn.execute(q, row_values)
            rowcount = cursor.rowcount
        # invalidate cache
        if rowcount:
            if self.list.has(None, None):
                self.list.delete(None, None)
            if self.list.has(module, None):
                self.list.delete(module, None)
            if self.list.has(module, key):
                self.list.delete(module, key)
            if self.get.has(module, key):
                self.get.delete(module, key)
        return bool(rowcount)


class MemberConfig:
    def __init__(self, db: Db, guild: int, member: int):
        self._db = db
        self.guild = guild
        self.member = member

    def set(self, module: str, key: str, value: str):
        """Set configuration for a guild member"""
        q = """
        INSERT INTO config_member ("config_ref", "member", "guild", "value")
        SELECT config_keys.id, %(member)s, %(guild)s, %(value)s
        FROM config_keys WHERE "module" = %(module)s AND "key" = %(key)s
        ON CONFLICT ("config_ref", "member", "guild")
        DO UPDATE SET "value" = %(value)s;
        """
        row_values = dict(
            module=module, key=key, member=self.member, guild=self.guild, value=value
        )
        with self._db.connection() as conn:
            conn.execute(q, row_values).fetchall()

    def get(self, module: Optional[str], key: Optional[str]):
        """Get guild member configuration key(s), optionally scoped to the module and key"""
        where_q, where_dict = scoped_key(module, key)
        row_values = dict(guild=self.guild, member=self.member)
        q = """
        SELECT "module", "key", "value" FROM config_member
        JOIN config_keys ON config_member.config_ref = config_keys.id
        WHERE "guild" = %(guild)s AND "member" = %(member)s
        """
        if where_q:
            q += "AND" + where_q
            row_values.update(where_dict)
        with self._db.connection() as conn:
            rows = conn.execute(q, row_values).fetchall()
        if key and not rows:
            # return default value for key
            default = ConfigKeys(self._db).get(module, key)[0].default
            DefaultValue = namedtuple("DefaultValue", ["module", "key", "value"])
            return [DefaultValue(module, key, default)]
        return rows

    def get_default(self, module: str, key: str, default: str) -> str:
        """Get guild configuration key, using `default` if key is unset"""
        result = self.get(module, key)
        if result:
            return result[0].value
        else:
            return default

    def unset(self, module: str, key: str):
        """Unset member configuration key for a guild member"""
        q = """
        WITH "config_ref" AS (SELECT id FROM config_keys WHERE "module" = %(module)s AND "key" = %(key)s)
        DELETE FROM config_member
        WHERE "config_ref" = config_ref AND "member" = %(member)s AND "guild" = %(guild)s
        """
        row_values = dict(module=module, key=key, member=self.member, guild=self.guild)
        with self._db.connection() as conn:
            rows = conn.execute(q, row_values).fetchall()
        return rows
