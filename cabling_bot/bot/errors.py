import logging
from typing import (Optional, Union)
from discord import Guild, Member
from cabling_bot.util import transform

from cabling_bot.bot import exceptions as CablesException
from discord.errors import Forbidden
from discord.ext.commands.errors import (
    BadColourArgument,
    BotMissingPermissions,
    CommandNotFound,
    MissingPermissions,
    MissingRequiredArgument,
    NoPrivateMessage,
    UnexpectedQuoteError,
    CheckAnyFailure,
    UserNotFound,
    GuildNotFound,
    MemberNotFound,
    RoleNotFound,
)
from cabling_bot.bot import (
    CablesContext as Context,
    CablesAppContext as ApplicationContext,
)
from ..util.discord import get_system_channel

logger = logging.getLogger(__name__)


async def report_guild_error(
    bot,
    ctx: Optional[Union[Context, ApplicationContext]],
    member: Optional[Member],
    guild: Guild,
    report_text: str,
    user_text: Optional[str] = None,
) -> Optional[bool]:
    """Report an error to guild staff as defined by `report_text`.

    Response will be sent to the user under the following conditions:
    - When `ctx` is passed, via the originating channel.
    - When `member` is passed, via DM.

    The default messaging to the user is that the bot is misconfigured. \
        This may be overridden with `user_text`.
    """
    if (ctx and not member and user_text):
        if not user_text:
            user_text = "Bot is misconfigured. Server staff have been notified."
        if type(ctx) is ApplicationContext:
            await ctx.respond(user_text, ephemeral=True)
        elif type(ctx) is Context:
            await ctx.fail_msg(user_text, delete_after=-1)
    elif (not ctx and member and user_text):
        try:
            await member.send(user_text)
        except Forbidden:
            pass

    channel = get_system_channel(bot, guild)
    if not channel:
        channel = guild.system_channel
    bot.loop.create_task(
        channel.send(report_text)
    )


async def report_database_error(
    bot,
    ctx: Optional[Union[Context, ApplicationContext]],
    action: str,
    error: Optional[Exception],
    user_text: Optional[str] = None,
) -> Optional[bool]:
    if ctx:
        if not user_text:
            user_text = ("Bot encountered an internal error."
                         + "Maintainers have been notified.")
        if type(ctx) is ApplicationContext:
            await ctx.respond(user_text, ephemeral=True)
        elif type(ctx) is Context:
            await ctx.fail_msg(user_text, delete_after=-1)

    if bot.debug_channel:
        text = f"Database error when accounting {action} event: {error}"
        bot.loop.create_task(
            bot.debug_channel.send(text, delete_after=-1)
        )


async def is_privileged_invoke(
    ctx: Union[Context, ApplicationContext],
) -> None:
    if ctx.author.id not in ctx.bot.owner_ids:
        return None
    if (
        ctx.guild is not None
        and ctx.guild != ctx.bot.dev_guild
    ):
        cmd = f"{ctx.command}"
        if len(ctx.invoked_parents) > 1:
            subcmds, c = ctx.invoked_parents, 1
            subcmds.pop(0)
            while c < len(subcmds) in subcmds:
                cmd += f" {ctx.invoked_parents[0]}"
                c += 1
        text = f"""\
            Refusing to display privileged details
            for `{cmd}` in *{ctx.guild.name}* ({ctx.channel.mention}).
            For data privacy, such responses are confined to
            the dev guild, and direct message.\
        """
        text = transform.clean_message(text)
        await ctx.author.send(text)
        return False
    elif (
        ctx.guild is None
        or ctx.guild == ctx.bot.dev_guild
    ):
        return True


async def default_error_handler(
    ctx: Union[Context, ApplicationContext],
    e: Exception
):
    reply = "Something made a fucky wucky!\n" + transform.codeblock(e)
    ephemeral = True

    if isinstance(e, CommandNotFound):
        return  # ignore non-commands
    elif isinstance(e, NoPrivateMessage):
        reply = "Sorry, that command isn't available in DMs."
    elif isinstance(e, CablesException.DMsDisallowed):
        reply = """\
            Temporarily change your privacy settings to allow DMs
            from server members, and then try again.
        """
    elif isinstance(e, CablesException.GuildUnavailable):
        # reply = "Guild is unavailable, likely due to an outage."
        return
    elif isinstance(e, MissingPermissions):
        reply = "Sorry, you don't have permission to do that."
    elif isinstance(e, BotMissingPermissions):
        reply = f"Uhhh... I apparently don't have permissions for that: {e}"
    elif isinstance(e, Forbidden):
        reply = f"Sorry, I don't have permission to do `{ctx.command}` with the arguments: {ctx.kwargs}"
    elif isinstance(e, MissingRequiredArgument):
        reply = f"You're missing a required argument `{e.param}`"
    elif isinstance(e, UserNotFound) or isinstance(e, MemberNotFound):
        reply = "The given user doesn't exist, or you otherwise entered invalid command input."
    elif isinstance(e, GuildNotFound):
        reply = "The given guild doesn't exist, or you otherwise entered invalid command input."
    elif isinstance(e, RoleNotFound):
        reply = "The given role doesn't exist, or you otherwise entered invalid command input."
    elif isinstance(e, CablesException.GuildMembershipError):
        reply = "You're not a member of that server or it doesn't exist."
    elif isinstance(e, CablesException.GuildOwnerOnlyError):
        reply = f"Sorry, you must be the guild owner to use the `{ctx.command}` command."
    elif isinstance(e, CablesException.ChanopsOnlyError):
        reply = f"Only Channel Moderators may use the `{ctx.command}` command."
    elif isinstance(e, CablesException.GmodsOnlyError):
        reply = f"Only Global Moderators may use the `{ctx.command}` command."
    elif isinstance(e, CablesException.StaffOnlyError):
        reply = f"Sorry, you must be a staff member to use the `{ctx.command}` command."
    elif isinstance(e, CablesException.MemberRoleInferior):
        reply = 'The proposed role cannot be managed, as it is above your highest role.'
    elif isinstance(e, CablesException.BotRoleInferior):
        reply = 'The proposed role cannot be managed, as it is above _my_ highest role.'
    elif isinstance(e, CheckAnyFailure) and ctx.invoked_parents[0] in ['role', 'r']:
        reply = 'You must be a staff member or have the Manage Roles permission to use this command.'
    elif isinstance(e, CheckAnyFailure) and ctx.invoked_parents[0] == 'watch':
        reply = 'You must be a staff member or have the Administrator permission to use this command.'
    elif isinstance(e, CablesException.RceDisabled):
        reply = ("Remote code execution is currently disabled 😌\n\n"
                 + "**WARNING:** This feature is turbo-cursed!! 🐉\n"
                 + "_Please,_ consult the documentation before using it!")
        ephemeral = False
    elif isinstance(e, CablesException.AuditReasonRequired):
        reply = 'A reason is required by guild policy.'
    elif isinstance(e, CablesException.AuditReasonOverloaded):
        return
    elif isinstance(e, CablesException.DefconConstraintViolation):
        reply = "Guild is in purgatory. "
        reply += "Administrative commanding suspended pending ownership transfer."
    elif isinstance(e, UnexpectedQuoteError):
        reply = """\
            There was an unexpected quotation mark in your command.\n
            It could be because:
            - you forgot to wrap a multi-word parameter in quotation marks
            - your quotation marks aren't matching
            - you've forgotten a required parameter
        """
    elif isinstance(e, BadColourArgument):
        reply = """\
            Couldn't parse the color you gave.\n
            Please specify colors in one of the following formats:
            - `#<hex>`
            - `0x<hex>`
            - `0x#<hex>`
            - `rgb(<number>, <number>, <number>)`
            where `hex` is either a 3 or 6 digit hex number
            and `number` is either 0-255 or 0-100%.
        """
    elif isinstance(e, CablesException.FeatureDisabled):
        reply = "Feature isn't enabled on this server."
    else:
        if type(ctx) is ApplicationContext:
            await ctx.respond(reply, ephemeral=False)
        elif type(ctx) is Context:
            await ctx.fail_msg(reply, delete_after=-1)
        raise e

    if type(ctx) is ApplicationContext:
        await ctx.respond(reply, ephemeral=False)
    elif type(ctx) is Context:
        if not ephemeral:
            await ctx.fail_msg(reply, delete_after=-1)
        else:
            await ctx.fail_msg(reply, delete_after=30)
