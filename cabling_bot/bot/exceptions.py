from discord.ext.commands import CheckFailure


class MemberRoleInferior(CheckFailure):
    pass


class BotRoleInferior(CheckFailure):
    pass


class GmodsOnlyError(CheckFailure):
    pass


class ChanopsOnlyError(CheckFailure):
    pass


class StaffOnlyError(CheckFailure):
    pass


class AuditReasonRequired(CheckFailure):
    pass


class AuditReasonOverloaded(CheckFailure):
    pass


class DefconConstraintViolation(CheckFailure):
    pass


class DMsDisallowed(CheckFailure):
    pass


class GuildMembershipError(CheckFailure):
    pass


class GuildUnavailable(CheckFailure):
    pass


class GuildOwnerOnlyError(CheckFailure):
    pass


class FeatureDisabled(CheckFailure):
    pass


class RceDisabled(CheckFailure):
    pass
