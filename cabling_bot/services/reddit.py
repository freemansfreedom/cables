import logging
import asyncpraw
import asyncpraw.exceptions

logger = logging.getLogger(__name__)


class RedditService:
    def __init__(self, bot):
        params, params_redacted = self._get_config(bot)

        self.client = None
        try:
            self.client = asyncpraw.Reddit(**params)
            logger.info(
                f"Initialized Reddit API module: {params_redacted}"
            )
        except asyncpraw.exceptions.MissingRequiredAttributeException:
            logger.warning(f"Reddit API misconfigured: {params_redacted}")

    def _get_config(self, bot):
        _username = bot.get_config('reddit.username')
        params = {
            "client_id": bot.get_config("reddit.client_id"),
            "client_secret": bot.get_config("reddit.client_secret"),
            "username": _username,
            "password": bot.get_config("reddit.password"),
            "user_agent": f"script:cables:v{bot.version} (by u/{_username})",
        }
        params_redacted = params | {
            "client_secret": "[REDACTED]",
            "password": "[REDACTED]",
        }

        return (params, params_redacted)
