from typing import Optional
import botocore.session
from aws_secretsmanager_caching import (SecretCache, SecretCacheConfig)


class SecretsManagerService:
    def __init__(self, region: Optional[str]) -> None:
        _boto_session = botocore.session.get_session()
        self.client = _boto_session.create_client(
            "secretsmanager",
            region_name=region
        )

        self.config = SecretCacheConfig(secret_refresh_interval=300.0)
        self.cache = SecretCache(config=self.config, client=self.client)
