import asyncio
from typing import (Optional, Union)
from discord import (Reaction, Member, User)
from cabling_bot.bot.constants import Emojis
from cabling_bot.bot import CablesContext as Context


async def quorum_vote(
    ctx: Context,
    topic: str,
    voters: Union[Member, User],
    timeout: Optional[float] = 300.0,
) -> bool:
    """Seeks quorum consensus from a given set of users by reaction voting.

    Default timeout is 5 minutes."""

    vote_text = f"Awaiting quorum consensus {topic}"
    vote_msg = await ctx.message.reply(vote_text, mention_author=False)
    await vote_msg.add_reaction(Emojis.confirm)

    def targeting(r: Reaction, u: User):
        return (r.message.id == vote_msg.id and u in voters)

    approvals = list()
    while not approvals or set(approvals) != set(voters):
        try:
            reaction: Reaction
            reactor: Union[Member, User]

            reaction, reactor = await ctx.bot.wait_for(
                'reaction_add', timeout=timeout, check=targeting
            )

            if reactor == ctx.bot.user:
                continue
            elif (
                reaction.is_custom_emoji()
                or reaction.emoji != Emojis.confirm
            ):
                await reaction.remove(reactor)
            else:
                approvals += [reactor]
        except asyncio.TimeoutError:
            err = f"Quorum was not attained ({len(approvals)}/{len(voters)})."
            if approvals:
                err += "\n\n**Approvals:**\n"
                c = 1
                for m in approvals:
                    err += f"{c}. {m}\n"
                    c += 1

            await ctx.fail_msg(err)
            await vote_msg.delete(delay=5.0)

            return False

    return True
