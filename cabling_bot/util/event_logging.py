# Kudos to Tortoise-Community/Tortoise-BOT for the inspiration!
# https://github.com/Tortoise-Community/Tortoise-BOT/blob/master/bot/utils/invite_help.py

import logging
from discord.utils import format_dt
from cabling_bot.bot import constants
from cabling_bot.util import transform, guild_invites
from cabling_bot.util.discord import get_system_channel
import cabling_bot.util.user
from cabling_bot.util.embeds import EmbedManager

from discord import (
    Guild, TextChannel, Message,
    Member, User, Invite, Colour
)
from discord.abc import PrivateChannel
from typing import (Optional, Union, List)
from cabling_bot.bot import (
    CablesContext as Context,
    CablesBot as Bot
)


logger = logging.getLogger(__name__)


class EventLogController:
    def __init__(self, bot: Bot):
        self._bot = bot
        self.log = EmbedManager(bot)
        self.tracker = guild_invites.GuildInvites(bot.db)

    ####################
    ## HELPER METHODS ##
    ####################

    async def _collect_targets(self, guild: Guild) -> list[TextChannel, PrivateChannel]:
        channels: List[Union[TextChannel, PrivateChannel]] = list()

        # guild owner, if it isn't cables
        if guild.owner != self._bot.user:
            owner_channel = (
                guild.owner.dm_channel
                or await self._bot.create_dm(guild.owner)
            )
            if owner_channel:
                channels.append(owner_channel)

        # guild system channel
        channels.append(guild.system_channel)

        announce_channel_id = self._bot.get_guild_key(
            guild.id,
            "notifications", "public_announce_channel"
        )
        announce_channel = guild.get_channel(announce_channel_id)
        if announce_channel:
            channels.append(announce_channel)
        elif "COMMUNITY" in guild.features:
            # fall back on the first news channel encountered
            for channel in guild.text_channels:
                if not channel.is_news():
                    continue
                channels.append(channel)
                break

        return channels

    async def foghorn(self, guild: Guild, title: str, text: str) -> List[Message]:
        channels = await self._collect_targets(guild)
        for channel in channels:
            _title, mention = title, None
            if isinstance(channel, PrivateChannel):
                _title += f" {guild.name}"
            else:
                mention = "everyone"

            await self.log.send(
                Colour.dark_red(),
                _title, text,
                channel=channel,
                title_icon=constants.Icons.emergency,
                mentionable=mention
            )

    ######################
    ## LISTENER METHODS ##
    ######################

    async def member_join(
        self,
        member: Member,
        route: str,
        invite: Optional[dict] = None,
        priority: Optional[int] = None,
    ) -> None:
        """Log guild member join events."""
        guild = member.guild
        enabled = self._bot.get_guild_key(
            guild.id, "notifications", "guild_joins"
        )

        # case: staff or forcible join
        mention = None
        if priority == 1:
            role = self._bot.get_privileged_role("staff", guild)
            mention = role.mention if role else "here"
            colour, icon = Colour.dark_red(), constants.Icons.emergency
        elif not priority:
            if not enabled:
                return
            colour, icon = Colour.green(), constants.Icons.user_join

        text = f"**Who:** {member.mention} ({member})\n"
        text += f"**How:** {route}"
        if invite:
            inviter = self.tracker._get_inviter(guild, member)
            inviter = transform.cast_to_mention(inviter, guild)
            text += f" ({inviter})" if inviter else f" ({invite['code']})"

        acct_age, age_warning = format_dt(member.created_at, 'R'), str()
        if not cabling_bot.util.user.account_age_check(self._bot, member, guild):
            age_warning += constants.Emojis.warning
        text += f"\n**Account Birth:** {acct_age} {age_warning}\n"

        badges = cabling_bot.util.user.collect_badges(member)
        text += f"**Sash:** {' '.join(badges)}\n" if badges else ""

        if priority == 0:
            await self.foghorn(guild, "User join", text)
            return

        # NOTE: mentions must be in the message content for them to work
        ## https://stackoverflow.com/a/61747552
        await self.log.send(
            colour,
            "User join", text,
            channel=get_system_channel(self._bot, guild),
            title_icon=icon, mentionable=mention,
            thumbnail=member.display_avatar
        )

    async def member_sanction(
        self,
        guild: Guild,
        action: str,
        target: Union[User, Member],
        issuer: Member,
        reason: Optional[str],
    ) -> None:
        """Log guild member kick and ban events."""

        text = f"**Who:** {target.mention} ({target})\n"

        if isinstance(target, Member):
            joined = format_dt(target.joined_at, style="R")
            text += f"**Joined:** {joined}\n"

        inviter = self.tracker._get_inviter(guild, target)
        if inviter:
            inviter = transform.cast_to_mention(inviter, guild)
            text += f"**Inviter:** `{inviter}`\n"

        text += f"**Issuer:** {issuer}"
        if reason:
            reason = reason.replace("\n", "\n> ")
            text += f"\n**Reason:**\n> {reason}"

        mention = None
        if self._bot.get_guild_key(
            guild.id, "moderation", "sanctions_mention_staff"
        ):
            mention = self._bot.get_privileged_role("staff", guild)

        await self.log.send(
            Colour.red(),
            f"User {action}", text,
            channel=get_system_channel(self._bot, guild),
            mentionable=mention,
            title_icon=constants.Icons.user_part,
        )

    async def member_unban(self, guild: Guild, user: User) -> None:
        """Log guild member unban events."""

        await self.log.send(
            Colour.og_blurple(),
            "User unbanned",
            f"{user.mention} ({user})",
            channel=get_system_channel(self._bot, guild),
            title_icon=constants.Icons.user_unban,
        )

    async def invite_create(
        self,
        invite: Invite,
        requestor: Member = None,
        ctx: Context = None,
    ) -> None:
        """Log a guild invitation being creacted."""

        inviter = invite.inviter
        if requestor:
            inviter = requestor
        log = f"Code `{invite.code}` generated "
        action = "by"
        if ctx:
            action = f"[for]({ctx.message.jump_url})"
        log += f"{action} {inviter.mention}"

        await self.log.send(
            constants.Colours.soft_green,
            "Invite created", log,
            channel=get_system_channel(self._bot, invite.guild),
            title_icon=constants.Icons.hash_green
        )

    async def invite_delete(
        self,
        invite: Invite,
        agent: Union[int, str] = None,
        action: str = None,
    ) -> None:
        """Log a guild invitation being revoked or otherwise deleted."""

        if type(agent) is int:
            agent = invite.guild.get_member(agent).mention
        if not action:
            action = "deleted"
        log = f"Code `{invite.code}` {action} "
        if not agent:
            agent = "service"
        log += f"by {agent}"

        await self.log.send(
            constants.Colours.dark_goldenrod,
            f"Invite {action}", log,
            channel=get_system_channel(self._bot, invite.guild),
            title_icon=constants.Icons.hash_red
        )
