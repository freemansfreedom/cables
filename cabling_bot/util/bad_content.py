import logging
from os import path
from typing import (Any, Optional, Union)
import jinja2

from asyncpraw.reddit import Submission
from cabling_bot.util.discord import reply_disapprovingly
from cabling_bot.util.user import can_send_dm
from cabling_bot.util.matching import ListFilter
from cabling_bot.bot import constants
from cabling_bot.util.embeds import EmbedManager

from discord import (File, Message, Embed, Forbidden)
from NHentai.nhentai import Doujin
from cabling_bot.bot import (CablesCog as Cog, CablesBot as Bot)

logger = logging.getLogger(__name__)


class MediaInspector(Cog):
    def __init__(self, bot: Bot):
        self._bot = bot
        self.log = EmbedManager(bot)

    def _craft_report_reason(
        self,
        category: int,
        msg: Message,
        reason: str
    ) -> tuple[str, list]:
        text, fields = str(), list()
        if category == 3:
            text = (
                f"{msg.author.mention} linked content that was removed "
                + "by a moderator of the community that it was posted in.\n"
                + f"> {msg.content}\n"
            )
        elif category in (1, 2):
            text = (
                f"User {msg.author.mention} may have posted "
                + f"forbidden content in {msg.channel.mention}\n"
                + f"> {msg.content}\n"
            )
            fields = [dict(name="Reason", value=reason)]
        else:
            return (None, None)
        return (text, fields)

    async def report(
        self,
        msg: Message,
        media: Union[Embed, str],
        category: int,
        reason: Union[str, list],
    ) -> None:
        guild = msg.guild
        gid = guild.id

        if type(reason) is list:
            reason = ", ".join(reason)
        text, fields = self._craft_report_reason(category, msg, reason)
        if not text:
            return False

        embed, file = None, None
        if type(media) is Embed:
            embed = media
        if type(media) is File:
            file = media

        role = None
        m = self.get_guild_key(gid, "content_scanning", "staff_mentions")
        if m:
            role = self._bot.get_privileged_role("staff", guild)

        await self.log.send(
            constants.Colours.soft_orange,
            "Content report", text,
            guild=msg.guild,
            title_url=msg.jump_url,
            title_icon=constants.Icons.exclaim_red,
            fields=fields,
            mentionable=role,
            additional_embeds=([embed] if embed else None),
            files=([file] if file else None),
        )

        if self.get_guild_key(gid, "content_scanning", "automoderation"):
            # TODO: move this somewhere else
            if category in (1, 2):
                text = ("The linked media potentially violates Discord's "
                        + "Community Guidelines against sexualizing mirors.")
                policy_url = self.get_guild_key(gid, "moderation", "acceptable_content_url")
                self._bot.loop.create_task(
                    self.automod(msg, text, policy_url)
                )
        else:
            msg = await msg.channel.fetch_message(msg.id)
            if msg.embeds:
                await msg.edit(suppress=True)

    async def automod(
        self,
        msg: Message,
        reason: str,
        url: str,
    ) -> None:
        await msg.delete()

        if not await can_send_dm(msg.author, self._bot.user):
            await reply_disapprovingly(msg.channel, msg.author)
            return

        tpl_path = path.abspath(f"{self._bot.app_path}/cabling_bot/templates")
        templates = jinja2.Environment(loader=jinja2.FileSystemLoader(tpl_path))
        template = templates.get_template('automod_user_notification.tpl')
        content = msg.content
        if content:
            content = content.replace('\n', '\n> ')
        text = template.render({
            "guild": msg.guild.name,
            "channel": msg.channel.mention,
            "content_uri": url,
            "message": content,
            "reason": reason,
        })

        try:
            await msg.author.send(text)
        except Forbidden:
            await reply_disapprovingly(msg.channel, msg.author.mention)

    async def reddit(
        self,
        msg: Message,
        submission: Submission
    ) -> tuple:
        bad_type = False
        removal_reason = submission.removed_by_category
        removed = (removal_reason or not submission.author)
        if removed:
            await msg.edit(suppress=True)
            if removal_reason == "deleted":
                bad_type = 4
            elif not submission.author:
                bad_type = 5
            elif removal_reason == "moderator":
                bad_type = 3

        return (bad_type, None)

    async def ehentai(self, msg: Message, metadata: dict) -> tuple:
        # filter tags by prefix to shorten the list
        prefices = ("female:", "male:", "mixed:", "other:")
        tags_filtered = ListFilter(metadata["tags"], prefices).by_prefix()

        # is the work considered bad?
        has_bad_tag = ListFilter(
            metadata["tags"],
            constants.BadTags.ehentai
        ).by_match()

        if has_bad_tag:
            tags_bad_pretty = [
                f"**{x}**"
                if x in constants.BadTags.ehentai else x
                for x in tags_filtered
            ]

        title: str = metadata["title"]
        has_bad_title = ListFilter(
            constants.BadTags.fragments,
            title.lower()
        ).by_match()

        # TODO: figure out how to return a list
        # of _both_ bad cases in an elegant way
        bad_type, bad_reason = None, None
        if has_bad_tag:
            bad_type = 1
            bad_reason = has_bad_tag
        elif has_bad_title:
            bad_type = 2
            bad_reason = has_bad_title

        tags = "\n".join(tags_filtered if not has_bad_tag else tags_bad_pretty)

        return (bad_type, bad_reason, tags)

    async def nhentai(self, msg: Message, doujin: Doujin) -> tuple:
        # build a subset of tags into a list
        tags_filtered = [tag.name for tag in doujin.tags if tag.type == "tag"]

        # is the work considered bad?
        has_bad_tag = ListFilter(
            tags_filtered,
            constants.BadTags.nhentai
        ).by_match()

        if has_bad_tag:
            tags_bad_pretty = [
                f"**{x}**"
                if x in constants.BadTags.nhentai else x
                for x in tags_filtered
            ]

        title: str = doujin.title.pretty
        has_bad_title = ListFilter(
            constants.BadTags.fragments,
            title.lower()
        ).by_match()

        bad_type, bad_reason = None, None
        if has_bad_tag:
            bad_type = 1
            bad_reason = has_bad_tag
        elif has_bad_title:
            bad_type = 2
            bad_reason = has_bad_title

        tags = "\n".join(tags_filtered if not has_bad_tag else tags_bad_pretty)

        return (bad_type, bad_reason, tags, title)

    def hydrus(self, tags: list):
        return ListFilter(tags, constants.BadTags.hydrus).by_match()
