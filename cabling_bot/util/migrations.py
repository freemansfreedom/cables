from discord import Guild


class MigrationManager():
    def __init__(self, bot):
        self._bot = bot
        self._db = bot.db
        self.revs = [
            "0.8.24"
        ]

    def v0_8_24(
        self,
        guild: Guild,
        known: dict,
        code: int,
        invite: dict,
    ) -> tuple:
        failures, successes = [], 0

        if invite["max_uses"] and not known[code]["max_uses"]:
            sql = """
                UPDATE guild_invite_codes
                SET max_uses = %s
                WHERE code = %s
            """
            values = (invite["max_uses"], code)
            with self._db.connection() as conn:
                result = conn.execute(sql, values)
                if not result.rowcount:
                    failures += [f"{guild.id}/{code}/max_uses"]
                else:
                    successes += 1

        if invite["expires_at"] and not known[code]["expires_at"]:
            sql = """
                UPDATE guild_invite_codes
                SET expires_at = %s
                WHERE code = %s
            """
            values = (invite["expires_at"], code)
            with self._db.connection() as conn:
                result = conn.execute(sql, values)
                if not result.rowcount:
                    failures += [f"{guild.id}/{code}/expires_at"]
                else:
                    successes += 1

        return (failures, successes)
