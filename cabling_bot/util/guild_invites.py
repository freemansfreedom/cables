# Kudos to Tortoise-Community/Tortoise-BOT for the inspiration!
# https://github.com/Tortoise-Community/Tortoise-BOT/blob/master/bot/utils/invite_help.py

import logging
import psycopg_pool
import asyncio

from datetime import (datetime, timezone, timedelta)
from typing import (NamedTuple, Optional, Sequence, Union)
from discord import (Guild, Invite, Member)
from discord.utils import utcnow

logger = logging.getLogger(__name__)


class GuildInvites:
    def __init__(self, db: psycopg_pool.ConnectionPool):
        self.db = db
        q = """
            -- Guild invitations
            CREATE TABLE IF NOT EXISTS guild_invite_codes (
                id              serial      PRIMARY KEY,
                guild           bigint      NOT NULL,
                code            text        NOT NULL,
                uses            bigint      NOT NULL DEFAULT 0,
                max_uses        int         NULL,
                created_by      bigint      NOT NULL,
                created_at      timestamp   NOT NULL DEFAULT CURRENT_TIMESTAMP,
                expires_at      timestamp   NULL,
                revoked         bool        NOT NULL DEFAULT false,
                revoked_by      bigint      NULL,
                revoked_at      timestamp   NULL,
                requested_by    bigint      NULL
            );
            CREATE UNIQUE INDEX IF NOT EXISTS idx_guildinvcodes_code
                ON guild_invite_codes (code);
            CREATE UNIQUE INDEX IF NOT EXISTS idx_guildinvcodes_guildcode
                ON guild_invite_codes (guild, code);
            CREATE INDEX IF NOT EXISTS idx_guildinvcodes_guildrevoked
                ON guild_invite_codes (guild, revoked);

            -- Member joins
            CREATE TABLE IF NOT EXISTS guild_member_joins (
                id              serial      PRIMARY KEY,
                guild           bigint      NOT NULL,
                "user"          bigint      NOT NULL,
                joined_at       timestamp   NOT NULL DEFAULT CURRENT_TIMESTAMP
            );
            CREATE INDEX IF NOT EXISTS idx_guildjoins_user
                ON guild_member_joins ("user");
            CREATE INDEX IF NOT EXISTS idx_guildjoins_joinedat
                ON guild_member_joins ("user", joined_at);

            -- Individual joins by invitation
            CREATE TABLE IF NOT EXISTS guild_invite_uses (
                id              serial      PRIMARY KEY,
                invite          bigint      REFERENCES guild_invite_codes ON DELETE CASCADE,
                "join"          bigint      REFERENCES guild_member_joins ON DELETE CASCADE
            );
            CREATE INDEX IF NOT EXISTS idx_guildinvuses_invite
                ON guild_invite_uses (invite);
            CREATE INDEX IF NOT EXISTS idx_guildinvuses_join
                ON guild_invite_uses ("join");
        """
        with self.db.connection() as conn:
            conn.execute(q)

    ####################
    ## HELPER METHODS ##
    ####################

    def _transform_to_dict(self, invites: list[Invite]) -> dict:
        """Converts the list results of a `Guild.invites()`
        call into a `dict` containing only a subset of
        metadata about each invite."""

        d = {}
        for invite in invites:
            if not invite.revoked:
                revoked = False
            d |= {
                f"{invite.code}": {
                    "created_by": invite.inviter.id,
                    "created_at": invite.created_at,
                    "uses": invite.uses,
                    "max_uses": invite.max_uses,
                    "expires_at": invite.expires_at,
                    "revoked": revoked,
                }
            }
        return d

    # TODO: consolidate into list_invites() ?
    # only difference is they return different fields...
    async def _list_known_invites(
        self,
        guild: Guild,
        only_active: Optional[bool] = True
    ) -> Optional[dict]:
        """Returns a list of guild-specific invite metadata."""

        sql = """
            SELECT
                code, created_by,
                created_at, uses,
                max_uses, expires_at,
                revoked
            FROM guild_invite_codes
            WHERE
                guild = %(guild)s
                &&ADDL_CLAUSE&&;
        """
        addl_criteria = "AND NOT revoked" if only_active else ""
        sql = sql.replace("&&ADDL_CLAUSE&&", addl_criteria)
        with self.db.connection() as conn:
            cursor = conn.execute(sql, dict(guild=guild.id))
            invites = cursor.fetchall()

            d = {}
            for invite in invites:
                (code, created_by, created_at, uses,
                    max_uses, expires_at, revoked) = invite

                created_at: datetime = created_at.replace(tzinfo=timezone.utc)

                d |= {
                    f"{code}": {
                        "created_by": created_by,
                        "created_at": created_at,
                        "uses": uses,
                        "max_uses": max_uses,
                        "expires_at": expires_at,
                        "revoked": revoked,
                    }
                }
            return d

    def _exists(
        self,
        code: str,
        guild: Optional[Guild] = False,
    ) -> Union[list, bool]:
        """Checks the existence of an invite code for
        a particular guild and returns a boolean."""

        if guild:
            sql = """
                SELECT guild, code
                FROM guild_invite_codes
                WHERE guild = %s AND code = %s;
            """
            values = (guild.id, code)
        else:
            sql = """
                SELECT guild, code
                FROM guild_invite_codes
                WHERE code = %(code)s;
            """
            values = dict(code=code)
        with self.db.connection() as conn:
            cursor = conn.execute(sql, values)
            if cursor.rowcount > 1:
                return cursor.fetchall()
            return True

    def _get_inviter(self, guild: Guild, member: Member) -> int:
        """Return a given guild member's inviter."""

        sql = """
            SELECT codes.created_by, codes.requested_by
            FROM guild_invite_codes AS codes
            JOIN guild_member_joins AS joins ON
                codes.guild = joins.guild
            JOIN guild_invite_uses AS uses ON
                uses."join" = joins.id
                AND joins."user" = %s
            WHERE
                codes.guild = %s;
        """
        values = (guild.id, member.id)
        with self.db.connection() as conn:
            cursor = conn.execute(sql, values)
            if not cursor.rowcount:
                return None
            row = cursor.fetchone()
            if row[1]:
                return row[1]
            return row[0]

    # async def _revoke_all_invites(
    #     self,
    #     guild: Guild,
    #     member: Member
    # ) -> bool:
    #     sql = """
    #         UPDATE guild_invite_codes
    #         SET
    #             revoked = true,
    #             revoked_by = %s,
    #             revoked_at = %s
    #         WHERE
    #             guild = %s
    #             AND NOT revoked;
    #     """
    #     values = (guild.id, member.id)
    #     with self.db.connection() as conn:
    #         result = conn.execute(sql, values)
    #         if not result.rowcount:
    #             return False
    #         return True

    #####################
    ## COMMAND METHODS ##
    #####################

    async def list_invites(
        self,
        guild: Guild,
        only_active: Optional[bool] = True,
        only_used: Optional[bool] = False,
        sort: Optional[str] = "created_at",
        page: Optional[int] = 1
    ) -> Optional[Sequence[NamedTuple]]:
        """Return guild-specific invite metadata."""

        offset = ((page * 10) - 10)
        sql = """
            SELECT code, created_by, requested_by, uses
            FROM guild_invite_codes
            WHERE
                guild = %s
                &&REVOKED_CLAUSE&&
                &&USED_CLAUSE&&
            ORDER BY &&SORT_FIELD&& DESC
            LIMIT 10 OFFSET %s;
        """

        sql = sql.replace(
            "&&REVOKED_CLAUSE&&",
            "AND NOT revoked" if only_active else ""
        )
        sql = sql.replace(
            "&&USED_CLAUSE&&",
            "AND uses > 0" if only_used else ""
        )
        if sort not in ("uses", "created_at"):
            sort = "created_at"
        sql = sql.replace(
            "&&SORT_FIELD&&",
            sort
        )

        values = (guild.id, offset)
        with self.db.connection() as conn:
            cursor = conn.execute(sql, values)
            return cursor.fetchall()

    def get_invite_detail(
        self, guild: int, code: str
    ) -> Optional[NamedTuple]:
        """Return a known invite's details from our database."""

        sql = """
            SELECT
                uses, max_uses,
                created_by, requested_by, created_at,
                revoked_by, revoked_at
            FROM guild_invite_codes
            WHERE guild = %s AND code = %s;
        """
        values = (guild, code)
        with self.db.connection() as conn:
            cursor = conn.execute(sql, values)
            if cursor.rowcount:
                return cursor.fetchone()
            else:
                return None

    async def count_invites(
        self,
        guild: Guild,
    ) -> Optional[int]:
        """Return query results for the total number of guild-specific invites that have been used at least once."""

        sql = """
            SELECT COUNT(code) AS invites
            FROM guild_invite_codes
            WHERE
                guild = %(guild)s
                AND uses > 0
            GROUP BY guild;
        """
        values = dict(guild=guild.id)
        with self.db.connection() as conn:
            cursor = conn.execute(sql, values)
            if not cursor.rowcount:
                return None

            result = cursor.fetchone()
            return result[0]

    async def get_invite_uses(
        self,
        guild: Guild,
        unique: Optional[bool] = True
    ) -> Optional[int]:
        """Returns the aggregate number of guild-specific
        member joins by invitation."""

        if unique:
            sql = """
                SELECT COUNT(tmp.unique_user)
                FROM (
                    SELECT COUNT(joins."user") AS unique_user
                    FROM guild_invite_codes AS codes
                    JOIN guild_invite_uses AS uses ON codes.id = uses.invite
                    JOIN guild_member_joins AS joins on uses."join" = joins.id
                    WHERE codes.guild = %(guild)s
                    GROUP BY joins."user"
                ) AS tmp;
            """
        else:
            sql = """
                SELECT SUM(uses) AS uses
                FROM guild_invite_codes
                WHERE
                    guild = %(guild)s
                    AND uses > 0;
            """
        values = dict(guild=guild.id)
        with self.db.connection() as conn:
            cursor = conn.execute(sql, values)
            if not cursor.rowcount:
                return None

            result = cursor.fetchone()
            return result[0]

    def list_member_invites(
        self,
        guild: Guild,
        member: Member
    ) -> Optional[Sequence[NamedTuple]]:
        """Returns the number of active invites generated by a member."""

        sql = """
            SELECT code
            FROM guild_invite_codes
            WHERE
                guild = %s
                AND NOT revoked
                AND (
                    created_by = %s
                    OR requested_by = %s
                );
        """
        values = (guild.id, member.id, member.id)
        with self.db.connection() as conn:
            cursor = conn.execute(sql, values)
            if not cursor.rowcount:
                return None
            return cursor.fetchall()

    def set_invite_requestor(
        self,
        invite: Invite,
        requestor: Member
    ) -> None:
        """Update the log of an invite with the member who requested it."""

        sql = """
            UPDATE guild_invite_codes
            SET requested_by = %s
            WHERE guild = %s AND code = %s;
        """
        values = (requestor.id, invite.guild.id, invite.code)
        with self.db.connection() as conn:
            conn.execute(sql, values)

    ######################
    ## LISTENER METHODS ##
    ######################

    def add_new_invite(
        self,
        invite: Invite
    ) -> None:
        """Log a guild invite creation event in the database."""

        sql = """
            INSERT INTO guild_invite_codes
                (guild, code, max_uses, created_by, created_at, expires_at)
            VALUES
                (%s, %s, %s, %s, %s, %s);
        """
        values = (
            invite.guild.id, invite.code, invite.max_uses,
            invite.inviter.id, invite.created_at, invite.expires_at
        )
        with self.db.connection() as conn:
            conn.execute(sql, values)

    async def revoke_invite(
        self,
        invite: Invite,
        who: int,
        timestamp: datetime,
    ) -> None:
        """Mark a specific invite as revoked in the database."""

        # NOTE: as of 2022-04, its possible that guild won't be returned
        # https://docs.pycord.dev/en/v2.0.0-beta.1/api.html#discord.on_invite_delete
        if isinstance(self._exists(invite.code), list):
            return False

        sql = """
            UPDATE guild_invite_codes
            SET
                revoked = true,
                revoked_by = %s,
                revoked_at = %s
            WHERE code = %s;
        """
        values = (who, timestamp, invite.code)
        with self.db.connection() as conn:
            result = conn.execute(sql, values)
            if not result:
                logger.error(f"Failed to revoke invite; code={invite.code}")

    async def store_join(self, member: Member) -> None:
        """Store the join time of the user."""

        sql = """
            INSERT INTO guild_member_joins
                (guild, "user", joined_at)
            VALUES
                (%s, %s, %s);
        """
        values = (member.guild.id, member.id, member.joined_at)
        with self.db.connection() as conn:
            conn.execute(sql, values)

    def was_single_use_invite(self, member: Member) -> Union[dict, bool]:
        sql = """
            SELECT
                code, created_by, requested_by,
                max_uses, revoked_at
            FROM guild_invite_codes
            WHERE
                guild = %s
                AND revoked_at >= %s
            ORDER BY revoked_at DESC
            LIMIT 1;
        """
        values = (member.guild.id, utcnow() - timedelta(seconds=1))
        with self.db.connection() as conn:
            cursor = conn.execute(sql, values)
            if cursor.rowcount:
                row = cursor.fetchone()
            else:
                return False

        revoked_at: datetime
        code, created_by, requested_by, max_uses, revoked_at = row
        if not revoked_at or max_uses != 1:
            return False

        revoked_at = revoked_at.replace(tzinfo=timezone.utc)
        diff: timedelta = abs(member.joined_at - revoked_at)
        logger.debug(f"was_single_use_invite: {diff} diff btwn join/revoke")
        if diff.seconds < 5:
            return {
                "code": code,
                "created_by": requested_by or created_by,
                "uses": 1
            }

    async def track_join(self, member: Member) -> Optional[dict]:
        """Attempt to correlate a member join with a specific invitation code."""

        ## SINGLE USE CASE
        ####################

        # NOTE: retries were implemented due to WS event delivery
        #   not being reliably ordered. without them, correlation
        #   will intermittently fail since the DB won't know
        #   that a single-use invite is revoked.
        attempt, maybe_invite = 0, None
        while attempt < 2:
            attempt += 1
            maybe_invite = self.was_single_use_invite(member)
            if not maybe_invite:
                await asyncio.sleep(0.5)
                continue
            else:
                break

        if not maybe_invite:
            debug = ("track_join: Invite tracking breakdown; "
                     + "race, app-mediated join or forced entry"
                     + f"for {member.id} on {member.guild.id}")
            logger.debug(debug)
        else:
            return maybe_invite

        ## MULTI-USE CASE
        ###################

        guild = member.guild
        response = await guild.invites()
        remote = self._transform_to_dict(response)
        known = await self._list_known_invites(guild)

        # debugging
        code_diffs = set(remote.keys()) - set(known.keys())
        if code_diffs:
            logger.error(f"track_join: Invite codes out of sync ({guild.id})")

        for code in remote.keys():
            if remote[code]["uses"] > known[code]["uses"]:
                return {
                    "code": code,
                    "created_by": known[code]["created_by"],
                    "uses": remote[code]["uses"]
                }

        return None

    async def track_invite_use(self, member: Member, invite: dict) -> None:
        """Log a member join event in the database."""

        # TODO: check a guild-level toggle before logging this metadata
        sql = "SELECT id FROM guild_invite_codes WHERE code = %(code)s;"
        with self.db.connection() as conn:
            cursor = conn.execute(sql, dict(code=invite["code"]))
            row = cursor.fetchone()
            invite_id = row[0]

        sql = """
            INSERT INTO guild_invite_uses
                (invite, "join")
            SELECT
                %s, id
            FROM guild_member_joins
            WHERE
                guild = %s
                AND "user" = %s
            ORDER BY id DESC
            LIMIT 1;
        """
        values = (invite_id, member.guild.id, member.id)
        with self.db.connection() as conn:
            conn.execute(sql, values)

        sql = "UPDATE guild_invite_codes SET uses = %s WHERE id = %s;"
        values = (invite["uses"], invite_id)
        with self.db.connection() as conn:
            conn.execute(sql, values)
