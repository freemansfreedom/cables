from __future__ import annotations
from typing import TYPE_CHECKING

import logging
from cabling_bot.util.user import fetch_member
from cabling_bot.bot.errors import report_guild_error

from discord import (Member, Forbidden)

if TYPE_CHECKING:
    from typing import Optional
    from discord import (Guild, TextChannel, Role)
    from cabling_bot.bot import CablesContext as Context

logger = logging.getLogger(__name__)


def list_administrators(ctx: Context, guild: Guild) -> Optional[list[Member]]:
    """Lists guild administrators.

    Administrators are users meeting any of the following criteria:
    - Have 'Administrator' or 'Ban Memmbers' permissions
    - Have the role defined by the guild-level `privileged.admin_role` config key
    - Have the role defined by the guild-level `privileged.admin_proxy_role` config key
    - Aren't a Discord-integrated bot
    """
    admin_role = ctx.bot.get_privileged_role("admin", guild)
    admin_proxy_role = ctx.bot.get_privileged_role("admin_proxy", guild)
    return [m for m in guild.members if (
        not m.bot
        and (
            m.guild_permissions.administrator
            or m.guild_permissions.ban_members
            or (admin_role and admin_role in m.roles)
            or (admin_proxy_role and admin_proxy_role in m.roles)
        )
    )]


def list_global_moderators(ctx: Context, guild: Guild) -> Optional[list[Member]]:
    """Lists global moderators for the guild.

    Global moderators are users meeting any of the following criteria:
    - Have the 'Manager Roles' permission
    - Have the guild-level 'Manage Channels' permission
    - Have the guild-level 'Moderate Members' permission
    - Have the guild-level 'Manage Messages' permission
    - Aren't a Discord-integrated bot
    - Aren't a guild Administrator (as defined by `list_administrators()`)
    """
    mod_role = ctx.bot.get_privileged_role("global_moderator", guild)
    admin_role = ctx.bot.get_privileged_role("admin", guild)
    admin_proxy_role = ctx.bot.get_privileged_role("admin_proxy", guild)

    global_mods = list()
    if mod_role:
        global_mods = mod_role.members
    global_mods += [
        m for m in guild.members if (
            m.guild_permissions.manage_roles
            or m.guild_permissions.manage_channels
            or m.guild_permissions.manage_permissions
            or m.guild_permissions.moderate_members
            or m.guild_permissions.manage_messages
        ) and not (
            m in global_mods
            or m.bot
            or m.guild_permissions.administrator
            or (admin_role and m in admin_role.members)
            or (admin_proxy_role and m in admin_proxy_role.members)
        )
    ]

    return global_mods


def list_channel_moderators(ctx: Context, guild: Guild) -> Optional[list[Member]]:
    """Lists channel-level moderators.

    Channel moderators aren't global moderators. They gain their privileged \
    through permission overrides on an individual channel scope.

    Criteria checked for each member in a channel override is as follows:
    - Has the 'Manage Channels' permission
    - Has the 'Moderate Members' permission
    - Has the 'Manage Messages' permission
    - Isn't a global moderator (as defined by `list_global_mods()`)
    - Isn't a guild administrator (as defined by `list_administrators()`)
    - Isn't a Discord-integrated bot
    """
    admin_role = ctx.bot.get_privileged_role("admin", guild)
    admin_proxy_role = ctx.bot.get_privileged_role("admin_proxy", guild)
    global_mods = list_global_moderators(ctx, guild)

    channels = [c for c in guild.channels]
    channel_mods = list()
    for c in channels:
        channel_mods += [
            obj for obj, overwrite in c.overwrites.items()
            if (
                type(obj) is Member
                and obj not in channel_mods
                and (
                    overwrite.pair()[0].manage_channels
                    or overwrite.pair()[0].manage_permissions
                    or overwrite.pair()[0].manage_messages
                ) and not (
                    (global_mods and obj in global_mods)
                    or obj.guild_permissions.administrator
                    or (admin_role and obj in admin_role.members)
                    or (admin_proxy_role and obj in admin_proxy_role.members)
                    or obj.bot
                )
            )
        ]
    return channel_mods


async def is_channel_moderator(
    ctx: Context,
    is_global_mod: bool = False,
    channel: Optional[TextChannel] = None,
    member: Optional[Member] = None,
) -> bool:
    """Returns whether or not a user is a moderator for the channel.

    When `member` nor `channel` is passed, the `Context` is used: \
    `ctx.author` and `ctx.channel`, respectively.

    The `is_global_mod` arg includes Global Moderators in the test.\
    The default behavior is to only return affirmatively for moderators\
    at the local channel scope. See criteria defined by \
    `list_global_moderators()` and `list_channel_moderators()`.
    """

    if not channel and not member:
        channel, member = ctx.channel, ctx.author

    guild = channel.guild
    if not await fetch_member(guild, member):
        return None

    global_mods = list_global_moderators(ctx, guild) if is_global_mod else list()
    admin_role = ctx.bot.get_privileged_role("admin", guild)
    admin_proxy_role = ctx.bot.get_privileged_role("admin_proxy", guild)

    if not (
        obj for obj, overwrite in channel.overwrites.items()
        if (
            type(obj) is Member
            and obj == member
            and (
                (is_global_mod and global_mods and obj in global_mods)
                or overwrite.pair()[0].manage_channels
                or overwrite.pair()[0].manage_permissions
                or overwrite.pair()[0].manage_messages
            ) and not (
                obj.guild_permissions.administrator
                or (admin_role and obj in admin_role.members)
                or (admin_proxy_role and obj in admin_proxy_role.members)
                or obj.bot
            )
        )
    ):
        return False
    return True


def is_staff_member(bot, target: Member, guild: Guild) -> bool:
    if not guild:
        logger.debug("is_staff_member: invoked on non-guild target")
        return None
    role: Role = bot.get_privileged_role("staff", guild)
    if not role:
        return None
    if target not in role.members:
        return False
    return True


async def remove_staff_roles(
    ctx: Context,
    target: Member,
    reason: Optional[str] = None
) -> bool:
    """Remove all staff-related roles from a guild member.

    Does nothing if the target isn't on the guild.
    """
    guild = ctx.guild
    if type(target) is not Member:
        return None

    staff_roles = [
        "staff",
        "channel_moderator", "global_moderator",
        "admin", "admin_proxy"
    ]
    roles: list[Role] = list()
    for name in staff_roles:
        role = ctx.bot.get_privileged_role(name, guild)
        if role:
            roles += [role]

    failures = 0
    for r in roles:
        if r not in target.roles:
            continue
        try:
            await target.remove_roles(r, reason=reason)
        except Forbidden:
            text = (
                f"Tried to remove '{r.name}' role from {target.mention}, but "
                + "it is above the bot's in the role heirarchy."
            )
            await report_guild_error(ctx.bot, ctx, None, guild, text)
            failures += 1

    return bool(not failures)
