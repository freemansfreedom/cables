from __future__ import annotations
from typing import TYPE_CHECKING

import logging
import jinja2
from os import path
from cabling_bot.util import interactions
from cabling_bot.util.user import can_send_dm

from discord import Member, Forbidden, NotFound

if TYPE_CHECKING:
    from typing import (Union, Optional)
    from discord import User
    from cabling_bot.bot import CablesContext as Context


logger = logging.getLogger(__name__)


async def check_safeties(
    ctx: Context,
    user: Union[User, Member],
    issuer: Union[Member, str],
    admins: list[Member],
) -> bool:
    gid = ctx.guild.id
    if issuer == user:
        await ctx.fail_msg("No self-harm! 😰")
        return False
    elif user == ctx.bot.user:
        await ctx.fail_msg("If you want me to leave, you can just say so! 😭")
        return False
    elif (
        ctx.bot.get_guild_key(gid, "defcon", "bot_protection")
        and user.bot
    ):
        err = "Bots are protected users by guild policy."
        await ctx.fail_msg(err)
        return False
    elif (
        issuer != ctx.guild.owner
        and user in admins
    ):
        await ctx.fail_msg("No in-fighting! 😰")
        return False
    return True


async def process_vote(
    ctx: Context,
    action: str,
    target: Union[User, Member],
    requestor: Member,
    voters: list[Member],
) -> bool:
    """Seeks quorum consensus from administrators by reaction voting.

    Short-circuits to `True` if the guild has `moderation.sanction_approval_quorum` disabled."""

    voting_enabled = (
        ctx.bot.get_guild_key(
            ctx.guild.id,
            "moderation",
            "sanction_approval_quorum"
        )
    )

    if requestor == ctx.guild.owner and target in voters:
        return None

    if not voting_enabled or len(voters) <= 1:
        return None

    if not await interactions.quorum_vote(
        ctx, f"to {action} {target}", voters
    ):
        return False

    return True


def _build_template(
    ctx: Context,
    user: Union[User, Member],
    issuer: Union[Member, str],
    reason: Optional[str] = None,
) -> str:
    """Crafts the DM notification message from template."""

    guild, gid = ctx.guild, ctx.guild.id
    uri = ctx.bot.get_guild_key(gid, "moderation", "guidelines_url")
    can_appeal = ctx.bot.get_guild_key(gid, "moderation", "ban_appeals")

    try:
        staff_role = ctx.bot.get_privileged_role("staff", guild)
        staff_members = staff_role.members
        if user in staff_role.members:
            staff_members.remove(user)
        staff_list = [f"{m.mention} ({m})" for m in staff_members]
        staff_list = "\n".join(map(lambda m: f"- {m}", staff_list))
    except AttributeError:
        staff_members, staff_list = list(), str()

    tpl_path = path.abspath(f"{ctx.bot.app_path}/cabling_bot/templates")
    templates = jinja2.Environment(loader=jinja2.FileSystemLoader(tpl_path))
    template = templates.get_template('ban_user_notification.tpl')
    return template.render({
        "guild": guild.name,
        "issuer": issuer.mention if type(issuer) is Member else issuer,
        "reason": reason.replace('\n', '\n> ') if reason else "",
        "policy_uri": uri,
        "can_appeal": can_appeal,
        "show_staff": False if len(staff_members) <= 1 else True,
        "staff_list": staff_list
    })


# TODO: generalize this and move it to bot context
async def notify_user(
    ctx: Context,
    user: Union[User, Member],
    issuer: Union[Member, str],
    reason: Optional[str] = None,
) -> None:
    """Attempts to notify a sanctioned user via DM."""

    guild, gid = ctx.guild, ctx.guild.id

    notify = ctx.bot.get_guild_key(gid, "moderation", "dm_sanctionee")
    try:
        is_member = await guild.fetch_member(user.id)
    except NotFound:
        is_member = False
    if not (is_member and notify):
        return False

    anon = ctx.bot.get_guild_key(gid, "moderation", "anonymous_sanctions")
    issuer = issuer if not anon else None

    text = _build_template(ctx, user, issuer, reason)

    if await can_send_dm(user, ctx.bot.user):
        try:
            await user.send(text)
            return True
        except Forbidden:
            sendable = False
    if not sendable:
        fail_text = f"A notification was unable to be provided to {user}. "
        fail_text += "This is likely due to their client's privacy settings."
        await ctx.fail_msg(
            fail_text,
            channel=ctx.channel, reply=False,
            delete_after=-1
        )
        return False
