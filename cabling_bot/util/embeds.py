import logging
from copy import copy
from cabling_bot.util import transform

from typing import (Union, Optional, List, Dict)
from cabling_bot.bot import (
    CablesContext as Context,
    CablesBot as Bot
)
from discord import (
    Guild, Member, Role, DMChannel, TextChannel,
    File, Embed, Asset, Colour, Forbidden, Thread, VoiceChannel,
)
from discord.abc import PrivateChannel

logger = logging.getLogger(__name__)


def field_constructor(data):
    if not data:
        return False

    parts = list()
    for k, v in data.items():
        if not v:
            continue

        # handle default values
        # (non-plural keys and string or int values)
        key, value = k, v

        # handle value lists
        # (pluralize keys and stringify value list)
        endings = ("er", "ist", "oup")
        if isinstance(v, list):
            if len(v) > 1:
                if k.endswith("y"):
                    key = k[:-1] + "ies"
                elif k.endswith(endings):
                    key = k + "s"

                value = ", ".join(value)
            else:
                value = v[0]

        parts.append(f"**{key}:** {value}")

    return "\n".join(parts)


def split_embeds(embeds: List[Embed]) -> List[List[Embed]]:
    """Split embeds into multiple posts in order to meet embed limits

    The sum total length of all embeds attached to a message must not exceed
    6000 characters. Up to 10 embeds can be attached to a single message.
    https://discord.com/developers/docs/resources/channel#embed-limits-limits
    """
    msgs = []
    current_msg = []
    length = 0
    for embed in embeds:
        if length + len(embed) < 6000 and len(current_msg) < 10:
            # append to current message
            current_msg.append(embed)
            length += len(embed)
        else:
            # start counter for a new message
            msgs.append(copy(current_msg))
            current_msg = [embed]
            length = len(embed)
    msgs.append(current_msg)
    return msgs


class EmbedManager:
    def __init__(self, bot: Bot) -> None:
        self._bot = bot

    def _get_report_channel(
        self, guild: Guild
    ) -> Union[TextChannel, VoiceChannel, Thread]:
        gid = guild.id
        channel = self._bot.get_guild_key(gid, "privileged", "mod_channel")
        if type(channel) == int:
            channel = guild.get_channel(channel)
        if not channel:
            channel = guild.system_channel
        return channel

    def _determine_channel(
        self,
        guild: Optional[Guild] = None,
        channel: Optional[Union[TextChannel, PrivateChannel, int]] = None,
    ):
        if isinstance(channel, int):
            channel = self._bot.get_channel(channel)
        if guild and not channel:
            channel = self._get_report_channel(guild)
        if (
            channel
            and type(channel) is not DMChannel
            and not channel.can_send()
        ):
            err = f"Missing view and/or send access to channel ({channel})"
            raise ValueError(err)
        return channel

    # Kudos to the python-discord/bot project for this
    # https://github.com/python-discord/bot/blob/f4a3de4cb399aa0c51ed6b8df6fbc45f27f38613/bot/exts/moderation/modlog.py#L83-L138
    async def send(
        self,
        colour: Optional[Union[Colour, int]],
        title: Optional[str],
        description: Optional[str],
        guild: Optional[Guild] = None,
        channel: Optional[Union[TextChannel, PrivateChannel, int]] = None,
        title_url: Optional[str] = None,
        title_icon: Optional[str] = None,
        thumbnail: Optional[Union[str, Asset]] = None,
        mentionable: Optional[Union[Role, Member, int, str]] = False,
        files: Optional[List[File]] = None,
        content: Optional[str] = None,
        additional_embeds: Optional[List[Embed]] = None,
        footer: Optional[str] = None,
        footer_icon: Optional[str] = None,
        fields: Optional[List[Dict]] = None,
    ) -> Context:
        """Log an event to a given channel in a pretty embed."""

        # this happens first as it runs checks
        channel = self._determine_channel(guild, channel)
        if not channel:
            err = f"send: Invalid target (guild={guild}, channel={channel})"
            raise ValueError(err)

        description = transform.truncate_text(description)
        embed = Embed(description=description)

        if title:
            if not title_icon:
                title_icon = Embed.Empty
            if not title_url:
                title_url = Embed.Empty
            embed.set_author(
                name=title,
                icon_url=title_icon,
                url=title_url
            )

        if colour:
            embed.colour = colour

        if footer:
            if not footer_icon:
                footer_icon = Embed.Empty
            embed.set_footer(text=footer, icon_url=footer_icon)

        if thumbnail:
            embed.set_thumbnail(url=thumbnail)

        if fields:
            for f in fields:
                embed.add_field(**f)

        if content:
            content = transform.truncate_text(content, limit=2000)

        text = None
        if mentionable:
            text = transform.cast_to_mention(mentionable, channel.guild)
            if content:
                text += f" {content}"

        posts = list()

        posts += [await channel.send(
            content=text,
            embed=embed,
            files=files
        )]

        if additional_embeds:
            if not isinstance(additional_embeds, list):
                err = ("send: Pass 'additional_embeds' as a list"
                       + "even if it's only a single Embed")
                logger.debug(err)
            for embeds in split_embeds(additional_embeds):
                posts += [await channel.send(embeds=embeds)]

        return posts
