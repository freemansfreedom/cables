
from typing import Optional, Union
from discord import DMChannel, Forbidden, NotFound, TextChannel, Thread, VoiceChannel
from cabling_bot.bot.types import MessageableChannel
from cabling_bot.bot import CablesContext as Context
from cabling_bot.bot import errors
from cabling_bot.util.user import can_send_dm


async def is_channel_sendable(
    bot,
    channel: MessageableChannel,
    fallback: Optional[MessageableChannel] = None,
) -> Optional[Union[MessageableChannel, bool]]:
    """Tests for permission to send messages in a given channel.

    Channel object must be one of type TextChannel, VoiceChannel, Thread, or DMChannel.

    Returns the channel on success, `None` for unknown, and otherwise `False`.
    """
    if not channel:
        if not fallback:
            err = "Must provide at least one channel object that is not None."
            raise ValueError(err)
        else:
            channel = fallback

    try:
        if type(channel) in (TextChannel, VoiceChannel, Thread):
            assert channel.can_send()
        elif isinstance(channel, DMChannel):
            assert await can_send_dm(channel, bot.user)
        else:
            raise TypeError
    except AssertionError as a:
        return a
    except TypeError:
        ok_types = "TextChannel, VoiceChannel, Thread, or DMChannel"
        raise TypeError(f"Channel object must be one of type {ok_types}")
    else:
        return channel


async def check_configured_channel(
    ctx: Context,
    module: str,
    key: str,
) -> Optional[Union[TextChannel, VoiceChannel, Thread]]:
    config = f"{module}.{key}"
    guild = ctx.guild
    channel_id = ctx.get_guild_key(guild.id, module, key)

    _prefix, _suffix = f"User tried to use `{ctx.command}`, but ", str()
    if not channel_id:
        _suffix = f"`{config}` isn't set"
    else:
        try:
            channel = await ctx.bot.fetch_channel(channel_id)
        except NotFound:
            _suffix = f"`{config}` is an invalid destination"
        except Forbidden:
            _suffix = f"lacking view perms to `{config}`"
        else:
            if channel.guild != guild:
                _suffix = f"`{config}` isn't part of this guild"
            elif type(channel) not in (TextChannel, VoiceChannel, Thread):
                _suffix = f"`{config}` isn't a valid channel type"
            elif not channel.permissions_for(guild.me).create_instant_invite:
                _suffix = f"lacking invite perms to `{config}`"
    if _suffix:
        err = _prefix + _suffix
        await errors.report_guild_error(ctx.bot, ctx, None, guild, err)
        return None

    return channel
