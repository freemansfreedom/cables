from typing import Optional
from discord import Guild, Member, Permissions
from cabling_bot.bot import CablesContext as Context, CablesBot as Bot


def is_readable(perms: Permissions) -> bool:
    return (
        perms.view_channel
        and (
            # voice channel
            perms.connect
            # text channel
            or (perms.read_messages and perms.read_message_history)
        )
    )


def is_writable(perms: Permissions) -> bool:
    return (
        # voice channel
        perms.speak
        # text channel
        or (
            perms.add_reactions
            and (perms.send_messages or perms.send_messages_in_threads)
        )
    )


def writable_channels_by_member(guild: Guild, member: Member) -> Optional[list]:
    matches = list()
    channels = guild.text_channels
    for channel in channels:
        perms = channel.permissions_for(member)
        if is_readable(perms) and is_writable(perms):
            matches.append(channel.id)

    return matches
