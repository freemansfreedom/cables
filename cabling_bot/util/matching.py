import re
import random
from typing import Optional, Tuple, Iterable
from discord import Permissions, Thread
from cabling_bot.bot.types import MessageableChannel

REGEX = r"(?P<url>https?://[^\s]+)"


def extract_url(text: str) -> str:
    try:
        url = re.search(REGEX, text).group("url")
    except AttributeError:
        return None
    else:
        return url


def extract_all_urls(text: str) -> list[str]:
    try:
        urls = re.findall(REGEX, text)
    except AttributeError:
        return None
    else:
        return urls


def has_url(text: str) -> Tuple:
    return bool(extract_url(text))


def rnd(iter):
    random.shuffle(iter)
    return iter[0]


class ListFilter():
    def __init__(self, tags: dict, filters: tuple):
        self.tags = tags
        self.filters = filters

        if not self.tags:
            return False
        if not self.filters:
            raise ValueError("no usable filters provided")

    def by_prefix(self) -> list[str]:
        return [x for x in self.tags if x.startswith(self.filters)] or False

    def by_suffix(self) -> list[str]:
        return [x for x in self.tags if x.endswith(self.filters)] or False

    def by_match(self) -> list[str]:
        return [x for x in self.tags if x in self.filters] or False


def channel_or_category(
    channel: MessageableChannel,
    iterable: Optional[Iterable] = None
) -> bool:
    channel_id = channel.id
    parent_id = None
    if channel.category:
        parent_id = channel.category.id
        if type(channel) is Thread:
            parent_id = channel.parent_id

    if not iterable:
        return False
    if not (
        channel_id in iterable
        or parent_id in iterable
    ):
        return False
    return True


def is_intersection(x: Permissions, y: Permissions) -> bool:
    """Returns ``True`` if self has at least one permission contained within other."""
    if (
        isinstance(x, Permissions)
        and isinstance(y, Permissions)
    ):
        x_set = set(iter(x))
        y_set = set(iter(y))
        result = x_set.intersection(y_set)
        return True if result else False
    else:
        raise TypeError(f"cannot compare {x.__class__.__name__} with {y.__class__.__name__}")
