import logging
import os
import discord
import locale
from pretty_help import DefaultMenu, PrettyHelp
from cabling_bot.bot import CablesBot as Bot

locale.setlocale(locale.LC_ALL)
if not locale.getlocale():
    locale.setlocale(locale.LC_ALL, 'en_US.UTF-8')

# module-wide logging
logger = logging.getLogger("cabling_bot")
logger.setLevel(logging.DEBUG)
handler = logging.StreamHandler()
handler.setFormatter(
    logging.Formatter(
        fmt="%(asctime)s %(name)-20s %(levelname)-8s %(message)s",
        datefmt="%Y-%m-%d %H:%M:%S",
    )
)
logger.addHandler(handler)
logger = logging.getLogger(__name__)

# initialize the bot client, context and config

env = os.environ.get("ENV", "development")
app_path = os.environ.get("CABLES_APP_PATH", "/usr/src/cables")
config_path = os.environ.get("CABLES_CONFIG_PATH", "/var/lib/cables")

intents = discord.Intents.none()
intents.guilds = True
intents.members = True
intents.bans = True
intents.integrations = True
intents.webhooks = True
intents.invites = True
intents.voice_states = True
intents.messages = True
intents.reactions = True
intents.message_content = True
intents.presences = True
intents.emojis_and_stickers = True

bot = Bot(
    command_prefix=".",
    environment=env,
    app_path=app_path,
    config_path=config_path,
    intents=intents
)

# discord module logging
discord_logger = logging.getLogger("discord")
discord_logger.setLevel(bot.get_config("pycord_log_level", "INFO"))
discord_logger.addHandler(handler)

# format help responses as embeds
menu = DefaultMenu(
    page_left="◀️", page_right="▶️", remove="❌",
    active_time=60, delete_after_timeout=True
)
ending_note = (
    f"Use '{bot.command_prefix}help <command>' or '{bot.command_prefix}help "
    + "<category>' for command or category details."
)
bot.help_command = PrettyHelp(
    navigation=menu,
    ending_note=ending_note,
    no_category="uncategorized"
)

cogs = [
    "events.ready",
    "events.cron",
    "cmds.migrate",
    "cmds.test",
    "events.dispatch",
    "events.guild_config",
    "events.modlog",
    "events.invites",
    "events.message_watch_user",
    "events.message_media_expand",
    "events.message_delete_react",
    "events.message_delete_recent",
    "events.message_hello",
    "events.message_roll",
    "events.message_amulets",
    "events.threads",
    "cmds.config",
    "cmds.debug",
    "cmds.inspect",
    "cmds.role_mgmt",
    "cmds.colors",
    "cmds.role_react",
    "cmds.namethrall",
    "cmds.embed",
    "cmds.topic",
    "cmds.bans",
    "events.bans",
    "cmds.report",
    "cmds.role_stats",
    "cmds.verifications",
]
logger.debug(f"Proposed extensions to load: {cogs}")
bot.load_extensions(cogs)

for name, cog in bot.cogs.items():
    cmds = [cmd.qualified_name for cmd in cog.walk_commands()]
    if cmds:
        logger.debug(f"Cog '{name}' loaded commands: {cmds}")
    listeners = [listener[0] for listener in cog.get_listeners()]
    if listeners:
        logger.debug(f"Cog '{name}' loaded listeners: {listeners}")

bot.run(bot.get_config("discord.bot_token"))
