**You have been banned** from _{{ guild }}_{% if issuer %} by {{ issuer }}{% endif %}{% if reason %} for the following reason:{% endif %}
> {{ reason }}
{% if policy_uri %}See the guild's official policy[1] for rules cited: {{ policy_uri }}

{% endif %}Views on when a ban is appropriate to employ vary between communities and moderation styles. Generally, they reflect the severity of the perceived conduct problem, and/or are a last resort for when other interventions fail.

Unless otherwise stated, this sanction is *indefinite* in length.

{% if can_appeal %}**You have the right to an appeal.** An administrator will contact you to answer any questions you may have and mediate this process. You are, however, free to ask whichever staff member you'd like.

{% if show_staff %}{{ staff_list }}

In the event that your messages can't be delivered, check your settings and contact relationships: <https://support.discord.com/hc/en-us/articles/360060145013-Why-isn-t-my-DM-going-through->.

{% endif %}{% endif %}Bans are often stressful events for everyone involved; remember to take care of yourself and try to get any support you need from friends or alternative online communities.