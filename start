#!/bin/bash

### Load

_INCLUDE_SOURCES=(
  "check_pwd.sh"
  "common.sh"
  "check_deps.sh"
)
set -a
for _file in "${_INCLUDE_SOURCES[@]}"; do
  source "$PWD/include/$_file"; done
set +a

### Option parsing

if [[ "$1" == "--freshen" || "$2" == "--freshen" ]]; then
  _OPT_FRESHEN=true
elif [[ "$1" == "--update" || "$2" == "--update" ]]; then
  _OPT_UPDATE=true
fi

### Manage poetry venv

if [[ "$POETRY_ACTIVE" && "$VIRTUAL_ENV" ]]; then
  _PYVENV_BASE="$(basename "$VIRTUAL_ENV")"
  if [ "${_PYVENV_BASE:0:11}" != "cabling-bot" ]; then
    _usage 'Incorrect poetry venv detected!'; fi

  if [ "$_OPT_FRESHEN" ]; then
    _log info 'Destroying current poetry venv'
    _VENV="$VIRTUAL_ENV"
    source deactivate
    rm -r "$_VENV"
    unset POETRY_ACTIVE
    export POETRY_ACTIVE

    # creates and activates a venv named the same as before
    _log info 'Installing project dependencies'
    poetry install
    source "$_VENV/bin/activate"
  elif [ "$_OPT_UPDATE" ]; then
    _log info 'Updating project dependencies'
    poetry update
  fi
else
  _log info 'Creating poetry venv'
  poetry install
  source "$_VENV/bin/activate"
fi

### Load environment from file
source "$PWD/include/env.sh"

### Parse program log level overrides

CABLES_APP_PATH="$PWD"
CABLES_CONFIG_PATH="$CABLES_APP_PATH/configs"
export CABLES_APP_PATH
export CABLES_CONFIG_PATH
_CONF_FILE="$CABLES_CONFIG_PATH/bot.json"
_CONF_LOG_LEVEL="$(jq -r '.pycord_log_level' "$_CONF_FILE")"
_ENV_LOG_LEVEL="$(grep PYCORD_LOG_LEVEL "$_ENV_FILE")"
if test \
  ! "$PYCORD_LOG_LEVEL" \
  -a ! "$_CONF_LOG_LEVEL" \
  -a ! "$_ENV_LOG_LEVEL"
then
  PYCORD_LOG_LEVEL=INFO
  export PYCORD_LOG_LEVEL
fi

### Run the program

python -m cabling_bot
_RET=$?

if [ "$_OPT_FRESHEN" ]; then
  _log info "Dropping to shell in fresh venv"; fi

exit $_RET
