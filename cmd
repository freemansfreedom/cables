#!/bin/sh -e

CONFIG_FILE="${CABLES_CONFIG_PATH}/bot.json"

# Logging

STDLOG() {
	local NOW; NOW="$(date +'%Y-%m-%d %H:%M:%S')"
    local LEVEL
	test "$1" -eq 0 && LEVEL=INFO
	test "$1" -eq 1 && LEVEL=ERROR
    test "$1" -eq 2 && LEVEL=WARNING
	test ! "$LEVEL" && LEVEL="$1"

	if [ "$LEVEL" = 'ERROR' ]
		then printf '%s\t%s\t%s\n' "$NOW" "$LEVEL" "$2" >&2
		else printf '%s\t%s\t%s\n' "$NOW" "$LEVEL" "$2"
    fi
}
export STDLOG

# Helper functions

CHECKDEP() {
	if ! command -v $1 >/dev/null; then
		STDLOG 1 "$1 not found!"
		exit 78
	fi
}

CHECKPERMS() {
	if ! test \
		-r "$1" \
		-a -w "$1"
	then
		return 1
	fi
}

# Update from SCM

if [ ! -d '.git' ]; then
	STDLOG 2 'Updated failed: not a git repository'; fi
if [ "$(git branch --show-current)" ]; then
	STDLOG 0 'Checking for updates'

	CHECKDEP git

	if ! git pull; then
		STDLOG 2 'Update failed: code pull error'
	else
		if ! poetry install $(test "$ENV" == production && echo "--no-dev") --no-interaction --no-ansi
			then STDLOG 2 'Update failed: poetry error'; fi
	fi
else
	STDLOG 2 'Update failed: not on a branch'
fi

# Run checks and start

CHECKDEP python
if ! CHECKPERMS "$CONFIG_FILE"; then
	STDLOG 2 "Permissions are wrong for '$CONFIG_FILE'; correcting"
	chmod 666 "$CONFIG_FILE"
fi
python ./cabling_bot/main.py
