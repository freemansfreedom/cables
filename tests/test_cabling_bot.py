from cabling_bot import __version__, util
from cabling_bot.cmds import colors, namethrall, role_react


def test_version():
    assert __version__ == "0.1.0"
